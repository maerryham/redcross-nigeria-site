<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- ======= titles ======= -->
    <title>The Nigeria Redcross</title>
    <link rel="icon" type="image/png" href="{{ asset('favicon.png')}}">
    <!-- ======= Google Fonts ======= -->
    <!-- Lato+Raleway Google Fonts -->
    <style>
        body{
            max-height:100vh;
        }
        .center{
            margin:300px auto;
            text-align:center;
        }
        .center p{
            font-family:"open sans";
            font-size:40px;
            font-weight:200;
            color:rgba(200,0,0,0.9);
        }
        .link{
            font-size:30px;
        }
    </style>
</head>
<body>
    <div class='container center'>
        <p>Thanks for reaching out to us, your request will be treated shortly</p>
        <a href="{{route('index')}}" class="link" >Back to homepage</a>
    </div>
</body>
<html>