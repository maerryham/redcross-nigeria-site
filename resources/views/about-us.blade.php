@extends('layouts.base')
@section('main-section')
 <!-- ======= page title part srat ======= -->
    <section class="page-title-area parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- page title -->
                    <div class="page-title">
                        <div class="title">
                            <h2>about us</h2>
                        </div>
                        <ul class="breadcrumb">
                            <li><a href="{{ route('index')}}">Home</a></li>
                            <li class="active">about us</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ======= page title part end ======= -->

    <!-- ======= about part start ======= -->
    <div class="about-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <!-- section title -->
                    <div class="title">
                        <h2>who we are</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <!-- about img -->
                    <div class="about-img">
                        <img src="{{asset('img/about/'.$page_data->image)}}" alt="" >
                    </div>
                </div>
                <div class="col-sm-7">
                    <!-- about details -->
                    <div class="about-details">
                        <p>
                            {{$page_data->who_we_are}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ======= about part end ======= -->

    <!-- ======= Mission part start ======= -->
    <section class="about-info-area section-padding parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <!-- about content -->
                    <div class="about-content">


                        <h4>our mission</h4>
                        <p>{{$page_data->our_mission}}</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <!-- about content -->
                    <div class="about-content">
                        <h4>our vision</h4>
                        <ul>
                            {!! $page_data->our_vision!!}

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ======= Mission part end ======= -->


@endsection()