@extends('layouts.base')
@section('main-section')<!-- Hero Section -->
<section class="page-title-area parallax">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- page title -->
                <div class="page-title">
                    <div class="title">
                        <h2>Newsletter</h2>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="{{ route('index')}}">Home</a></li>
                        <li class="active">Newsletter </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="about-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <!-- section title -->
                <div class="title">
                    <h2>Newsletter</h2>
                </div>
            </div>
        </div>


        <div class="container" id="message">
            @if(Session::has('message'))
                <div class="alert alert-success">
                    {{ Session::get('message') }}
                </div>
            @endif
        </div>

            {{--<form action="/newsletter/subscribe" method="post" class="form-inline container">--}}
                    {{--<div class="contact-form bg-trans row">--}}
                        {{--{{ csrf_field() }}--}}

                        {{--<div class="col-xs-12 col-sm-12 col-md-12">--}}

                            {{--<div class="form-group">--}}

                                {{--<div class="input-group">--}}
                                    {{--<input type="email" class="form-control" id="email" name="email" placeholder="Your Email">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="col-xs-12 col-sm-12 text-md-center">--}}
                          {{--<center>--}}
                              {{--<button type="submit" id="submit" name="submit" class="sub-btn donate-btn more-btn hvr-shutter-out-horizontal">Subscribe now</button>--}}
                          {{--</center>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                {{--</div>--}}
            {{--</form>--}}

    </div>
</div>
<!-- End Booking Section -->

    @endsection()


    @section('jquery')
        {{--<script src="{{ asset('js/jquery-2.1.1.min.js')}}"></script>--}}

@endsection