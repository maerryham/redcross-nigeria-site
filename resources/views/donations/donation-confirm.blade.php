@extends('layouts.base')
@section('main-section')

    <div class="donate-note">
        <img src="{{ asset('img/donation.jpg')}}">
        <div class="note">
            <div class="col-sm-6 col-sm-offset-3 text-center">
                <p class=" text-white">
                    Thanks for Donating to the <span class="text-red">"Nigerian Redcross"</span>
                </p>
                <a href="{{ route('index')}}" class="sub-btn donate-btn more-btn hvr-shutter-out-horizontal ">Home page</a>
            </div>
        </div>
    </div>
@endsection()