@extends('layouts.base')
@section('main-section')
    <!-- ======= page title part srat ======= -->
    <section class="page-title-area parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- page title -->
                    <div class="page-title">
                        <div class="title">
                            <h2>Donations</h2>
                        </div>
                        <ul class="breadcrumb">
                            <li><a href="">Home</a></li>
                            <li class="active">Donations</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ======= page title part end ======= -->

    <!-- ======= about part start ======= -->
    <div class="about-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 ">
                    <!-- section title -->
                    <div class="title text-center">
                        <h2>Make Donations</h2>
                    </div>
                    <div class="">
                        <form id="donation-form">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Fullname</label>
                                    <input class="form-control" type="text" name="name" placeholder="Name" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Email address</label>
                                    <input class="form-control" type="email" name="email" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Phone number <small>(optional)</small></label>
                                    <input class="form-control" type="text" name="phone_number" placeholder="Phone Number">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Choose a redcross region <small>(optional)</small></label>
                                    <select class="form-control" name="region"  required>
                                        <option value="">Choose a Redcross region</option>
                                        @foreach($regions as $region)
                                            <option value={{$region->id}}>{{ $region->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Choose a cause for donation (<small>(optional)</small></label>
                                    <select class="form-control" id="choose"  name="cause" placeholder="select  a cause for donation" required>
                                        {{--<option value="">General Donation</option>--}}
                                        @foreach($causes as $cause)
                                            <option value={{$cause->name}}>{{ $cause->name}}</option>
                                        @endforeach
                                        <option value='other'>Others</option>
                                    </select>
                                    <label id="others" style="display: none;">Enter your Donation Cause below
                                        <input type="text" class="form-control" name="dcause" />
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Amount (NGN) <small>(optional)</small></label>
                                    <input class="form-control" type="number" name="amount"  min="100" placeholder="How much will you like to donate " required>
                                </div>
                            </div>
                            {{ csrf_field() }}
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Message <small>(optional)</small></label>
                                    <textarea class="form-control" name="message" data-gramm="true" spellcheck="false" data-gramm_editor="true" placeholder="Message"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox"  value="0" class="form-control" name="anonymous"  id="anonymous">
                                        Keep me anonymous.
                                    </label>
                                </div>
                                <button type="submit" class="sub-btn donate-btn more-btn hvr-shutter-out-horizontal">Make Donation</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="paystackEmbedContainer"></div>
@endsection()
@section('script-section')
    <script src="https://js.paystack.co/v1/inline.js"></script>
    <script>
        const donation_form = $('#donation-form');
        const anonymous_input = $('#anonymous');


        function generateRef(){
            return Math.floor(Math.random() * (2000 - 1000) + 100);
        }

        function paystackPayment(data){
            const handler = PaystackPop.setup({
                key: "{{env('PAYSTACK_PUBLIC_KEY')}}",
                email: data.email,
                amount: parseInt(data.amount) * 100,
                ref: 'donate-'+data.reference,
                metadata: {
                    custom_fields: [
                        {
                            name: data.name,
                            email: data.email,
                            phone_number: data.phone_number
                        }
                    ]
                },
                callback: function(response){
                    window.location.assign("/donations/confirmation/"+ response.reference);
                },
                onClose: function(){

                }
            });
            handler.openIframe();
        }

        function donate(data){
            $.ajax({
                url: "{{ route('make-donation')}}",
                method:'POST',
                data: data,
                success: function (response){
                    if(response.status){
                        paystackPayment(response.data);
                    }
                },
                error: function (response){
                    console.log(response);
                }
            })
        }

        donation_form.on('submit', function (e) {
            e.preventDefault();
            var data = $(this).serialize();
            data += '&reference=' + generateRef();
            donate(data);
        });

        anonymous_input.on('click', function (e){
            console.log($(this));
            if($(this).is('[selected]')){
                $(this).val(0);
                $(this).removeAttr('selected');
            }else{
                $(this).val(1);
                $(this).attr('selected', 'selected');
            }
        });
    </script>
    <script>
        $(document).ready(function() {
            $("#choose").on("change", function() {
                if ($(this).val() === "other") {
                    $("#others").show();
                }
                else {
                    $("#others").hide();
                }
            });
        });
    </script>
@endsection()
