<!-- ======= call to action part start ======= -->
<script>
    $(function () {
        var messages = [],
            index = 0;

        messages.push("{{$contact_detail->first_message_f}}");
        messages.push("{{$contact_detail->second_message_f}}");
        messages.push("{{$contact_detail->third_message_f}}");


        function cycle() {
            $('#changing_footer').html(messages[index]);
            index++;

            if (index === messages.length) {
                index = 0;
            }

            setTimeout(cycle, 3000);
        }

        cycle();
    });
</script>


<section class="call-to-action">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="call-to-main" >
                    <h2 id="changing_footer">We care for children, protect their welfare, and prepare them for the future.</h2>
                </div>
            </div>
            <div class="col-md-3 text-right">
                <div class="donate-call">
                    <a href="{{ route('donation-index-page') }}" class="donate-btn more-btn hvr-shutter-out-horizontal"><i class="fa fa-send"></i>donate now</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ======= call to action part end ======= -->
<!-- ======= footer area start ======= -->
    <footer class="footer-area parallax">
        <div class="top-footer-area section-padding">
            <div class="container">
                <div class="row">
                    <!-- footer widgets -->
                    <div class="col-md-3 col-sm-6">
                        <div class="footer-widgets">
                            <h2>Head Office</h2>
                            <p>
                                {{$contact_detail->address}}
                            </p>
                            <ul>
                                <li><i class="fa fa-phone" aria-hidden="true"></i> <a href="#">{{$contact_detail->phone}}</a></li>
                                <li><i class="fa fa-envelope" aria-hidden="true"></i> <a href="#">{{$contact_detail->email}}</a></li>
                            </ul>
                            <!-- footer social link -->
                            <div class="footer-social-link">
                                <a href="{{$contact_detail->facebook}}" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <a href="{{$contact_detail->twitter}}" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                <a href="{{$contact_detail->instagram}}" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- footer widgets -->
                    <div class="col-md-2 col-sm-6">
                        <div class="footer-widgets">
                            <h2>quick links</h2>
                            <ul>
                                <li><a href="{{route('about-us')}}"><i class="fa fa-angle-right"></i>About Us</a></li>
                                <li><a href="{{ route('donation-index-page') }}"><i class="fa fa-angle-right"></i>Donate</a></li>
                                <li><a href="{{ route('contact-us')}}"><i class="fa fa-angle-right"></i>Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                    {{--  <!-- footer widgets -->--}}
                    <div class="col-md-3 col-sm-6">
                        <div class="footer-widgets ">
                            <h2>TWITTER FEEDS</h2>
                            <a class="twitter-timeline"  href="https://twitter.com/nrcs_ng"  data-widget-id="358752879730896897">Tweets by @nrcs_ng</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

                        </div>
                    </div>
                    <!-- footer widgets -->
                    <div class="col-md-4 col-sm-6">
                        <div class="footer-widgets m-unset">
                            <h2>Photo Gallery</h2>
                            <ul class="list">
                                <li><a href="{{ asset('img/gallery/gallery1.jpg')}}" data-lightbox="example-set" data-title="believed"><img src="{{ asset('img/gallery/gallery1.jpg')}}" alt="#"></a></li>
                                <li><a href="{{ asset('img/gallery/gallery2.jpg')}}" data-lightbox="example-set" data-title="believed"><img src="{{ asset('img/gallery/gallery2.jpg')}}" alt="#"></a></li>
                                <li><a href="{{ asset('img/gallery/gallery3.jpg')}}" data-lightbox="example-set" data-title="believed"><img src="{{ asset('img/gallery/gallery3.jpg')}}" alt="#"></a></li>
                                <li><a href="{{ asset('img/gallery/gallery4.jpg')}}" data-lightbox="example-set" data-title="believed"><img src="{{ asset('img/gallery/gallery4.jpg')}}" alt="#"></a></li>
                                <li><a href="{{ asset('img/gallery/gallery5.jpg')}}" data-lightbox="example-set" data-title="believed"><img src="{{ asset('img/gallery/gallery5.jpg')}}" alt="#"></a></li>
                                <li><a href="{{ asset('img/gallery/gallery6.jpg')}}" data-lightbox="example-set" data-title="believed"><img src="{{ asset('img/gallery/gallery2.jpg')}}" alt="#"></a></li>
                                <li><a href="{{ asset('img/gallery/gallery7.jpg')}}" data-lightbox="example-set" data-title="believed"><img src="{{ asset('img/gallery/gallery3.jpg')}}" alt="#"></a></li>
                                <li><a href="{{ asset('img/gallery/gallery8.jpg')}}" data-lightbox="example-set" data-title="believed"><img src="{{ asset('img/gallery/gallery1.jpg')}}" alt="#"></a></li>
                            </ul>
                            <div class="subscribe-box">
                                <form action="{{route('newsletter/subscribe')}}" method="post">
                                    {{ csrf_field() }}
                                    <input type="email" placeholder="Your Email to Subscibe to our Newsletter" name="email" required>
                                    <button type="submit" class="donate-btn more-btn hvr-shutter-out-horizontal"><i class="fa fa-send"></i>Subscribe</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-footer-area">
            <div class="container">
                <!-- copy right -->
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <div class="copy-right">
                            <p>Copyright &copy; 2018 all rights reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>


<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5c627cd97cf662208c951fd4/1d3gbqogo';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
    <!-- ======= footer area end ======= -->