<!-- ======= header part start ======= -->
    <header class="header-area">
        <!-- top header area -->
        <style>
            .social-link {
                list-style: none;
                padding: 0;
                line-height: 1;
                box-sizing: border-box;
                float: left;
            }

            .social-link li {
                display: inline-block;
                margin: 0;
                /*padding: 0 10px;*/
            }

            .social-link li a{
                border-radius: 30px;
                margin: 0px 5px;
                padding: 10px;
                width: 40px;
                height: 40px;
                /*border: solid 1px #fff;*/
                background: #D22034;
                transition: .5s;
                color: #fff;
            }

            .social-link li fa{
                font-family: "Font Awesome 5 Brands";
                font-weight: 400;
                color: #FFF;
                position: relative;
                transition: .3s ease;
                font-family: ET-Extra!important;
                speak: none;
                font-style: normal;
                font-weight: 400;
                font-variant: normal;
                text-transform: none;
                line-height: inherit!important;

            }
            .social-link li a:hover{
                background: #FFF !important;
                color: #D22034;
            }
        </style>
        <div class="top-header-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <!-- header contact -->
                        <div class="top-header-info">
                            <a href="#"><i class="fa fa-phone"></i>{{$contact_detail->phone}}</a><span class="seprator">|</span>
                            <a href="mailto:admin@redcrossnigeria.org"><i class="fa fa-envelope"></i>{{$contact_detail->email}}</a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <ul class="social-link">
                            <li><a href="{{$contact_detail->facebook}}"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="{{$contact_detail->twitter}}"><i class="fa fa-twitter"></i></a></li>
                            {{--<li><a href="#"><i class="fa fa-google-plus"></i></a></li>--}}
                            <li><a href="{{$contact_detail->instagram}}" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <!-- <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li> -->
                        </ul>
                    </div>
                    <div class="col-md-3 text-right">
                        <!-- header support -->
                        <div class="top-header-info header-right">
                            <a href="{{ route('frequent-asked') }}">faq</a><span class="seprator">|</span>
                            <a href="{{ route('contact-us') }}">contact us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- menu area -->
        <div class="menu-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <!-- logo part -->
                        <div class="believed-logo">
                            <a href="{{ route('index') }}"><img src="{{ asset('img/logo.png')}}" alt=""></a>
                        </div>
                        <!-- mobile menu wraper -->
                        <div class="responsive-menu-wrap"></div>
                    </div>
                    <div class="col-md-10">
                        <!-- menu part -->
                        <nav class="main-menu">
                            <ul class="navigation">
                                <li>
                                    <a href="{{ route('index') }}">home</a>
                                </li>
                                <li ><a href="{{ route('about-us') }}">about us</a><i class="fa fa-caret-down" aria-hidden="true"></i>
                                     <ul class="drop-menu">
                                        <li><a href="{{ route('history-page')}}">Our history</a></li>

                                         <li>
                                             <a href="{{ route('gallery') }}">Our Gallery</a>
                                         </li>
                                         <li>
                                             <a href="{{ route('download_page') }}">Download Page</a>
                                         </li>
                                         {{--<li>--}}
                                             {{--<a href="{{ route('download_report') }}">Click to download NRCS Annual Report 2018</a>--}}
                                         {{--</li>--}}
                                         <li>
                                             <a href="{{ route('fundamentalprinciples') }}">7 fundamental principles</a>
                                         </li>
                                    </ul>
                                </li>
                                <li ><a href="{{ route('news') }}">News</a><i class="fa fa-caret-down" aria-hidden="true"></i>
                                    <ul class="drop-menu">
                                        <li><a href="{{ route('news')}}">Latest News</a></li>

                                        <li>
                                            <a href="{{ route('archive') }}">Archives</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('newsletter') }}">Newsletter</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('press_release') }}">Press release</a>
                                        </li>
                                    </ul>
                                </li>


                                <li><a href="{{ route('regions')}}">Branches</a>
                                </li>

                                <li>
                                    {{--<a href="https://redcrossnigeria.org/database/index.php">Join NRC</a>--}}
                                    <a href="http://redcrossnigeria.org/database/index_new.php">Join NRC</a><i class="fa fa-caret-down" aria-hidden="true"></i>
                                    <ul class="drop-menu">
                                        <li><a href="{{ route('get-trained-now')}}">Training And Certification</a></li>
                                        <li><a href="http://redcrossnigeria.org/database/index_new.php">Join us</a></li>
                                    </ul>
                                </li>

                                <li>
                                    {{--<a href="https://redcrossnigeria.org/database/index.php">Join NRC</a>--}}
                                    <a href="{{ route('contact-us') }}">Contact us</a><i class="fa fa-caret-down" aria-hidden="true"></i>
                                    <ul class="drop-menu">
                                        <li><a href="{{ route('board-members')}}">Board Members</a></li>
                                        <li><a href="{{ route('head-of-departments')}}">Head of Departments</a></li>
                                    </ul>
                                </li>


                            </ul>
                        </nav>
                        <!-- donate box -->
                        <div class="donate-box">
                            <span style="display: block; text-align: right;">
                                <a style="" href="{{ route('donation-index-page') }}" class="donate-btn hvr-shutter-out-horizontal">donate</a><br><br>
                            </span>
                            <span id="some-id" style="text-align: right"></span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- ======= header part end ======= -->
<script src="http://code.jquery.com/jquery-1.7.min.js"></script>
<script>
    $(function () {
        var messages = [],
            index = 0;
        messages.push("{{$contact_detail->first_message_h}}");
        messages.push("{{$contact_detail->second_message_h}}");
        messages.push("{{$contact_detail->third_message_h}}");


        function cycle() {
            $('#some-id').html(messages[index]);
            index++;

            if (index === messages.length) {
                index = 0;
            }

            setTimeout(cycle, 3000);
        }

        cycle();
    });
</script>
