@extends('layouts.base')
@section('main-section')
    <section class="page-title-area parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- page title -->
                    <div class="page-title">
                        <div class="title">
                            <h2>Frequently Asked Questions</h2>
                        </div>
                        <ul class="breadcrumb">
                            <li><a href="{{ route('index')}}">Home</a></li>
                            <li class="active">FAQ</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ======= page title part end ======= -->

    <!-- ======= Contact Area Start ======= -->
    <section class="contact-area section-padding parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <!-- section title -->
                    <div class="title">
                        <h2>Frequently Asked Questions</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                    <div class="contact-form bg-trans">

                        <div class="accordion" id="accordionExample2">
                            @foreach($faqs as $faq)
                            <div class="card z-depth-0 bordered">
                                <div class="card-header" id="headingOne{{$faq->id}}">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne{{$faq->id}}"
                                                aria-expanded="true" aria-controls="collapseOne2">
                                            {{$faq->question}}
                                        </button>
                                    </h5>
                                </div>
                                <div id="collapseOne{{$faq->id}}" class="collapse" aria-labelledby="headingOne{{$faq->id}}" data-parent="#accordionExample2">
                                    <div class="card-body" style="text-align: justify">
                                        {!!$faq->answer!!}
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            {{--<div class="card z-depth-0 bordered">--}}
                                {{--<div class="card-header" id="headingTwo2">--}}
                                    {{--<h5 class="mb-0">--}}
                                        {{--<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo2"--}}
                                                {{--aria-expanded="false" aria-controls="collapseTwo2">--}}
                                            {{--What kind of qualification do I need?--}}
                                            {{--How much experience do I need?--}}
                                        {{--</button>--}}
                                    {{--</h5>--}}
                                {{--</div>--}}
                                {{--<div id="collapseTwo2" class="collapse" aria-labelledby="headingTwo2" data-parent="#accordionExample2">--}}
                                    {{--<div class="card-body">--}}
                                        {{--<p>For our mobile entry-level positions, we require at least two years' post-graduate experience. This requirement may be higher for some specialist positions, such as engineers, logisticians or medical workers. Greater professional experience may be required for senior positions.</p>--}}
                                        {{--<p>Please note that internships may count towards the professional experience required, depending on what they involve and providing they last at least six months.</p>--}}
                                        {{--<p>Please note that our recruiters evaluate applications as a whole. To succeed, you'll need the right combination of motivation, professional and personal experience, and language and soft skills.</p>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="card z-depth-0 bordered">--}}
                                {{--<div class="card-header" id="headingThree2">--}}
                                    {{--<h5 class="mb-0">--}}
                                        {{--<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree2"--}}
                                                {{--aria-expanded="false" aria-controls="collapseThree2">--}}
                                            {{--What language skills do I need? How will I be tested?--}}
                                        {{--</button>--}}
                                    {{--</h5>--}}
                                {{--</div>--}}
                                {{--<div id="collapseThree2" class="collapse" aria-labelledby="headingThree2" data-parent="#accordionExample2">--}}
                                    {{--<div class="card-body">--}}
                                        {{--<p>English is the official languages of the NRCS. A very good knowledge of both languages is, therefore, highly recommended and</p>--}}
                                        {{--sometimes required. Exceptions may be made for some specific jobs.</p>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="card z-depth-0 bordered">--}}
                                {{--<div class="card-header" id="headingFour">--}}
                                        {{--<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour"--}}
                                                {{--aria-expanded="false" aria-controls="collapseThree2">--}}
                                            {{--What are the steps in the recruitment process?--}}
                                        {{--</button>--}}

                                {{--</div>--}}
                                {{--<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample2">--}}
                                    {{--<div class="card-body">--}}
                                        {{--<h5>Recruitment for mobile field positions usually follows this order:</h5>--}}
                                        {{--<ul>--}}
                                            {{--<li>Screening – We select candidates for interview.</li>--}}
                                            {{--<li>Interviews – Candidates have a general interview with a recruiter and/or a technical interview with experts for specialist positions.</li>--}}
                                            {{--<li>Language tests – We test candidates' language skills.</li>--}}
                                            {{--<li>Technical tests (optional)</li>--}}
                                        {{--</ul>--}}
                                        {{--<p>Interviews can be done either at our headquarters or via Skype/phone, depending on where you are based. Candidates are expected to cover their own recruitment-related expenses.</p>--}}


                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="card z-depth-0 bordered">--}}
                                {{--<div class="card-header" id="headingFive">--}}
                                    {{--<h5 class="mb-0">--}}
                                        {{--<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive"--}}
                                                {{--aria-expanded="false" aria-controls="collapseFive">--}}
                                            {{--Can I apply again if I am not successful?--}}
                                        {{--</button>--}}
                                    {{--</h5>--}}
                                {{--</div>--}}
                                {{--<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample2">--}}
                                    {{--<div class="card-body">--}}
                                        {{--<p>If you were not short-listed, it probably means that you do not match our requirements. If you later improve your language skills, gain more work experience and meet our criteria, you are welcome to apply again.</p>--}}
                                        {{--<p>However, we recommend you do not reapply for at least six months. This is because, generally speaking, we don't think that any significant change can occur in a CV before then.</p>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="card z-depth-0 bordered">--}}
                                {{--<div class="card-header" id="headingSix">--}}
                                    {{--<h5 class="mb-0">--}}
                                        {{--<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSix"--}}
                                                {{--aria-expanded="false" aria-controls="collapseSix">--}}
                                            {{--Can I apply again if I am not successful?--}}
                                        {{--</button>--}}
                                    {{--</h5>--}}
                                {{--</div>--}}
                                {{--<div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample2">--}}
                                    {{--<div class="card-body">--}}
                                        {{--<p>If you were not short-listed, it probably means that you do not match our requirements. If you later improve your language skills, gain more work experience and meet our criteria, you are welcome to apply again.</p>--}}
                                        {{--<p>However, we recommend you do not reapply for at least six months. This is because, generally speaking, we don't think that any significant change can occur in a CV before then.</p>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="card z-depth-0 bordered">--}}
                                {{--<div class="card-header" id="headingSeven">--}}
                                    {{--<h5 class="mb-0">--}}
                                        {{--<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSeven"--}}
                                                {{--aria-expanded="false" aria-controls="collapseSix">--}}
                                            {{--Why do I need to get medical clearance before going on assignment?--}}
                                        {{--</button>--}}
                                    {{--</h5>--}}
                                {{--</div>--}}
                                {{--<div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample2">--}}
                                    {{--<div class="card-body">--}}
                                        {{--<p>Our mobile staff are often posted to remote areas where health-care services may be limited.</p>--}}
                                        {{--<p>To minimize the risk of medical issues arising while you're in the field, you must get medical clearance before you go.--}}
                                        {{--<p>For more information, please see our medical standards.</p>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="card z-depth-0 bordered">--}}
                                {{--<div class="card-header" id="headingEight">--}}
                                    {{--<h5 class="mb-0">--}}
                                        {{--<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseEight"--}}
                                                {{--aria-expanded="false" aria-controls="collapseEight">--}}
                                            {{--Will I be safe?--}}
                                        {{--</button>--}}
                                    {{--</h5>--}}
                                {{--</div>--}}
                                {{--<div id="collapseEight" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample2">--}}
                                    {{--<div class="card-body">--}}
                                        {{--<p>We take our staff's safety very seriously. We are constantly monitoring the situation and adapting our security rules – by having curfews, restricting the use of public transport, etc. We expect every staff member to abide by our security rules and ensure others do so as well.</p>--}}
                                    {{--<p>Another vital aspect of our staff's safety is the NRCS's reputation as a neutral and impartial organization, which allows us to work in areas where we might otherwise be unwelcome. We therefore ask our staff not to make any public political statements that could harm how our organization is perceived, and which would undermine our work.</p>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="card z-depth-0 bordered">--}}
                                {{--<div class="card-header" id="headingNine">--}}
                                    {{--<h5 class="mb-0">--}}
                                        {{--<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseNine"--}}
                                                {{--aria-expanded="false" aria-controls="collapseEight">--}}
                                            {{--Can I choose my assignment?--}}
                                        {{--</button>--}}
                                    {{--</h5>--}}
                                {{--</div>--}}
                                {{--<div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordionExample2">--}}
                                    {{--<div class="card-body">--}}
                                        {{--<p>Not at first. When you join the NRCS, you must be prepared to go wherever your skills are most needed. With time, you will be given more say in your assignment and advice on how to develop your career.</p>--}}

                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ======= Contact Area End ======= -->


@endsection()
@section('google-map-script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCq4jmXOSB3mlhBKAmVhWq5BUmFzvwWIsk"></script>
    <script src="js/gmap3.min.js"></script>
@endsection()