@extends('layouts.base')
@section('main-section')
 <!-- ======= page title part srat ======= -->
    <section class="page-title-area parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- page title -->
                    <div class="page-title">
                        <div class="title">
                            <h2>download page</h2>
                        </div>
                        <ul class="breadcrumb">
                            <li><a href="{{ route('index')}}">Home</a></li>
                            <li class="active">Download Page</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ======= page title part end ======= -->

    <!-- ======= about part start ======= -->
    <div class="about-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <!-- section title -->
                    <div class="title">
                        <h2>download page</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($documents as $document)
                <a href="{{asset('download/'.$document->file_name)}}" target="_blank">
                    <div class="col-sm-5">
                    <!-- about img -->
                    <div class="about-img">
                        {{--<img src="{{asset('img/about/'.$page_data->image)}}" alt="" >--}}
                        <i class="fa fa-download pull-right"></i>
                    </div>
                </div>
                <div class="col-sm-7">
                    <!-- about details -->

                        <h3 title="Click to download {{$document->title}}">
                            {{$document->title}}
                        </h3>

                </div></a>
                    @endforeach()


            </div>
        </div>
    </div>
    <!-- ======= about part end ======= -->



@endsection()