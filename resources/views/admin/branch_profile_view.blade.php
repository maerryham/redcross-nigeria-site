
@extends('admin.layouts.base')

@section('main-section')
    <div class="content-body">
        <!-- Revenue, Hit Rate & Deals -->

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="horz-layout-basic"></h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="{{route('admin/branch_profile')}}">View All {{Auth::user()->name}} Branch Profile</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collpase show">
                        <div class="card-body">
                            <div class="" id="message">

                                @if(Session::has('message'))
                                    <div class="alert alert-success" >
                                        {{ Session::get('message') }}
                                    </div>
                                @endif
                            </div>

                            <form class="form form-horizontal" method="post" action="branch_profile_upload" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <?php
                                        $year = strftime("%Y", strtotime($branch_profile->date));
                                        $month = strftime("%B", strtotime($branch_profile->date));
                                    ?>
                                   <h4 class="form-section">View {{Auth::user()->name}} Branch Profile
                                        <span id="present_date" class="pull-right"> {{$month}}, {{$year}} </span>

                                    </h4>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Branch Profile Subject</label>
                                        <div class="col-md-9" >
                                                {{$branch_profile->subject}}
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput2">Branch Profile Content/<br>Description</label>
                                        <div  class="col-md-9" >

                                            {!!  $branch_profile->content !!}

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Branch Services</label>
                                        <div class="col-md-9" >

                                            <?php $json_output = json_decode($branch_profile->branch_services, true);
                                            ?>
                                                @foreach($json_output as $value)
                                                    @if($value != '')
                                                        <span style="margin:5px;">{{$value}}</span>
                                                    @endif
                                                @endforeach
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Volunteer Strength</label>
                                        <div class="col-md-9" >
                                            {{$branch_profile->volunteer_strength}}
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Number of Active Volunteer</label>
                                        <div class="col-md-9" >
                                            {{$branch_profile->active_volunteer}}
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Video Link</label>
                                        <div class="col-md-9" >
                                            <div class="embed-responsive embed-responsive-16by9" style="margin-bottom: 10px">
                                                <iframe class="embed-responsive-item" src="{{$branch_profile->video_link}}" allowfullscreen></iframe>
                                            </div>
                                            {{$branch_profile->video_link}}

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput3">Images for Slide</label>
                                        <div class="col-md-9">

                                            <?php $json_output = json_decode($branch_profile->images, true);
                                            ?>
                                            @foreach($json_output as $value)
                                                @if($value != '')
                                                    {{--<span style="margin:5px;">{{$value}}</span>--}}
                                                    <img class="img-thumbnail" style="max-width:100px" src="{{asset('img/branch_profile/'.$value)}}">
                                                @endif
                                            @endforeach

                                            {{--<img class="img-thumbnail" style="max-width:100px" src="{{asset('img/branch_profile/'.$branch_profile->images)}}">--}}

                                        </div>
                                    </div>




                                    <div class="form-actions">
                                        {{--<button type="button" class="btn btn-warning mr-1">--}}
                                        {{--<i class="ft-x"></i> Cancel--}}
                                        {{--</button>--}}
                                       <center>
                                          <a href="../branch_profile_edit/{{$branch_profile->id}}"> <button  type="button" class="btn btn-info center">
                                                Edit Branch Profile
                                            </button>
                                          </a>
                                       </center>
                                    </div>
                                </div></form>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>

@endsection
@section('style-section')

@endsection
@section('script-section')
    <script>
        $(document).ready(function() {
            $('#editor1').trumbowyg();
            // $('#editor2').trumbowyg();

        });
    </script>

    <script>
        $(document).ready(function() {
            $("#other").on("change", function() {
                // $(".animals").change(function () {
                //check if the selected option is others
                if (this.value == "other") {
                    //toggle textbox visibility
                    $("#other-text").toggle();
                }
            });
        });
    </script>

    <script>
        function myFunction() {

            var x = document.getElementById('any_date');
            var y = document.getElementById('present_date');
            if (x.style.display === 'none') {
                x.style.display = 'block';
                y.style.display = 'none';
                //document.getElementById("myBtn").disabled = true;
            } else {
                x.style.display = 'none';
                document.getElementById('any_month').value = '';
                document.getElementById('any_year').value = '';

                y.style.display = 'block';
            }


        }
    </script>
@endsection