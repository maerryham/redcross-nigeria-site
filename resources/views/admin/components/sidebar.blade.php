<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            {{--<li class=" nav-item"><a href="index-2.html"><i class="la la-home"></i><span class="menu-title" data-i18n="nav.dash.main">Dashboard</span><span class="badge badge badge-info badge-pill float-right mr-2">3</span></a>--}}
                {{--<ul class="menu-content">--}}
                    {{--<li><a class="menu-item" href="dashboard-ecommerce.html" data-i18n="nav.dash.ecommerce">eCommerce</a>--}}
                    {{--</li>--}}
                    {{--<li><a class="menu-item" href="dashboard-crypto.html" data-i18n="nav.dash.crypto">Crypto</a>--}}
                    {{--</li>--}}
                    {{--<li class="active"><a class="menu-item" href="dashboard-sales.html" data-i18n="nav.dash.sales">Sales</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
<?php $user = Auth::user() ?>
            @if ($user->isAdmin())
                <li class="nav-item <?php if($page !='' && $page == 'home'){ echo 'active';}else{ echo '';} ?>">
                        <a href="{{route('home')}}" data-i18n="nav.templates.vert.main">Home Page</a>
                </li>
                <li class=" nav-item"><a class="menu-item" href="#" data-i18n="nav.templates.horz.main">About us page</a>
                    <ul class="menu-content">
                        <li  class="<?php if($page !='' && $page == 'about'){ echo 'active';}else{ echo '';} ?>"><a class="menu-item" href="{{route('admin/about')}}">About us</a>
                        </li>
                        <li class="<?php if($page !='' && $page == 'history'){ echo 'active';}else{ echo '';} ?>"><a class="menu-item" href="{{route('admin/history')}}">Our History</a>
                        </li>
                        <li class="<?php if($page !='' && $page == 'gallery'){ echo 'active';}else{ echo '';} ?>"><a class="menu-item" href="{{route('admin/gallery')}}">Our Gallery</a>
                        </li>
                        <li class="<?php if($page !='' && $page == 'download_page'){ echo 'active';}else{ echo '';} ?>"><a class="menu-item" href="{{route('admin/download_page')}}">Download Page</a>
                        </li>

                    </ul>
                </li>
                <li class=" nav-item"><a href="#" data-i18n="nav.templates.horz.main">News</a>
                    <ul class="menu-content">
                        <li class="<?php if($page !='' && $page == 'news'){ echo 'active';}else{ echo '';} ?>"><a class="menu-item" href="{{route('admin/news')}}" data-i18n="nav.templates.vert.classic_menu">All News</a>
                        </li>
                        <li class="<?php if($page !='' && $page == 'news_upload'){ echo 'active';}else{ echo '';} ?>"><a class="menu-item" href="{{route('admin/news_upload')}}" data-i18n="nav.templates.vert.classic_menu">Upload News</a>
                        </li>
                        <li class="<?php if($page !='' && $page == 'newsletter'){ echo 'active';}else{ echo '';} ?>"><a class="menu-item" href="{{route('admin/newsletter')}}" data-i18n="nav.templates.vert.classic_menu">Newsletter</a>
                        </li>
                        <li class="<?php if($page !='' && $page == 'press_release'){ echo 'active';}else{ echo '';} ?>"><a class="menu-item" href="{{route('admin/press_release')}}">Press Release</a>
                        </li>
                    </ul>
                </li>
                <li><a class="menu-item" href="#" data-i18n="nav.templates.horz.main">Board Members</a>
                    <ul class="menu-content">
                    <li class="<?php if($page !='' && $page == 'board_members_upload'){ echo 'active';}else{ echo '';} ?>"><a class="menu-item" href="{{route('admin/board_members_upload')}}" data-i18n="nav.templates.vert.classic_menu">Add New Board Members</a>
                    </li>
                    <li class="<?php if($page !='' && $page == 'board_members'){ echo 'active';}else{ echo '';} ?>"><a class="menu-item" href="{{route('admin/board_members')}}" data-i18n="nav.templates.vert.classic_menu">View all Board Members</a>
                    </li>
                    </ul>
                </li>
                <li><a class="menu-item" href="#" data-i18n="nav.templates.horz.main">Head of Departments</a>
                    <ul class="menu-content">
                    <li class="<?php if($page !='' && $page == 'head_of_departments_upload'){ echo 'active';}else{ echo '';} ?>"><a class="menu-item" href="{{route('admin/head_of_departments_upload')}}" data-i18n="nav.templates.vert.classic_menu">Add New Head of Departments</a>
                    </li>
                    <li class="<?php if($page !='' && $page == 'head_of_departments'){ echo 'active';}else{ echo '';} ?>"><a class="menu-item" href="{{route('admin/head_of_departments')}}" data-i18n="nav.templates.vert.classic_menu">View all Head of Departments</a>
                    </li>
                    </ul>
                </li>



            <li class="<?php if($page !='' && $page == 'movement_partners'){ echo 'active';}else{ echo '';} ?> nav-item"><a href="{{route('admin/movement_partners')}}"><i class="la la-support"></i><span class="menu-title" data-i18n="nav.support_raise_support.main">Movement Partners</span></a>
            </li>
            <li class="<?php if($page !='' && $page == 'faqs'){ echo 'active';}else{ echo '';} ?> nav-item"><a href="{{route('admin/faqs')}}"><i class="la la-support"></i><span class="menu-title" data-i18n="nav.support_raise_support.main">Frequently Asked Questions</span></a>
            </li>
            @endif

            @if (!$user->isAdmin())
            <li><a class="menu-item" href="#" data-i18n="nav.templates.horz.main">Branch Profile</a>
                <ul class="menu-content">

                    <li class="<?php if($page !='' && $page == 'branch_profile'){ echo 'active';}else{ echo '';} ?>"><a class="menu-item" href="{{route('admin/branch_profile')}}" data-i18n="nav.templates.vert.classic_menu">View all  Branch Profile </a>
                    </li>
                    <li class="<?php if($page !='' && $page == 'branch_profile_upload'){ echo 'active';}else{ echo '';} ?>"><a class="menu-item" href="{{route('admin/branch_profile_upload')}}" data-i18n="nav.templates.vert.classic_menu">Upload Branch Profile</a>
                    </li>

                </ul>
            </li>

            @endif


        </ul>
    </div>
</div>
