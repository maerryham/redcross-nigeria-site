
@extends('admin.layouts.base')

@section('main-section')
    <div class="content-body">
        <!-- Revenue, Hit Rate & Deals -->

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="horz-layout-basic"></h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="{{route('admin/about')}}">View About us page</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collpase show">
                        <div class="card-body">

                            <form class="form form-horizontal" method="post" action="{{route('about_edit')}}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <h4 class="form-section">About us page</h4>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Who we are</label>
                                        <div class="col-md-9" >
                                            {{--<input type="text" id="projectinput1" class="form-control" value="{{$page_data->who_we_are}}" name="fname">--}}
                                            <textarea id="project9" rows="15" class="form-control" name="who_we_are">{{$page_data->who_we_are}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput2">Our Mission</label>
                                        <div  class="col-md-9" >

                                                <textarea id="project99" rows="15" class="form-control" name="our_mission">{{$page_data->our_mission}}</textarea>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput3">Our Vision</label>
                                        <div class="col-md-9">

                                                <textarea id="editor1" rows="15" class="form-control" name="our_vision">{{$page_data->our_vision}}</textarea>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput3">Image</label>
                                        <div class="col-md-9">
                                            <img class="img-responsive" width="100" src="{{asset('img/about/'.$page_data->image)}}"
                                                 alt="Image">

                                            <input type="file" id="projectinput1" class="form-control" name="image">

                                        </div>
                                    </div>



                                    <div class="form-actions">
                                    {{--<button type="button" class="btn btn-warning mr-1">--}}
                                    {{--<i class="ft-x"></i> Cancel--}}
                                    {{--</button>--}}
                                   <center> <button type="submit" class="btn btn-primary center">
                                    <i class="la la-check-square-o"></i> Update About Page
                                    </button></center>
                                    </div>
                                </div></form>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>

@endsection
@section('style-section')

@endsection
@section('script-section')

    <script>
        $(document).ready(function() {
            $('#editor1').trumbowyg();
            $('#project9').trumbowyg();
            $('#project99').trumbowyg();
            // $('#editor2').trumbowyg();

        });
    </script>

@endsection