
@extends('admin.layouts.base')

@section('main-section')
<div class="content-body">
    <!-- Revenue, Hit Rate & Deals -->

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="horz-layout-basic"></h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="{{route('admin/document_report')}}">View Annnal Report</a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collpase show">
                    <div class="card-body">
                        <div class="" id="message">

                            @if(Session::has('message'))
                                <div class="alert alert-success" >
                                    {{ Session::get('message') }}
                                </div>
                            @endif
                        </div>
                        <form class="form form-horizontal"  method="post" action="{{route('admin/document_report_edit')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-body">
                                <h4 class="form-section">Annnal Report for Download</h4>
                                {{--<div class="form-group row">--}}
                                    {{--<label class="col-md-3 label-control" for="projectinput1">Annual Report</label>--}}
                                    {{--<div class="col-md-9">--}}
                                        {{--{{$page_data->annual_report}}--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <div class="form-group row">
                                    <label class="col-md-3 label-control" for="projectinput2">Annnal Report</label>
                                    <div class="col-md-9">
                                        {{$page_data->annual_report}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 label-control" for="projectinput3">Choose new Report</label>
                                    <div class="col-md-9">


                                        <input type="file" id="projectinput1" class="form-control" name="annual_report">

                                    </div>
                                </div>



                                <div class="form-actions">
                                    {{--<button type="button" class="btn btn-warning mr-1">--}}
                                    {{--<i class="ft-x"></i> Cancel--}}
                                    {{--</button>--}}
                                    <center> <button type="submit" class="btn btn-primary center">
                                            <i class="la la-check-square-o"></i> Upload New Annual Report
                                        </button></center>
                                </div>
                            </div></form>
                    </div>
                </div>
            </div>
        </div>
    </div>



</div>

@endsection
