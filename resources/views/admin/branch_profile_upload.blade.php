
@extends('admin.layouts.base')

@section('main-section')
    <div class="content-body">
        <!-- Revenue, Hit Rate & Deals -->

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="horz-layout-basic"></h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="{{route('admin/branch_profile')}}">View All {{Auth::user()->name}} Branch Profile</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collpase show">
                        <div class="card-body">
                            <div class="" id="message">

                                @if(Session::has('message'))
                                    <div class="alert alert-success" >
                                        {{ Session::get('message') }}
                                    </div>
                                @endif
                            </div>

                            <form class="form form-horizontal" method="post" action="branch_profile_upload" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <span id="another" class="pull-right" style="font-size: 12px; margin: 10px">
                                        <input type="checkbox" onchange="myFunction()"  name="" id="choose_month" /> Choose Another Month?</span>
                                    <h4 class="form-section">Upload {{Auth::user()->name}} Branch Profile
                                        <span id="present_date" class="pull-right"> {{date('F')}}, {{date('Y')}} </span>
                                       <span id="any_date" class="pull-right" style="display: none">
                                           <select name="any_month" id="any_month">
                                            <option value="">Choose Month</option>
                                                   <option value="01">January</option>
                                                   <option value="02">Febraury</option>
                                                   <option value="03">March</option>
                                                   <option value="04">April</option>
                                                   <option value="05">May</option>
                                                   <option value="06">June</option>
                                                   <option value="07">July</option>
                                                   <option value="08">August</option>
                                                   <option value="09">September</option>
                                                   <option value="10">October</option>
                                                   <option value="11">November</option>
                                                   <option value="12">December</option>

                                        </select>
                                        <select name="any_year" id="any_year">
                                            <option  value="">Choose Year</option>
                                                <?php $i = date('Y');
                                                while($i > 2000) { ?>
                                                    <option value="2019">{{$i}} </option>
                                               <?php
                                                $i--;
                                                }?>

                                        </select>
                                       </span>
                                    </h4>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Branch Profile Subject</label>
                                        <div class="col-md-9" >
                                            <input type="text" id="projectinput1" class="form-control" placeholder="Enter Your Branch Profile Subject" name="subject">

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput2">Branch Profile Content/<br>Description</label>
                                        <div  class="col-md-9" >

                                            <textarea id="editor1" rows="15" placeholder="Enter your Branch Profile Content" class="form-control" name="contents" required></textarea>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Branch Services</label>
                                        <div class="col-md-9" >




                                            <input type="checkbox" value="First Aid Training" name="branch_services[]" class="animals" /> First Aid Training <br>
                                            <input type="checkbox" value="Emergency calls" name="branch_services[]" class="animals" /> Emergency calls<br>
                                            <input type="checkbox" value="Health Outreach"  name="branch_services[]" class="animals" /> Health Outreach <br>
                                            <input type="checkbox" value="Ambulance" name="branch_services[]" class="animals" /> Ambulance <br>
                                            <input type="checkbox" value="other" id="other" class="animals" />
                                            <label for="other">Others</label>

                                            <input id='other-text' type="text" style="display: none" class="form-control" placeholder="Please Enter Other Branch Services, Separate each service with a comma" name="others_services">

                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Volunteer Strength</label>
                                        <div class="col-md-9" >
                                            <input type="number" id="projectinput1" class="form-control" placeholder="Enter the Number of your Branch Volunteer Strength" name="volunteer_strength">

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Number of Active Volunteer</label>
                                        <div class="col-md-9" >
                                            <input type="number" id="projectinput1" class="form-control" placeholder="Enter the Number of your Branch Active Volunteer" name="active_volunteer">

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Video Link</label>
                                        <div class="col-md-9" >
                                            <input type="text" id="projectinput1" class="form-control" placeholder="Enter Your Branch Youtube Video Link for the Month" name="video_link">

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput3">Select Images for Slide</label>
                                        <div class="col-md-9">

                                            <input type="file" id="projectinput1" class="form-control" name="images[]"  multiple>


                                        </div>
                                    </div>




                                    <div class="form-actions">
                                        {{--<button type="button" class="btn btn-warning mr-1">--}}
                                        {{--<i class="ft-x"></i> Cancel--}}
                                        {{--</button>--}}
                                       <center> <button type="submit" class="btn btn-primary center">
                                        <?php
                                               $date = date('Y-m');
                                               $month = strftime("%b", strtotime($date));
                                               $year = strftime("%b", strtotime($date));
                                        ?>
                                            <i class="la la-check-square-o"></i> Upload Branch Profile
                                        </button>
                                       </center>
                                    </div>
                                </div></form>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>

@endsection
@section('style-section')

@endsection
@section('script-section')
    <script>
        $(document).ready(function() {
            $('#editor1').trumbowyg();
            // $('#editor2').trumbowyg();

        });
    </script>

    <script>
        $(document).ready(function() {
            $("#other").on("change", function() {
                // $(".animals").change(function () {
                //check if the selected option is others
                if (this.value == "other") {
                    //toggle textbox visibility
                    $("#other-text").toggle();
                }
            });
        });
    </script>

    <script>
        function myFunction() {

            var x = document.getElementById('any_date');
            var y = document.getElementById('present_date');
            if (x.style.display === 'none') {
                x.style.display = 'block';
                y.style.display = 'none';
                //document.getElementById("myBtn").disabled = true;
            } else {
                x.style.display = 'none';
                document.getElementById('any_month').value = '';
                document.getElementById('any_year').value = '';

                y.style.display = 'block';
            }


        }
    </script>
@endsection