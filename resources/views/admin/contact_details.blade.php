
@extends('admin.layouts.base')

@section('main-section')
<div class="content-body">
    <!-- Revenue, Hit Rate & Deals -->

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="horz-layout-basic"></h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="{{route('admin/contact_details_edit')}}">Edit Contact Details</a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collpase show">
                    <div class="card-body">
                        <div class="" id="message">

                            @if(Session::has('message'))
                                <div class="alert alert-success" >
                                    {{ Session::get('message') }}
                                </div>
                            @endif
                        </div>
                        <form class="form form-horizontal">
                            <div class="form-body">
                                <h4 class="form-section">Home/Contact Details</h4>
                                <div class="form-group row">
                                    <label class="col-md-3 label-control" for="projectinput1">Address</label>
                                    <div class="col-md-9">
                                        {{$page_data->address}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 label-control" for="projectinput2">Email</label>
                                    <div class="col-md-9">
                                        {{$page_data->email}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 label-control" for="projectinput3">Phone Number</label>
                                    <div class="col-md-9">
                                        {{$page_data->phone}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 label-control" for="projectinput3">Facebook Address</label>
                                    <div class="col-md-9">
                                        {{$page_data->facebook}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 label-control" for="projectinput3">Twitter Address</label>
                                    <div class="col-md-9">
                                        {{$page_data->twitter}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 label-control" for="projectinput3">Instagram Address</label>
                                    <div class="col-md-9">
                                        {{$page_data->instagram}}
                                    </div>
                                </div>
                                <h4 class="form-section">Flash Messages Header</h4>
                                <div class="form-group row">
                                    <label class="col-md-3 label-control" for="projectinput3">First Message</label>
                                    <div class="col-md-9">
                                        {{$page_data->first_message_h}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 label-control" for="projectinput3">Second Message</label>
                                    <div class="col-md-9">
                                        {{$page_data->second_message_h}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 label-control" for="projectinput3">Third Message</label>
                                    <div class="col-md-9">
                                        {{$page_data->third_message_h}}
                                    </div>
                                </div>
                                <h4 class="form-section">Flash Messages Footer</h4>
                                <div class="form-group row">
                                    <label class="col-md-3 label-control" for="projectinput3">First Message</label>
                                    <div class="col-md-9">
                                        {{$page_data->first_message_f}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 label-control" for="projectinput3">Second Message</label>
                                    <div class="col-md-9">
                                        {{$page_data->second_message_f}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 label-control" for="projectinput3">Third Message</label>
                                    <div class="col-md-9">
                                        {{$page_data->third_message_f}}
                                    </div>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



</div>

@endsection
