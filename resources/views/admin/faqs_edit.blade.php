
@extends('admin.layouts.base')

@section('main-section')
    <div class="content-body">
        <!-- Revenue, Hit Rate & Deals -->

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="horz-layout-basic"></h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="{{route('admin/faqs')}}">View All FAQS</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collpase show">
                        <div class="card-body">

                            <form class="form form-horizontal" method="post" action="../faqs_edit" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <h4 class="form-section">Edit FAQS</h4>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">FAQS Question</label>
                                        <div class="col-md-9" >
                                            <input type="text" id="projectinput1" class="form-control" placeholder="Enter Your FAQ Question" name="question" value="{{$press_release->question}}">
                                            <input type="text" id="projectinput1" class="form-control" name="id" style="display: none;" value="{{$press_release->id}}" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput2">FAQS answer</label>
                                        <div  class="col-md-9" >

                                            <textarea id="editor1" rows="15" placeholder="Enter your FAQ answer" class="form-control" name="answer" required>{{$press_release->answer}}</textarea>

                                        </div>
                                    </div>

                                    {{--<div class="form-group row">--}}
                                    {{--<label class="col-md-3 label-control" for="projectinput3">Image</label>--}}
                                    {{--<div class="col-md-9">--}}

                                    {{--<input type="file" id="projectinput1" class="form-control" name="image">--}}

                                    {{--</div>--}}
                                    {{--</div>--}}




                                    <div class="form-actions">
                                        {{--<button type="button" class="btn btn-warning mr-1">--}}
                                        {{--<i class="ft-x"></i> Cancel--}}
                                        {{--</button>--}}
                                        <button type="submit" class="btn btn-primary pull-right">
                                            <i class="la la-check-square-o"></i> Update FAQ
                                        </button>
                                    </div>
                                </div></form>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>

@endsection
@section('style-section')
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
@endsection
@section('script-section')
    <script>
        $(document).ready(function() {
            $('#editor1').trumbowyg();
            // $('#editor2').trumbowyg();

        });
    </script>
@endsection