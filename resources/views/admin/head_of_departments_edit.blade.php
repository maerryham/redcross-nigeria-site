
@extends('admin.layouts.base')

@section('main-section')
    <div class="content-body">
        <!-- Revenue, Hit Rate & Deals -->

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="horz-layout-basic"></h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="{{route('admin/head_of_departments')}}">View Head of Departments</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collpase show">
                        <div class="card-body">

                            <form class="form form-horizontal" method="post" action="../head_of_departments_edit" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <h4 class="form-section">Edit Head of Department</h4>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Head of Department Full Name</label>
                                        <div class="col-md-9" >
                                            <input type="text" id="projectinput1" class="form-control" value="{{$board_members->full_name}}" name="full_name">

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Sex</label>
                                        <div class="col-md-9" >
                                            {{--<input type="text" id="projectinput1" class="form-control" placeholder="Enter The Head of Departments Full Name" name="full_name">--}}
                                            <select name="sex" class="form-control" >
                                                <option value="{{$board_members->sex}}"  >{{$board_members->sex}}</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">State of Origin</label>
                                        <div class="col-md-9" >
                                            {{--<input type="text" id="projectinput1" class="form-control" placeholder="Enter The Head of Departments Full Name" name="full_name">--}}

                                            <select class="form-control" name="state_id">
                                                <option value="">Choose State of Origin</option>
                                                @foreach($states as $state)
                                                    <option @if($state->id == $board_members->state_id) {{"selected"}} @endif value="{{$state->id}}">{{$state->state_name}} State</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Marital Status</label>
                                        <div class="col-md-9" >
                                            {{--<input type="text" id="projectinput1" class="form-control" placeholder="Enter The Head of Departments Full Name" name="full_name">--}}
                                            <select name="marital_status" class="form-control" >
                                                <option value="{{$board_members->marital_status}}"  >{{$board_members->marital_status}}</option>
                                                <option value="Single">Single</option>
                                                <option value="Married">Married</option>
                                                <option value="Others">Others</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Profession</label>
                                        <div class="col-md-9" >
                                            <input type="text" id="projectinput1" class="form-control" value="{{$board_members->profession}}"   placeholder="Enter The Head of Departments Profession" name="profession">

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Qualification</label>
                                        <div class="col-md-9" >
                                            <input type="text" id="projectinput1" class="form-control" value="{{$board_members->qualification}}"  placeholder="Enter The Head of Departments Qualification" name="qualification">

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Email</label>
                                        <div class="col-md-9" >
                                            <input type="text" id="projectinput1" class="form-control" value="{{$board_members->email}}" name="email">

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput2">Head of Department Post</label>
                                        <div  class="col-md-9" >
                                            <input type="text" id="projectinput1" class="form-control" value="{{$board_members->post}}" name="post">


                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput3">Picture</label>
                                        <div class="col-md-9">
                                            <img class="img-responsive" width="100" src="{{asset('img/head_of_departments/'.$board_members->image)}}"
                                                 alt="Image">

                                            <input type="file" id="projectinput1" class="form-control" name="image">
                                            <input type="text" id="projectinput1" class="form-control" name="id" style="display: none;" value="{{$board_members->id}}" required>

                                        </div>
                                    </div>




                                    <div class="form-actions">
                                        {{--<button type="button" class="btn btn-warning mr-1">--}}
                                        {{--<i class="ft-x"></i> Cancel--}}
                                        {{--</button>--}}
                                        <button type="submit" class="btn btn-primary pull-right">
                                            <i class="la la-check-square-o"></i> Update Head of Department
                                        </button>
                                    </div>
                                </div></form>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>

@endsection
@section('style-section')
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
@endsection
@section('script-section')
    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
    <script>
        var quill = new Quill('#editor', {
            theme: 'snow'
        });
    </script>
@endsection