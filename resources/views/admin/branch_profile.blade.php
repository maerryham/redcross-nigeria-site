
@extends('admin.layouts.base')

@section('main-section')
    <div class="content-body">
        <!-- Revenue, Hit Rate & Deals -->


        <div class="row">
            <div id="recent-sales" class="col-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Branch Profile</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                            <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right"
                            href="{{route('admin/branch_profile_upload')}}" >Upload Branch Profile</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content mt-1">
                        <div class="" id="message">

                            @if(Session::has('message'))
                                <div class="alert alert-success" >
                                    {{ Session::get('message') }}
                                </div>
                            @endif
                        </div>
                        <div class="table-responsive">
                            <table id="recent-orders" class="table table-hover table-xl mb-0">

                                @if(count($branch_profile) == 0)
                                    <h4 style="text-align: center">No Branch Profile Uploaded, Please Click Upload by the sidebar to upload {{Auth::user()->name}} Branch Profile</h4>
                                @else
                                <thead>
                                <tr>
                                    <th class="border-top-0">S/N</th>
                                    <th class="border-top-0">Date</th>
                                    <th class="border-top-0">Title</th>
                                    <th class="border-top-0">Action </th>

                                </tr>
                                </thead>
                                <tbody><?php $s=0 ?>
                                @foreach($branch_profile as $gallery)
                                    <?php
                                    $year = strftime("%Y", strtotime($gallery->date));
                                    $month = strftime("%B", strtotime($gallery->date));

                                    $s++ ?>
                                    <tr>
                                        <td class="text-truncate">{{$s}}</td>
                                        <td class="text-truncate">{{$month}}, {{$year}}</td>
                                        <td class="text-truncate">{{$gallery->subject}}</td>

                                        <td>
                                            <a href="{{route('admin/branch_profile')}}/{{$gallery->id}}"><button  type="button" class="btn btn-sm btn-outline-info round">View</button></a>
                                            <a href="{{route('admin/branch_profile')}}_edit/{{$gallery->id}}"><button  type="button" class="btn btn-sm btn-outline-success round">Edit</button></a>
                                            <button type="button" onclick="delete_branch_profile({{$gallery->id}});" class="btn btn-sm btn-outline-danger round">Delete</button>
                                        </td>

                                    </tr>
                                @endforeach

                                </tbody>
                                @endif
                            </table>
                        </div>

                    </div>
                    <center>{{$branch_profile->links() }}</center>
                </div>
            </div>
        </div>


    </div>










@endsection



