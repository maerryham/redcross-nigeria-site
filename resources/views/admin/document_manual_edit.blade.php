
@extends('admin.layouts.base')

@section('main-section')
    <div class="content-body">
        <!-- Revenue, Hit Rate & Deals -->

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="horz-layout-basic"></h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="{{route('admin/download_page')}}">View Download Page</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collpase show">
                        <div class="card-body">

                            <form class="form form-horizontal" method="post" action="../download_page_edit" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <h4 class="form-section">Edit Download Page</h4>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Document Title</label>
                                        <div class="col-md-9" >
                                            <input type="text" id="projectinput1" class="form-control" value="{{$board_members->title}}" name="title">

                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput3">Document</label>
                                        <div class="col-md-9">
                                        <a href="{{asset('img/board_members/'.$board_members->file_name)}}"><i class="fa fa-download"></i>{{$board_members->file_name}}</a>


                                            <input type="file" id="projectinput1" class="form-control" name="image">
                                            {{--<input type="hidden" id="projectinput1" class="form-control" name="id" value="{{$board_members->id}}">--}}
                                            <input type="text" id="projectinput1" class="form-control" name="id" style="display: none;" value="{{$board_members->id}}" required>

                                        </div>
                                    </div>




                                    <div class="form-actions">
                                        {{--<button type="button" class="btn btn-warning mr-1">--}}
                                        {{--<i class="ft-x"></i> Cancel--}}
                                        {{--</button>--}}
                                        <button type="submit" class="btn btn-primary pull-right">
                                            <i class="la la-check-square-o"></i> Update Download Page
                                        </button>
                                    </div>
                                </div></form>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>

@endsection
@section('style-section')
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
@endsection
@section('script-section')
    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
    <script>
        var quill = new Quill('#editor', {
            theme: 'snow'
        });
    </script>
@endsection