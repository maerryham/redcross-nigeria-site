

@extends('admin.layouts.base')

@section('main-section')
    <div class="content-body">
        <!-- Revenue, Hit Rate & Deals -->


        <div class="row">
            <div id="recent-sales" class="col-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Frequently Asked Questions</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                            <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right"
                            href="{{route('admin/faqs_upload')}}">Upload FAQS</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content mt-1">
                        <div class="" id="message">

                            @if(Session::has('message'))
                                <div class="alert alert-success" >
                                    {{ Session::get('message') }}
                                </div>
                            @endif
                        </div>
                        <div class="table-responsive">
                            <table id="recent-orders" class="table table-hover table-xl mb-0">
                                <thead>
                                <tr>
                                    <th class="border-top-0">S/N</th>
                                    <th class="border-top-0">Question</th>
                                    <th class="border-top-0">Answer</th>
                                    {{--<th class="border-top-0">Image</th>--}}
                                    <th class="border-top-0">Action</th>

                                </tr>
                                </thead>
                                <tbody><?php $s=0 ?>
                                @foreach($press_release as $new)
                                    <?php $s++ ?>
                                    <tr>
                                        <td class="text-truncate">{{$s}}</td>
                                        <td class="text-truncate">{{$new->question}}</td>
                                        <td class="text-truncate">{!!substr($new->answer,0,50)!!} ...</td>
                                        {{--<td class="text-truncate p-1">--}}



                                            {{--<img class="img-responsive" width="100" src="{{asset('img/press_release/'.$new->image)}}"--}}
                                                 {{--alt="Image">--}}



                                        {{--</td>--}}
                                        <td>
                                            <a href="faqs_edit/{{$new->id}}"><button  type="button" class="btn btn-sm btn-outline-success round">Edit</button></a>
                                            <button type="button" onclick="delete_faqs({{$new->id}});" class="btn btn-sm btn-outline-danger round">Delete</button>
                                        </td>

                                    </tr>
                                @endforeach

                                </tbody>

                            </table>
                        </div>

                    </div>
                    <center>{{$press_release->links() }}</center>
                </div>
            </div>
        </div>


    </div>










@endsection
