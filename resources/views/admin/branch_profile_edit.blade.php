
@extends('admin.layouts.base')

@section('main-section')
    <div class="content-body">
        <!-- Revenue, Hit Rate & Deals -->

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="horz-layout-basic"></h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="{{route('admin/branch_profile')}}">View All {{Auth::user()->name}} Branch Profile</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collpase show">
                        <div class="card-body">
                            <div class="" id="message">

                                @if(Session::has('message'))
                                    <div class="alert alert-success" >
                                        {{ Session::get('message') }}
                                    </div>
                                @endif
                            </div>

                            <form class="form form-horizontal" method="post" action="../branch_profile_edit" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <?php
                                    $year = strftime("%Y", strtotime($branch_profile->date));
                                    $month = strftime("%B", strtotime($branch_profile->date));
                                    ?>

                                    <h4 class="form-section">Edit {{Auth::user()->name}} Branch Profile

                                       <p class="pull-right" >
                                           <select name="any_month">
                                               <?php $date = date_parse($month);
                                               //var_dump($date['month']); ?>

                                                   <option value="{{$date['month']}}" selected>{{$month}}</option>
                                                   <option value="01">January</option>
                                                   <option value="02">Febraury</option>
                                                   <option value="03">March</option>
                                                   <option value="04">April</option>
                                                   <option value="05">May</option>
                                                   <option value="06">June</option>
                                                   <option value="07">July</option>
                                                   <option value="08">August</option>
                                                   <option value="09">September</option>
                                                   <option value="10">October</option>
                                                   <option value="11">November</option>
                                                   <option value="12">December</option>

                                        </select>
                                        <select name="any_year">

                                            <option value="{{$year}}" selected>{{$year}}</option>
                                                <?php $i = date('Y');
                                                while($i > 2000) { ?>
                                                    <option value="2019">{{$i}} </option>
                                               <?php
                                                $i--;
                                                }?>

                                        </select>
                                       </p>
                                    </h4>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Branch Profile Subject</label>
                                        <div class="col-md-9" >
                                            <input type="text" id="projectinput1" class="form-control"  name="subject" value="{{$branch_profile->subject}}">
                                            <input type="text" id="projectinput1" class="form-control" name="id" style="display:none" value="{{$branch_profile->id}}">

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput2">Branch Profile Content/<br>Description</label>
                                        <div  class="col-md-9" >

                                            <textarea id="editor1" rows="15" class="form-control" name="contents" required>{!! $branch_profile->content !!}</textarea>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Branch Services</label>
                                        <div class="col-md-9" >

                                            <?php $json_output = json_decode($branch_profile->branch_services, true);
                                            ?>
                                            @foreach($json_output as $value)
                                                @if($value != '')

                                                        <input type="checkbox" value="{{$value}}" name="branch_services[]" class="animals" checked /> {{$value}} <br>
                                                @endif
                                            @endforeach


                                            <input type="checkbox" value="other" id="other" class="animals" />
                                            <label for="other">Others</label>

                                            <input id='other-text' type="text" style="display: none" class="form-control" placeholder="Please Enter Other Branch Services, Separate each service with a comma" name="others_services">

                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Volunteer Strength</label>
                                        <div class="col-md-9" >
                                            <input type="number" id="projectinput1" class="form-control" value="{{$branch_profile->volunteer_strength}}" name="volunteer_strength">

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Number of Active Volunteer</label>
                                        <div class="col-md-9" >
                                            <input type="number" id="projectinput1" class="form-control" value="{{$branch_profile->active_volunteer}}" name="active_volunteer">

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Video Link</label>
                                        <div class="col-md-9" >
                                            <div class="embed-responsive embed-responsive-16by9" style="margin-bottom: 10px">
                                                <iframe class="embed-responsive-item" src="{{$branch_profile->video_link}}" allowfullscreen></iframe>
                                            </div>
                                            <input type="text" id="projectinput1" class="form-control" value="{{$branch_profile->video_link}}" name="video_link">

                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput3">Images for Slide</label>
                                        <div class="col-md-9">
                                            <ul class="row" style="list-style-type: none">
                                            <?php $json_output = json_decode($branch_profile->images, true);
                                            $output_image = [];
                                            ?>
                                            @foreach($json_output as $value)
                                                @if($value != '')
                                                    <li class="col-md-3">
                                                    <img class="img-thumbnail" style="max-width:100px" src="{{asset('img/branch_profile/'.$value)}}">
                                                        <input type="text" id="projectinput1" style="display: none" width="60px" value="{{$value}}" class="form-control" name="images[]" value="{{$value}}">
                                                        <span class="btn btn-outline-info">Remove</span>
                                                    <?php $output_image[] = $value; ?>
                                                    <li>
                                                @endif
                                            @endforeach
                                            </ul>
                                            <h5>Add More images below</h5>
                                            <input type="file" id="projectinput1" class="form-control" name="new_images[]" value="" multiple>


                                        </div>
                                    </div>




                                    <div class="form-actions">
                                        {{--<button type="button" class="btn btn-warning mr-1">--}}
                                        {{--<i class="ft-x"></i> Cancel--}}
                                        {{--</button>--}}
                                       <center> <button type="submit" class="btn btn-primary center">
                                        <?php
                                               $date = date('Y-m');
                                               $month = strftime("%b", strtotime($date));
                                               $year = strftime("%b", strtotime($date));
                                        ?>
                                            <i class="la la-check-square-o"></i> Update Branch Profile
                                        </button>
                                       </center>
                                    </div>
                                </div></form>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>

@endsection
@section('style-section')

@endsection
@section('script-section')
    <script>
        $(document).ready(function() {
            $('#editor1').trumbowyg();
            // $('#editor2').trumbowyg();

        });
    </script>

    <script>
        $(document).ready(function() {
            $("#other").on("change", function() {
                // $(".animals").change(function () {
                //check if the selected option is others
                if (this.value == "other") {
                    //toggle textbox visibility
                    $("#other-text").toggle();
                }
            });
        });
    </script>

    <script>
        // function myFunction() {
        //
        //     var x = document.getElementById('any_date');
        //     var y = document.getElementById('present_date');
        //     if (x.style.display === 'none') {
        //         x.style.display = 'block';
        //         y.style.display = 'none';
        //         //document.getElementById("myBtn").disabled = true;
        //     } else {
        //         x.style.display = 'none';
        //         document.getElementById('any_month').value = '';
        //         document.getElementById('any_year').value = '';
        //
        //         y.style.display = 'block';
        //     }
        //
        //
        // }
    </script>
    <script>
        function remove() {
            this.parentNode.parentNode.removeChild(this.parentNode);
        }

        var lis = document.querySelectorAll('li');
        var button = document.querySelectorAll('span');

        for (var i = 0, len = lis.length; i < len; i++) {
            button[i].addEventListener('click', remove, false);
            // input[i].value = '';
        }
    </script>
@endsection