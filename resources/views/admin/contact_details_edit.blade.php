
@extends('admin.layouts.base')

@section('main-section')
    <div class="content-body">
        <!-- Revenue, Hit Rate & Deals -->

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="horz-layout-basic"></h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="{{route('home')}}">View Contact Details</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collpase show">
                        <div class="card-body">
                            <div class="" id="message">

                                @if(Session::has('message'))
                                    <div class="alert alert-success" >
                                        {{ Session::get('message') }}
                                    </div>
                                @endif
                            </div>
                            <form class="form form-horizontal" method="post" action="{{route('admin/contact_details_edit')}}" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="form-body">
                                    <h4 class="form-section">Home/Contact Details</h4>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput1">Address</label>
                                        <div class="col-md-9">
                                            <input type="text" id="projectinput1" class="form-control" value="{{$page_data->address}}" name="address">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput2">Email</label>
                                        <div class="col-md-9">
                                            <input type="text" id="projectinput1" class="form-control" value="{{$page_data->email}}" name="email">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput3">Phone Number</label>
                                        <div class="col-md-9">
                                            <input type="text" id="projectinput1" class="form-control" value="{{$page_data->phone}}" name="phone">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput3">Facebook Address</label>
                                        <div class="col-md-9">
                                            <input type="text" id="projectinput1" class="form-control" value="{{$page_data->facebook}}" name="facebook">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput3">Twitter Address</label>
                                        <div class="col-md-9">
                                            <input type="text" id="projectinput1" class="form-control" value="{{$page_data->twitter}}" name="twitter">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput3">Instagram Address</label>
                                        <div class="col-md-9">
                                            <input type="text" id="projectinput1" class="form-control" value="{{$page_data->instagram}}" name="instagram">
                                        </div>
                                    </div>

                                    <h4 class="form-section">Flash Messages Header</h4>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput3">First Message</label>
                                        <div class="col-md-9">

                                            <input type="text" id="projectinput1" class="form-control" value="{{$page_data->first_message_h}}" name="first_message_h">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput3">Second Message</label>
                                        <div class="col-md-9">
                                            <input type="text" id="projectinput1" class="form-control" value="{{$page_data->second_message_h}}" name="second_message_h">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput3">Third Message</label>
                                        <div class="col-md-9">
                                            <input type="text" id="projectinput1" class="form-control" value="{{$page_data->third_message_h}}" name="third_message_h">
                                        </div>
                                    </div>
                                    <h4 class="form-section">Flash Messages Footer</h4>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput3">First Message</label>
                                        <div class="col-md-9">
                                            <input type="text" id="projectinput1" class="form-control" value="{{$page_data->first_message_f}}" name="first_message_f">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput3">Second Message</label>
                                        <div class="col-md-9">
                                            <input type="text" id="projectinput1" class="form-control" value="{{$page_data->second_message_f}}" name="second_message_f">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="projectinput3">Third Message</label>
                                        <div class="col-md-9">
                                            <input type="text" id="projectinput1" class="form-control" value="{{$page_data->third_message_f}}" name="third_message_f">
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        {{--<button type="button" class="btn btn-warning mr-1">--}}
                                        {{--<i class="ft-x"></i> Cancel--}}
                                        {{--</button>--}}
                                        <button type="submit" class="btn btn-primary center">
                                            <i class="la la-check-square-o"></i> Update Contact Details
                                        </button>
                                    </div>

                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>

@endsection
