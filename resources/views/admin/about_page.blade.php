
@extends('admin.layouts.base')

@section('main-section')
<div class="content-body">
    <!-- Revenue, Hit Rate & Deals -->

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="horz-layout-basic"></h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="{{route('about_edit')}}">Edit About us page</a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collpase show">
                    <div class="card-body">
                        <div class="" id="message">

                            @if(Session::has('message'))
                                <div class="alert alert-success" >
                                    {{ Session::get('message') }}
                                </div>
                            @endif
                        </div>
                        <form class="form form-horizontal">
                            <div class="form-body">
                                <h4 class="form-section">About us page</h4>
                                <div class="form-group row">
                                    <label class="col-md-3 label-control" for="projectinput1">Who we are</label>
                                    <div class="col-md-9">
                                        {{$page_data->who_we_are}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 label-control" for="projectinput2">Our Mission</label>
                                    <div class="col-md-9">
                                        {{$page_data->our_mission}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 label-control" for="projectinput3">Our Vision</label>
                                    <div class="col-md-9">
                                        {!!$page_data->our_vision!!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 label-control" for="projectinput3">About Us Image</label>
                                    <div class="col-md-9">

                                        <img class="img-responsive" width="400" src="{{asset('img/about/'.$page_data->image)}}"
                                             alt="About Image">
                                    </div>
                                </div>



                            {{--<div class="form-actions">--}}
                                {{--<button type="button" class="btn btn-warning mr-1">--}}
                                    {{--<i class="ft-x"></i> Cancel--}}
                                {{--</button>--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--<i class="la la-check-square-o"></i> Save--}}
                                {{--</button>--}}
                            {{--</div>--}}
                            </div></form>
                    </div>
                </div>
            </div>
        </div>
    </div>



</div>

@endsection
