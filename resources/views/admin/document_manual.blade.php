
@extends('admin.layouts.base')

@section('main-section')
<div class="content-body">
    <!-- Revenue, Hit Rate & Deals -->

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="horz-layout-basic"></h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="{{route('admin/download_page_upload')}}">Add new Document</a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collpase show">
                    <div class="card-body">
                        <div class="" id="message">

                            @if(Session::has('message'))
                                <div class="alert alert-success" >
                                    {{ Session::get('message') }}
                                </div>
                            @endif
                        </div>
                        <div class="table-responsive">
                            <table id="recent-orders" class="table table-hover table-xl mb-0">
                                <thead>
                                <tr>
                                    <th class="border-top-0">S/N</th>
                                    <th class="border-top-0">Title</th>
                                    {{--<th class="border-top-0">Description</th>--}}
                                    {{--<th class="border-top-0">Image</th>--}}
                                    <th class="border-top-0">Action</th>

                                </tr>
                                </thead>
                                <tbody><?php $s=0 ?>
                                @foreach($page_data as $new)
                                    <?php $s++ ?>
                                    <tr>
                                        <td class="text-truncate">{{$s}}</td>
                                        <td class="text-truncate">{{$new->title}}</td>

                                        <td>
                                            <a href="download_page_edit/{{$new->id}}"><button  type="button" class="btn btn-sm btn-outline-success round">Edit</button></a>
                                            <button type="button" onclick="delete_document({{$new->id}});" class="btn btn-sm btn-outline-danger round">Delete</button>
                                        </td>

                                    </tr>
                                @endforeach

                                </tbody>

                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>



</div>

@endsection
