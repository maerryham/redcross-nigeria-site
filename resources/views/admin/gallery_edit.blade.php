
@extends('admin.layouts.base')

@section('main-section')
    <div class="content-body">
        <!-- Revenue, Hit Rate & Deals -->


        <div class="row">
            <div id="recent-sales" class="col-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit Gallery Image</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            {{--<ul class="list-inline mb-0">--}}
                            {{--<li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right"--}}
                            {{--href="" target="_blank">View all</a></li>--}}
                            {{--</ul>--}}
                        </div>
                    </div>
                    <div class="card-content mt-1">
                        <div class="table-responsive">
                            <table id="recent-orders" class="table table-hover table-xl mb-0">
                                <thead>
                                <tr>
                                    <th class="border-top-0">S/N</th>
                                    <th class="border-top-0">Image</th>
                                    <th class="border-top-0">Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                <form method="post" action="../gallery_edit" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <tr>
                                        <td class="text-truncate">1</td>
                                        <td class="text-truncate p-1">



                                            <img class="img-responsive" width="100" src="{{asset('img/gallery/'.$gallery->image)}}"
                                                 alt="Image">

                                            <input type="file" id="projectinput1" class="form-control" name="image" required>
                                            <input type="text" id="projectinput1" class="form-control" name="id" style="display: none;" value="{{$gallery->id}}" required>

                                        </td>
                                        <td>

                                            <button type="submit" style="position: absolute; bottom: 10px;" class="btn btn-sm btn-outline-success round">Submit</button>
                                        </td>

                                    </tr>
                                </form>
                                {{--@endforeach--}}

                                </tbody>

                            </table>
                        </div>

                    </div>

                </div>
            </div>
        </div>


    </div>










@endsection
