@extends('layouts.base')
@section('main-section')
    <!-- ======= page title part srat ======= -->
    <section class="page-title-area parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- page title -->
                    <div class="page-title">
                        <div class="title">
                            <h2>Archives</h2>
                        </div>
                        <ul class="breadcrumb">
                            <li><a href="{{route('index')}}">Home</a></li>
                            <li><a href="{{route('news')}}">News</a></li>
                            <li class="active">Archives</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="blog-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <!-- section title -->
                    <div class="title">
                        <h2>Archives</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="related-causes col-md-offset-2 col-md-8">

                    <!-- single related causes -->
                    @foreach($archives as $archive)
                        <?php   // $date_det = strftime("%b %d, %Y", strtotime($report->date));
                        $date = strftime("%d", strtotime($archive->date));
                        $month = strftime("%b", strtotime($archive->date));
                        $day = strftime("%A", strtotime($archive->date));
                        $year = strftime("%Y", strtotime($archive->date));
                        // $time =  date("g:i A", strtotime($report->date));

                        $date_det = strftime("%b %d, %Y", strtotime($archive->date)); ?>
                    <div class="related-causes-info">
                        <div class="related-causus-img" style="max-height: 600px; overflow:hidden">
                            <a href="archive/{{$archive->page_link}}">
                                <img src="{{asset('img/gallery/'.$archive->image)}}" alt="">
                            </a>
                        </div>
                        <div class="related-causus-details">
                            <h2><a style="color: #D22034" href="archive/{{$archive->page_link}}">{{$archive->title}}</a></h2>
                            <p>{!! substr($archive->description,0,100) !!}...</p>
                            <p>{{$date}} {{$month}}, {{$year}}</p>
                            <a href="archive/{{$archive->page_link}}" class="donate-btn hvr-shutter-out-horizontal">read more</a>
                        </div>
                    </div>
                    @endforeach

                    <center>{{$archives->links() }}</center>
                </div>

            </div>
        </div>
    </section>

@endsection()