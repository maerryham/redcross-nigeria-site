@extends('layouts.base')
@section('main-section')
    <!-- ======= page title part srat ======= -->
    <section class="page-title-area parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- page title -->
                    <div class="page-title">
                        <div class="title">
                            <h2>7 Fundamental Principles</h2>
                        </div>
                        <ul class="breadcrumb">
                            <li><a href="{{route('index')}}">Home</a></li>
                            <li><a href="{{route('about-us')}}">About us</a></li>
                            <li class="active">7 Fundamental Principles</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="blog-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <!-- section title -->
                    <div class="title">
                        <h2>Fundamental Principles of the Red Cross and Red Crescent</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="related-causes">

                    <!-- single related causes -->
                    <div class="related-causes-info">
                        <div class="related-causus-img">
                            <img src="{{asset('img/principles/humanity.png')}}" alt="">
                        </div>
                        <div class="related-causus-details">
                            <h2>Humanity</h2>
                            <p>The International Red Cross and Red Crescent Movement, born of a desire to bring assistance without discrimination to the wounded on the battlefield, endeavours, in its international and national capacity, to prevent and alleviate human suffering wherever it may be found. Its purpose is to protect life and health and to ensure respect for the human being. It promotes mutual understanding, friendship, cooperation and lasting peace amongst all peoples.</p>
                            {{--<a href="#" class="donate-btn hvr-shutter-out-horizontal">read more</a>--}}
                        </div>
                    </div>
                    <!-- single related causes -->
                    <div class="related-causes-info">
                        <div class="related-causus-img">
                            <img src="{{asset('img/principles/impartiality.png')}}" alt="">
                        </div>
                        <div class="related-causus-details">
                            <h2>Impartiality</h2>
                            <p>It makes no discrimination as to nationality, race, religious beliefs, class or political opinions. It endeavours to relieve the suffering of individuals, being guided solely by their needs, and to give priority to the most urgent cases of distress.</p>

                            <a href="#" class="donate-btn hvr-shutter-out-horizontal">read more</a>
                        </div>
                    </div>
                    <div class="related-causes-info">
                        <div class="related-causus-img">
                            <img src="{{asset('img/principles/neutrality.png')}}" alt="">
                        </div>
                        <div class="related-causus-details">
                            <h2>Neutrality</h2>
                            <p>In order to continue to enjoy the confidence of all, the Movement may not take sides in hostilities or engage at any time in controversies of a political, racial, religious or ideological nature.</p>

                            {{--<a href="#" class="donate-btn hvr-shutter-out-horizontal">read more</a>--}}
                        </div>
                    </div>
                    <div class="related-causes-info">
                        <div class="related-causus-img">
                            <img src="{{asset('img/principles/independence.png')}}" alt="">
                        </div>
                        <div class="related-causus-details">
                            <h2>Independence</h2>
                            <p>The Movement is independent. The National Societies, while auxiliaries in the humanitarian services of their governments and subject to the laws of their respective countries, must always maintain their autonomy so that they may be able at all times to act in accordance with the principles of the Movement. </p>

                            {{--<a href="#" class="donate-btn hvr-shutter-out-horizontal">read more</a>--}}
                        </div>
                    </div>
                    <div class="related-causes-info">
                        <div class="related-causus-img">
                            <img src="{{asset('img/principles/voluntary.png')}}" alt="">
                        </div>
                        <div class="related-causus-details">
                            <h2>Voluntary service</h2>
                            <p>It is a voluntary relief movement not prompted in any manner by desire for gain. </p>

                            {{--<a href="#" class="donate-btn hvr-shutter-out-horizontal">read more</a>--}}
                        </div>
                    </div>
                    <div class="related-causes-info">
                        <div class="related-causus-img">
                            <img src="{{asset('img/principles/unity.png')}}" alt="">
                        </div>
                        <div class="related-causus-details">
                            <h2>Unity</h2>
                            <p>There can be only one Red Cross or one Red Crescent Society in any one country. It must be open to all. It must carry on its humanitarian work throughout its territory. </p>

                            {{--<a href="#" class="donate-btn hvr-shutter-out-horizontal">read more</a>--}}
                        </div>
                    </div>
                    <div class="related-causes-info">
                        <div class="related-causus-img">
                            <img src="{{asset('img/principles/universality.png')}}" alt="">
                        </div>
                        <div class="related-causus-details">
                            <h2>Universality</h2>
                            <p>The International Red Cross and Red Crescent Movement, in which all Societies have equal status and share equal responsibilities and duties in helping each other, is worldwide.</p>

                            {{--<a href="#" class="donate-btn hvr-shutter-out-horizontal">read more</a>--}}
                        </div>
                    </div>







                </div>

            </div>
        </div>
    </section>
    <style>
        .related-causus-details {
            width: 80%;
        }
        .related-causus-img {
            width: 20%;
        }
    </style>

@endsection()
