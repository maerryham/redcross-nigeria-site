@extends('layouts.base')
@section('main-section')
      <!-- ======= page title part srat ======= -->
    <section class="page-title-area parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- page title -->
                    <div class="page-title">
                        <div class="title">
                            <h2>Board Members</h2>
                        </div>
                        <ul class="breadcrumb">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">Board Members</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
      <section id="team" class="team-area section-padding">
          <div class="container">
              <div class="row">
                  <div class="col-md-8 col-md-offset-2 text-center">
                      <!-- section title -->
                      <div class="title">
                          <h2>Our Board Member</h2>
                          {{--<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi tempora veritatis nemo aut ea iusto eos est expedita, quas ab adipisci consectetur tempora jet.</p>--}}
                      </div>
                  </div>
              </div>
              <div class="row">

                  <div class="col-sm-12 col-md-6">
                      <!-- Single Member -->
                      <div class="team-member ">
                          <div class="member-top">
                              <a href="{{asset('img/board_members/'.$boardmember->image)}}" target="_blank" class="pull-right"> <img class="pull-right" src="{{asset('img/board_members/'.$boardmember->image)}}" alt=""></a>

                          </div>

                      </div>
                  </div>
                  <div class="col-sm-12 col-md-6">
                      <div class="member-dec" style="line-height: 20px;">
                          <h4>{{$boardmember->full_name}}</h4>
                          <h6>{{$boardmember->sex}}</h6>
                          <h6>{{$boardmember->state->state_name}} STATE</h6>
                          <h6>{{$boardmember->marital_status}}</h6>
                          <h6>{{$boardmember->profession}}</h6>
                          <h6>{{$boardmember->qualification}}</h6>
                          <h6>{{$boardmember->post}}</h6>
                          <h6>{{$boardmember->reason_for_joining}}</h6>
                      </div>
                  </div>
                  <style>
                      .member-dec h6{
                          margin-bottom: 20px;
                      } .member-dec h4{
                          margin-bottom: 20px;
                      }
                      .member-dec {
                          padding: 100px;
                      }
                      .member-top{
                          max-height: 500px;
                          overflow: hidden;
                      }

                  </style>


              </div>
          </div>
      </section>


@endsection()