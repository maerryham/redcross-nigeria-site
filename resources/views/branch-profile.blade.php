@extends('layouts.base')
@section('main-section')
      <!-- ======= page title part srat ======= -->
    <section class="page-title-area parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- page title -->
                    <div class="page-title">
                        <div class="title">
                            <h2>Branch Profile</h2>
                        </div>
                        <ul class="breadcrumb">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">{{$state->state_name}} Branch Profile</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ======= page title part end ======= -->
     <!-- ======= blog part start ======= -->
    <section class="blog-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <!-- section title -->
                    <div class="title">
                        <h2>{{$state->state_name}}  Branch Profile</h2>
                    </div>
                </div>
            </div>
            <div class="row">


                <div class="col-sm-8">
                    <article class="single-blog-content">
                        {{--<img src="img/news/04.jpg" alt="">--}}
                        <h2>{{$branch_profile->subject}}</h2>
                        <?php
                        $month = strftime("%B", strtotime($branch_profile->date));
                        $year = strftime("%Y", strtotime($branch_profile->date));
                        ?>
                        <div class="blog-get-info">
                            <i class="fa fa-calendar" aria-hidden="true"></i> <span>{{$month}} {{$year}}</span>

                        </div>
                        <p> {!! $branch_profile->content  !!}
                        </p>

                    </article>
                    <h2 class="text-center" style="font-size: 20px; margin: 30px 0 20px; color: #D22034;">Branch Services</h2>
                    <div class="row">

                        <?php $json_output = json_decode($branch_profile->branch_services, true);
                         ?>

                        @foreach($json_output as $value)
                            @if($value != '')
                        <div class="col-md-4 col-sm-6 text-center">
                            <div class="events-author">
                                <a href="#">
                                    <h4>{{$value}}</h4>
                                </a>

                            </div>
                        </div>
                                @endif
                        @endforeach

                    </div>

                    <h2 class="text-center" style="font-size: 20px; margin: 30px 0 20px; color: #D22034;">Branch  Monthly Data</h2>

                   
                    <div class="col-md-6 col-sm-6">
                        <!-- single causes -->
                        <div class="causes-info">
                            <div class="causes-img" >
                                {{--<img src="img/causes/01.jpg" alt="">--}}
                                <div class="project-count-inf" style="">
                                    <span class="counter" data-count="{{$branch_profile->volunteer_strength}}">{{$branch_profile->volunteer_strength}}</span>
                                </div>
                            </div>
                            <div class="causes-details">
                                <h2 ><a style="text-align: center;     margin: 20px 0 0;" href="#">VOLUNTEER  STRENGTH</a></h2>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <!-- single causes -->
                        <div class="causes-info">
                            <div class="causes-img" >
                                {{--<img src="img/causes/01.jpg" alt="">--}}
                                <div class="project-count-inf" style="">
                                    <span class="counter" data-count="{{$branch_profile->active_volunteer}}">{{$branch_profile->active_volunteer}}</span>
                                </div>
                            </div>
                            <div class="causes-details">
                                <h2 ><a style="text-align: center;     margin: 20px 0 0;" href="#">ACTIVE Volunteer for the month</a></h2>

                            </div>
                        </div>
                    </div>
<style>
    .circled{
        z-index: 9;
        background-color: rgba(210, 32, 53, 0.9);
        color: #fff;
        width: 100px;
        height: 100px;
        line-height: 90px;
        left: 50%;
        transform: translate(-50%);
        text-align: center;
        border-radius: 50%;
        border: 5px solid #F6F6F6;
    }
    .counter{
        font-weight: 700;
        font-size: 20px;
    }
    .project-count-inf{
        top:0px;     transform: translate(110%); left:0; z-index: 9;
        background-color: rgba(210, 32, 53, 0.9);    color: #fff;    width: 100px;    height: 100px;    line-height: 90px;    left: 50%;
        text-align: center;    border-radius: 50%;    border: 5px solid #F6F6F6;
    }
</style>
                    <div class="col-md-6">
                        <h4 class="text-center" style="color: #D22034">Address</h4>
                        <h5 class="text-center"> {{$state->state_address}}</h5>
                    </div>
                    <div class="col-md-6">
                        <h4 class="text-center" style="color: #D22034">Phone</h4>
                        <h5 class="text-center"> {{$state->state_phone}}</h5>
                    </div>


                </div>
                <div class="col-sm-4">
                    {{--<h2>upcoming events</h2>--}}
                    <div class="upcoming-events owl-carousel">


                        <?php $json_output = json_decode($branch_profile->images, true);
                        ?>

                        @foreach($json_output as $image)
                            @if($image != '')
                        <div class="single-events events-info">
                            <div class="news-img">
                                <img src="{{asset('img/branch_profile/'.$image)}}" alt="">
                            </div>
                        </div>
                            @endif
                        @endforeach

                    </div>

                    <div class="">
                        <div class="embed-responsive embed-responsive-16by9" style="margin-bottom: 10px">
                            <iframe class="embed-responsive-item" src="{{$branch_profile->video_link}}" allowfullscreen></iframe>
                        </div>
                        {{--<div class="news-img">--}}
                            {{----}}
                            {{--<img src="{{asset('img/news/04.jpg')}}" alt="">--}}
                        {{--</div>--}}
                    </div>
               </div>

               <div class="col-md-12" style="margin-top: 20px">

                   {{--<i class="fa fa-arrow-right"></i>--}}
                   @if($previous == "")
                       <a href="#" class="donate-btn hvr-shutter-out-horizontal left" style="float:left"><i class="fa fa-arrow-left text-white"></i>No Previous Month</a>
                       @else
                   <a href="{{route('branch-profile')}}/{{$state->id}}/{{$previous->id}}" class="donate-btn hvr-shutter-out-horizontal left" style="float:left"><i class="fa fa-arrow-left text-white"></i> Previous Month</a>
                   @endif
                   @if($next == "")
                       <a href="#" class="donate-btn hvr-shutter-in-horizontal right" style="float:right"><i class="fa fa-arrow-right text-white"></i>No Next Month</a>
                   @else
                       <a href="{{route('branch-profile')}}/{{$state->id}}/{{$next->id}}" class="donate-btn hvr-shutter-in-horizontal right" style="float:right"><i class="fa fa-arrow-right text-white"></i> Next Month</a>

                   @endif

               </div>

            </div>

        </div>
    </section>

@endsection()