@extends('layouts.base')
@section('main-section')
      <!-- ======= page title part srat ======= -->
    <section class="page-title-area parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- page title -->
                    <div class="page-title">
                        <div class="title">
                            <h2>News</h2>
                        </div>
                        <ul class="breadcrumb">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">News</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ======= page title part end ======= -->
     <!-- ======= blog part start ======= -->
    <section class="blog-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <!-- section title -->
                    <div class="title">
                        <h2>latest News</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8 col-md-offset-2">
            @foreach($new as $news)
                <?php   // $date_det = strftime("%b %d, %Y", strtotime($report->date));
                     $date = strftime("%d", strtotime($news->date));

                    $day = strftime("%A", strtotime($news->date));
                    $year = strftime("%Y", strtotime($news->date));
                    $month = strftime("%b", strtotime($news->date));
                    // $time =  date("g:i A", strtotime($report->date));

                 $date_det = strftime("%b %d, %Y", strtotime($news->date)); ?>

                    <article class="single-blog-content" style="margin-bottom: 100px">
                        <a href="news/{{$news->page_link}}"><img src="{{asset('img/news/'.$news->image)}}" alt=""></a>
                        <h2> <a style="color: #D22034" href="news/{{$news->page_link}}">
                            {{$news->title}}
                            </a></h2>
                        <div class="blog-get-info">
                            <i class="fa fa-calendar" aria-hidden="true"></i> <span>{{$date}} {{$month}}, {{$year}}</span>
                            {{--<i class="fa fa-clock-o" aria-hidden="true"></i><span> 6.30pm</span>--}}
                        </div>
                        <p>{!!$news->description!!}</p>

                    </article>


                @endforeach
                    <center>{{$new->links()}}</center>

                </div>
            </div>


        </div>
    </section>

@endsection()