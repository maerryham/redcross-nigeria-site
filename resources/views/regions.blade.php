@extends('layouts.base')
@section('style-section')
    <link href="{{ asset('css/jsmaps.css')}}" rel="stylesheet" type="text/css" />
@endsection()
@section('main-section')
   
  <section class="section-padding">
     <div class="container">
        <div class="row">
            <div class="title text-center">
                <h2>Nigeria Redcross regions.</h2>
                <p>Click anywhere on the map to see details of each region</p>
            </div>
            <div class="col-md-10 col-md-offset-1">
                <div class="jsmaps-wrapper" id="nigeria-map"></div>
            </div>    
        </div>
    </div>
  </section>
 
@endsection()
@section('script-section')
    <script src="{{ asset('js/jsmaps-libs.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/jsmaps-panzoom.js')}}"></script>
    <script src="{{ asset('js/jsmaps.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/nigeria.js')}}" type="text/javascript"></script>
    <script type="text/javascript">

        $(function() {
        
        $('#nigeria-map').JSMaps({
            map: 'nigeria'
        });

        });
    </script>   
@endsection()