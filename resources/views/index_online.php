<?php include 'core/init.php';


ob_start();?>
    <!doctype html>
    <html lang="en" class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- ======= titles ======= -->
        <title>The Nigeria Redcross</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="themewarehouse">
        <!-- ======= favicon ======= -->
        <link rel="icon" type="image/png" href="http://redcrossnigeria.org/redcross-nigeria-site/favicon.png">
        <!-- ======= Google Fonts ======= -->
        <!-- Lato+Raleway Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Raleway:300,400,500,600,700,800,900" rel="stylesheet">
        <!-- ======= all css ======= -->
        <!-- bootstrap css -->
        <link rel="stylesheet" href="http://redcrossnigeria.org/redcross-nigeria-site/css/bootstrap.min.css">
        <!-- font-awesome css -->
        <link rel="stylesheet" href="http://redcrossnigeria.org/redcross-nigeria-site/css/font-awesome.min.css">
        <!-- animate css -->
        <link rel="stylesheet" href="http://redcrossnigeria.org/redcross-nigeria-site/css/animate.css">
        <!-- owl carousel css -->
        <link rel="stylesheet" href="http://redcrossnigeria.org/redcross-nigeria-site/css/owl.carousel.min.css">
        <!-- hover css -->
        <link rel="stylesheet" href="http://redcrossnigeria.org/redcross-nigeria-site/css/hover.css">
        <!-- hover css -->
        <link rel="stylesheet" href="http://redcrossnigeria.org/redcross-nigeria-site/css/lightbox.min.css">
        <!-- normalize css -->
        <link rel="stylesheet" href="http://redcrossnigeria.org/redcross-nigeria-site/css/normalize.css">
        <!-- slicknav css -->
        <link rel="stylesheet" href="http://redcrossnigeria.org/redcross-nigeria-site/css/slicknav.min.css">
        <!-- main css -->
        <link rel="stylesheet" href="http://redcrossnigeria.org/redcross-nigeria-site/css/main.css">


        <!-- responsive css -->
        <link rel="stylesheet" href="http://redcrossnigeria.org/redcross-nigeria-site/css/responsive.css">
    </head>
    <body>
    <!-- ======= header part start ======= -->
    <header class="header-area">
        <!-- top header area -->
        <style>
            .social-link {
                list-style: none;
                padding: 0;
                line-height: 1;
                box-sizing: border-box;
                float: left;
            }

            .social-link li {
                display: inline-block;
                margin: 0;
                /*padding: 0 10px;*/
            }

            .social-link li a{
                border-radius: 30px;
                margin: 0px 5px;
                padding: 10px;
                width: 40px;
                height: 40px;
                /*border: solid 1px #fff;*/
                background: #D22034;
                transition: .5s;
                color: #fff;
            }

            .social-link li fa{
                font-family: "Font Awesome 5 Brands";
                font-weight: 400;
                color: #FFF;
                position: relative;
                transition: .3s ease;
                font-family: ET-Extra!important;
                speak: none;
                font-style: normal;
                font-weight: 400;
                font-variant: normal;
                text-transform: none;
                line-height: inherit!important;

            }
            .social-link li a:hover{
                background: #FFF !important;
                color: #D22034;
            }
        </style>
        <div class="top-header-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <!-- header contact -->
                        <div class="top-header-info">
                            <a href="#"><i class="fa fa-phone"></i>08135928706</a><span class="seprator">|</span>
                            <a href="mailto:admin@redcrossnigeria.org"><i class="fa fa-envelope"></i>admin@redcrossnigeria.org</a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <ul class="social-link">
                            <li><a href="https://www.facebook.com/NigerianRedCrossSocietyNhqts"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/nrcs_ng"><i class="fa fa-twitter"></i></a></li>

                            <li><a href="https://www.instagram.com/redcrossabujabranch/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <!-- <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li> -->
                        </ul>
                    </div>
                    <div class="col-md-3 text-right">
                        <!-- header support -->
                        <div class="top-header-info header-right">
                            <a href="http://redcrossnigeria.org/redcross-nigeria-site/frequent-asked">faq</a><span class="seprator">|</span>
                            <a href="http://redcrossnigeria.org/redcross-nigeria-site/contact-us">contact us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- menu area -->
        <div class="menu-area">
            <div class="container">
                <div class="row">

                    <div class="col-md-2">
                        <!-- logo part -->
                        <div class="believed-logo">
                            <a href="http://localhost:8080"><img src=http://redcrossnigeria.org/redcross-nigeria-site/img/logo.png" alt=""></a>
                        </div>
                        <!-- mobile menu wraper -->
                        <div class="responsive-menu-wrap"></div>
                    </div>
                    <div class="col-md-10">
                        <!-- menu part -->
                        <nav class="main-menu">
                            <ul class="navigation">
                                <li>
                                    <a href="http://redcrossnigeria.org/redcross-nigeria-site/">home</a>
                                </li>
                                <li ><a href="http://redcrossnigeria.org/redcross-nigeria-site/about-us">about us</a><i class="fa fa-caret-down" aria-hidden="true"></i>
                                    <ul class="drop-menu">
                                        <li><a href="http://redcrossnigeria.org/redcross-nigeria-site/history">Our history</a></li>

                                        <li>
                                            <a href="http://redcrossnigeria.org/redcross-nigeria-site/gallery">Our Gallery</a>
                                        </li>
                                        <li>
                                            <a href="http://redcrossnigeria.org/redcross-nigeria-site/gallery">Click to download Branch manual</a>
                                        </li>
                                        <li>
                                            <a href="http://redcrossnigeria.org/redcross-nigeria-site/gallery">Click to download NRCS Annual Report 2018</a>
                                        </li>
                                        <li>
                                            <a href="http://redcrossnigeria.org/redcross-nigeria-site/fundamentalprinciples">7 fundamental principles</a>
                                        </li>
                                    </ul>
                                </li>
                                <li ><a href="http://redcrossnigeria.org/redcross-nigeria-site/about-us">News</a><i class="fa fa-caret-down" aria-hidden="true"></i>
                                    <ul class="drop-menu">
                                        <li><a href="http://redcrossnigeria.org/redcross-nigeria-site/news">Latest News</a></li>

                                        <li>
                                            <a href="http://redcrossnigeria.org/redcross-nigeria-site/archive">Archives</a>
                                        </li>
                                        <li>
                                            <a href="http://redcrossnigeria.org/redcross-nigeria-site/newsletter">Newsletter</a>
                                        </li>
                                        <li>
                                            <a href="http://redcrossnigeria.org/redcross-nigeria-site/press_release">Press release</a>
                                        </li>
                                    </ul>
                                </li>


                                <li><a href="http://redcrossnigeria.org/redcross-nigeria-site/regions">Branches</a>
                                </li>
                                <li><a href="http://redcrossnigeria.org/redcross-nigeria-site/board-members">Board Members</a>
                                </li>
                                <li>

                                    <a href="https://redcrossnigeria.org/web/database/index.php">Join NRC</a><i class="fa fa-caret-down" aria-hidden="true"></i>
                                    <ul class="drop-menu">
                                        <li><a href="http://redcrossnigeria.org/redcross-nigeria-site/get-trained-now">Training And Certification</a></li>
                                        <li><a href="https://redcrossnigeria.org/web/database/index.php">Join us</a></li>
                                    </ul>
                                </li>


                                <li><a href="http://redcrossnigeria.org/redcross-nigeria-site/contact-us">contact us</a></li>
                            </ul>
                        </nav>
                        <!-- donate box -->
                        <div class="donate-box">
                            <span style="display: block; text-align: right;">
                                <a style="" href="http://redcrossnigeria.org/redcross-nigeria-site/donations" class="donate-btn hvr-shutter-out-horizontal">donate</a><br><br>
                            </span>
                            <span id="some-id" style="text-align: right">Donate for the  Needy</span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- ======= header part end ======= -->
    <script src="http://code.jquery.com/jquery-1.7.min.js"></script>
    <script>
        $(function () {
            var messages = [],
                index = 0;

            messages.push('Just for the needy');
            messages.push('We need your support');
            messages.push('Donate Now');

            function cycle() {
                $('#some-id').html(messages[index]);
                index++;

                if (index === messages.length) {
                    index = 0;
                }

                setTimeout(cycle, 3000);
            }

            cycle();
        });
    </script>

    <section class="page-title-area parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- page title -->
                    <div class="page-title">
                        <div class="title">
                            <h2>Login</h2>
                        </div>
                        <ul class="breadcrumb">
                            <li><a href="http://localhost:8080">Home</a></li>
                            <li class="active">Login</li>
                            <?php include 'includes/menu.php'; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ======= page title part end ======= -->

    <!-- ======= Contact Area Start ======= -->
    <?php

    //if (logged_in() === true) {
    //  header('Location: welcome.php');
    //exit();
    //} else {
    ?>
    <section class="contact-area section-padding parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <!-- section title -->
                    <div class="title">
                        <h2>Login</h2>
                        <p>By registering in our database, you will be able to track your:
                            Membership,
                            Volunteering,
                            Donations and
                            Training</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php
                if ($_SERVER["REQUEST_METHOD"] == "POST"){
                    $email = $_POST['email'];
                    $password = $_POST['password'];

                    if (empty($email) === true || empty($password) === true){
                        $errors[] = 'You need to enter an email and password';
                    } else if (email_exists($link, $email) === false){
                        $errors[] ='We can\'t find that email/usernumber<br> Have you registered?';
                    } else {
                        $login = login($link, $email, $password);
                        $pid = personid_from_email($link, $email);
                        $inactive = get_dbfield($link, $pid, 'Inactive');
                        $accountactivated = get_dbfield($link, $pid, 'accountactivated');
                        if ($inactive == 1){
                            $errors[] = 'The account is deactivated. Please contact your branch for more information.';
                        } else if ($accountactivated == 0){
                            $errors[] = 'Your account has not been activated. Please <b>check your email inbox</b> with a mail from us and click on the link.';
                        } else if ($login === false) {
                            $errors[]= 'That password is incorrect';
                        } else {
                            $_SESSION['personid'] = $login;
                            update_lastlogin($link, $login);
                            header('Location: welcome.php');
                            exit();
                        }
                    }
                }


                ?>
                <div class="col-md-4  col-sm-12">

                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                    <img src="https://redcrossnigeria.org/database/images/volunteers.jpg" style="width: 100%; height: auto; border-radius:5px;">
                    <div class="fb-like" data-href="https://redcrossnigeria.org/database/index.php" data-layout="button" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>

                </div>
                <div class="col-md-8  col-sm-8">
                    <div class="contact-form bg-trans">

                        <div class="container">


                            <div class="row justify-content-center">
                                <div class="col-md-8">
                                    <div class="card" style="margin: 0px;">
                                        <div class="alert alert-danger">
                                        <?php
                                        if (empty($errors) == false){
                                            echo "<b><big><span style='color:red;'>We tried to log you in, but...</span></b></big>";
                                            echo output_errors($errors);
                                        }
                                        ?>
                                        </div>
                                        <div class="card-body">

                                            <form method="POST" action="https://redcrossnigeria.org/database/index.php">
                                                <input type="hidden" name="_token" value="zvIHFxMXGX7KdPrpXUy2fU2lgqZm2hTZ4ta2fpSo">
                                                <div class="form-group row">
                                                    <label for="email" class="col-sm-4 col-form-label text-md-right">Email Address</label>

                                                    <div class="col-md-6">
                                                        <input id="email" type="email" class="form-control" name="email" value="" required autofocus>

                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                                                    <div class="col-md-6">
                                                        <input id="password" type="password" class="form-control" name="password" required>

                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-6 offset-md-4">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="remember" > Remember Me
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row mb-0">
                                                    <div class="col-md-8 offset-md-4">
                                                        <button type="submit" class="btn btn-primary">
                                                            Login
                                                        </button>

                                                        <a class="btn btn-link" href="https://redcrossnigeria.org/database/recover.php">
                                                            Forgot Your Password?
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-0">
                                                    <a href="https://redcrossnigeria.org/database/register.php" class="btn btn-primary">
                                                        Register
                                                    </a>
                                                    <a href="https://redcrossnigeria.org/database/register_org_info.php" class="btn btn-success">
                                                        Register your organisation
                                                    </a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    //}
    ?>

    <!-- ======= call to action part start ======= -->
    <script>
        $(function () {
            var messages = [],
                index = 0;

            messages.push('Just for the needy');
            messages.push('We care for children, protect their welfare, and prepare them for the future.');
            messages.push('Please Donate Now');

            function cycle() {
                $('#changing_footer').html(messages[index]);
                index++;

                if (index === messages.length) {
                    index = 0;
                }

                setTimeout(cycle, 3000);
            }

            cycle();
        });
    </script>


    <section class="call-to-action">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="call-to-main" >
                        <h2 id="changing_footer">We care for children, protect their welfare, and prepare them for the future.</h2>
                    </div>
                </div>
                <div class="col-md-3 text-right">
                    <div class="donate-call">
                        <a href="{{ route('donation-index-page') }}" class="donate-btn more-btn hvr-shutter-out-horizontal"><i class="fa fa-send"></i>donate now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ======= call to action part end ======= -->
    <!-- ======= footer area start ======= -->
    <footer class="footer-area parallax">
        <div class="top-footer-area section-padding">
            <div class="container">
                <div class="row">
                    <!-- footer widgets -->
                    <div class="col-md-4 col-sm-6">
                        <div class="footer-widgets">
                            <h2>Head Office</h2>
                            <p>
                                Nigerian Red Cross, National Headquarters, Plot 589, T.O.S. Benson Crescent Off Ngozi Okonjo Iweala
                                Street, Utako District Abuja, Federal Capital Territory, Nigeria
                            </p>
                            <ul>
                                <li><i class="fa fa-phone" aria-hidden="true"></i> <a href="#">08135928706</a></li>
                                <li><i class="fa fa-envelope" aria-hidden="true"></i> <a href="#">admin@redcrossnigeria.org</a></li>
                            </ul>
                            <!-- footer social link -->
                            <div class="footer-social-link">
                                <a href="https://www.facebook.com/NigerianRedCrossSocietyNhqts" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <a href="https://twitter.com/nrcs_ng" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                <a href="https://www.instagram.com/redcrossabujabranch/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- footer widgets -->
                    <div class="col-md-4 col-sm-6">
                        <div class="footer-widgets">
                            <h2>quick links</h2>
                            <ul>
                                <li><a href="http://redcrossnigeria.org/redcross-nigeria-site/about-us"><i class="fa fa-angle-right"></i>About Us</a></li>
                                <li><a href="http://redcrossnigeria.org/redcross-nigeria-site/donations"><i class="fa fa-angle-right"></i>Donate</a></li>
                                <li><a href="http://redcrossnigeria.org/redcross-nigeria-site/contact-us"><i class="fa fa-angle-right"></i>Contact Us</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6">
                        <div class="footer-widgets m-unset">
                            <h2>Photo Gallery</h2>
                            <ul class="list">
                                <li><a href="http://redcrossnigeria.org/redcross-nigeria-site/img/gallery/gallery1.jpg" data-lightbox="example-set" data-title="believed"><img src="http://redcrossnigeria.org/redcross-nigeria-site/img/gallery/gallery1.jpg" alt="#"></a></li>
                                <li><a href="http://redcrossnigeria.org/redcross-nigeria-site/img/gallery/gallery2.jpg" data-lightbox="example-set" data-title="believed"><img src="http://redcrossnigeria.org/redcross-nigeria-site/img/gallery/gallery2.jpg" alt="#"></a></li>
                                <li><a href="http://redcrossnigeria.org/redcross-nigeria-site/img/gallery/gallery3.jpg" data-lightbox="example-set" data-title="believed"><img src="http://redcrossnigeria.org/redcross-nigeria-site/img/gallery/gallery3.jpg" alt="#"></a></li>
                                <li><a href="http://redcrossnigeria.org/redcross-nigeria-site/img/gallery/gallery4.jpg" data-lightbox="example-set" data-title="believed"><img src="http://redcrossnigeria.org/redcross-nigeria-site/img/gallery/gallery4.jpg" alt="#"></a></li>
                                <li><a href="http://redcrossnigeria.org/redcross-nigeria-site/img/gallery/gallery5.jpg" data-lightbox="example-set" data-title="believed"><img src="http://redcrossnigeria.org/redcross-nigeria-site/img/gallery/gallery5.jpg" alt="#"></a></li>
                                <li><a href="http://redcrossnigeria.org/redcross-nigeria-site/img/gallery/gallery6.jpg" data-lightbox="example-set" data-title="believed"><img src="http://redcrossnigeria.org/redcross-nigeria-site/img/gallery/gallery2.jpg" alt="#"></a></li>
                                <li><a href="http://redcrossnigeria.org/redcross-nigeria-site/img/gallery/gallery7.jpg" data-lightbox="example-set" data-title="believed"><img src="http://redcrossnigeria.org/redcross-nigeria-site/img/gallery/gallery3.jpg" alt="#"></a></li>
                                <li><a href="http://redcrossnigeria.org/redcross-nigeria-site/img/gallery/gallery8.jpg" data-lightbox="example-set" data-title="believed"><img src="http://redcrossnigeria.org/redcross-nigeria-site/img/gallery/gallery1.jpg" alt="#"></a></li>
                            </ul>
                            <div class="subscribe-box">
                                <form action="" method="post">
                                    <input type="hidden" name="_token" value="zvIHFxMXGX7KdPrpXUy2fU2lgqZm2hTZ4ta2fpSo">
                                    <input type="text" placeholder="Subscribe for our newsletter">
                                    <button type="submit" class="donate-btn more-btn hvr-shutter-out-horizontal"><i class="fa fa-send"></i>Subscribe</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-footer-area">
            <div class="container">
                <!-- copy right -->
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <div class="copy-right">
                            <p>Copyright &copy; 2018 all rights reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>


    <!--Start of Tawk.to Script-->











    <!--End of Tawk.to Script-->
    <!-- ======= footer area end ======= --><!-- ======= All js ======= -->
    <!-- modernizr js -->
    <script src="http://redcrossnigeria.org/redcross-nigeria-site/js/vendor/modernizr-3.5.0.min.js"></script>

    <!-- jQuery min js -->
    <script src="http://redcrossnigeria.org/redcross-nigeria-site/js/vendor/jquery-3.2.1.min.js"></script>


    <!-- bootstrap js -->
    <script src="http://redcrossnigeria.org/redcross-nigeria-site/js/bootstrap.min.js"></script>
    <!-- sticky js -->
    <script src="http://redcrossnigeria.org/redcross-nigeria-site/js/jquery.sticky.js"></script>
    <!-- owl carousel js -->
    <script src="http://redcrossnigeria.org/redcross-nigeria-site/js/owl.carousel.min.js"></script>
    <!-- jarallax js -->
    <script src="http://redcrossnigeria.org/redcross-nigeria-site/js/jarallax.min.js"></script>
    <!-- waypoints js -->
    <script src="http://redcrossnigeria.org/redcross-nigeria-site/js/jquery.waypoints.min.js"></script>
    <!-- barfiller js -->
    <script src="http://redcrossnigeria.org/redcross-nigeria-site/js/jquery.barfiller.js"></script>
    <!-- countdown js -->
    <script src="http://redcrossnigeria.org/redcross-nigeria-site/js/jquery.countdown.min.js"></script>
    <!-- countdown js -->
    <script src="http://redcrossnigeria.org/redcross-nigeria-site/js/isotope.min.js"></script>
    <!-- lightbox js -->
    <script src="http://redcrossnigeria.org/redcross-nigeria-site/js/lightbox.min.js"></script>
    <!-- scrollUp js -->

    <!-- slick js -->
    <script src="http://redcrossnigeria.org/redcross-nigeria-site/js/slicknav.min.js"></script>
    <!-- plugins js -->
    <script src="http://redcrossnigeria.org/redcross-nigeria-site/js/plugins.js"></script>
    <!-- main js -->
    <script src="http://redcrossnigeria.org/redcross-nigeria-site/js/main.js"></script>
    <script src="http://redcrossnigeria.org/redcross-nigeria-site/js/lga.js"></script>

    <script src='https://www.google.com/recaptcha/api.js'></script>



    </body>

    </html>
<?php
if($link == false){
    mysqli_close($link);
}


?>