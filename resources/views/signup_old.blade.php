@extends('layouts.base')
@section('main-section')
<section class="page-title-area parallax" xmlns="http://www.w3.org/1999/html">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- page title -->
                    <div class="page-title">
                        <div class="title">
                            <h2>Join NRC</h2>
                        </div>
                        <ul class="breadcrumb">
                            <li><a href="{{ route('index')}}">Home</a></li>
                            <li class="active">Join NRC</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ======= page title part end ======= -->





    <section class="contact-area section-padding parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <!-- section title -->
                    <div class="title">
                        <h2>Join The Nigerian Red Cross</h2>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                    @if(Session::has('message'))
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    <div class="contact-form bg-trans bootstrap-iso" id="signup">
                        <form action="signup/store" method="post">
                            {{ csrf_field() }}
                            <div class="col-sm-6">

                                <div class="form-group">

                                    <select class="form-control"  name="state_id" id="popa" onchange="soso()" placeholder="Choose your State of Residence" required>
                                        <option value="">Choose your State of Residence</option>

                                            @foreach($states as $state)
                                                <option value={{$state->state_id}}>{{ $state->state_name }}</option>
                                            @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <select class="form-control"  name="local_govt_id" id="pops"  placeholder="Choose your Local Government Area" required>
                                        <option value="">Choose your Local Government Area</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="first_name" placeholder="First Name" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="surname" placeholder="Surname" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="phone_number" placeholder="Phone" required>
                                </div>
                            </div>

                            <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>

                            <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

                            <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/bootstrap/latest/css/bootstrap.css" />

                            <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.css" />

                            <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.min.css" />

                            <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.min.css.map" />

                            <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" />

                            <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />

                            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>




                            <div class="col-sm-6">
                                <div class="form-group">
                                   <div class="input-group" id="datetimepicker6">


                                        <input type="text" class="form-control" name="date_of_birth" placeholder="Date of Birth YYYY-MM-DD" />
                                       <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            {{--<div class="form-group">--}}
                                {{--<div class='input-group date' id='datetimepicker'>--}}
                                    {{--<input type='text' class="form-control" />--}}
                                    {{--<span class="input-group-addon">--}}
										{{--<span class="glyphicon glyphicon-calendar"></span>--}}
									{{--</span>--}}
                                {{--</div>--}}
                            {{--</div>--}}



                            <script type="text/javascript">
                                $(function () {
                                  //  $('#datetimepicker6').datetimepicker();
                                    $('#datetimepicker6').datetimepicker({
                                        //useCurrent: false //Important! See issue #1075
                                        format :"YYYY-MM-DD HH:mm:ss"
                                    });

                                });
                            </script>


                            <div class="col-sm-6">
                                <div class="form-group">

                                    <select class="form-control"  name="title" placeholder="Title" required>
                                        <option value="">Choose your Title</option>
                                        <option value="Mr">Mr</option>
                                        <option value="Mrs">Mrs</option>
                                        <option value="Ms">Ms</option>
                                        <option value="Miss">Miss</option>
                                        <option value="Prof">Prof</option>
                                        <option value="Chief">Chief</option>
                                        <option value="Dr.">Dr.</option>
                                        <option value="Hon.">Hon.</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {{--<input class="form-control" type="text" name="marital_status" placeholder="Marital Status" required>--}}
                                    <select class="form-control"  name="marital_status" placeholder="Marital Status" required>
                                        <option value="">Choose your Marital status</option>
                                        <option value="1">Single</option>
                                        <option value="2">Married</option>
                                        <option value="3">Divorced</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-12" style="padding-left:40px">
                                <div class="radio">
                                    <label> Choose your gender<br><input type="radio" name="gender" value="Male"/>Male</label><br>
                                    <label><input type="radio" name="gender" value="Female"/>Female</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <select class="form-control"  name="education_level_id" placeholder="Education Level" required>
                                        <option value="">Choose your Education Level</option>
                                        <option value="1">No Education</option>
                                        <option value="2">Primary</option>
                                        <option value="3">Secondary</option>
                                        <option value="4">Tertiary</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="discipline" placeholder="Discipline" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <textarea class="form-control" name="res_address" data-gramm="true" spellcheck="false" data-gramm_editor="true" placeholder="Residential Address" required="required"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <textarea class="form-control" name="work_address" data-gramm="true" spellcheck="false" data-gramm_editor="true" placeholder="Workplace Address" required="required"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="occupation" placeholder="Occupation" required>
                                </div>
                            </div>
                            <script>
                                $(document).ready(function() {
                                    $('.vehicleChkBox').change(function () {
                                        if ($(this).attr('checked')) {
                                            $(this).val(1);
                                        } else {
                                            $(this).val(0);
                                        }
                                    })
                                });
                            </script>

                            <div class="col-sm-12">
                                <div class="radio">
                                    <label> How would you like to contribute to the Nigerian Red Cross?
                                        <br><input type="checkbox" class="vehicleChkBox" value="0" name="make_donations"/>By making donations</label><br>
                                    <label><input type="checkbox" class="vehicleChkBox" value="0" name="be_a_member" />By being a member</label><br>
                                    <label><input type="checkbox" class="vehicleChkBox" value="0" name="be_a_volunteer"/>By volunteering</label><br>
                                </div>
                           </div>

                            <script type="text/javascript">
                                // // when page is ready
                                // $(document).ready(function() {
                                //     // on form submit
                                //     $("#form").on('submit', function() {
                                //         // to each unchecked checkbox
                                //         $(this + 'input[type=checkbox]:not(:checked)').each(function () {
                                //             // set value 0 and check it
                                //             $(this).attr('checked', true).val(0);
                                //         });
                                //     })
                                // })
                            </script>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" type="email" name="email" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" type="password" name="password" placeholder="Password" required>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea class="form-control" name="about_you" data-gramm="true" spellcheck="false" data-gramm_editor="true" placeholder="Tell us About yourself" required="required"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {{--<div class="g-recaptcha" data-sitekey="6LfXfnwUAAAAAIJW_OYS6viKZ-ydqSn5_-obFdys"></div>--}}
                                </div>
                            </div>



                            <div class="col-sm-12">
                                <div class="form-group">

                                </div>
                                <button class="sub-btn donate-btn more-btn hvr-shutter-out-horizontal">Signup Now</button>
                            </div>
                        </form>
                    </div>
                </div>



            </div>
            </div>
    </section>
    <!-- ======= Contact Area End ======= -->


@endsection()
@section('google-map-script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCq4jmXOSB3mlhBKAmVhWq5BUmFzvwWIsk"></script>
    <script src="js/gmap3.min.js"></script>
@endsection()