<?php

// Forces https:// if URL = http://
//if($_SERVER["HTTPS"] != "on"){
//    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
//    exit();
//}

include 'core/init.php';
logged_in_redirect();   //If user is logged in -> redirect to Welcome page
ob_start();

// HEAD
include 'includes/head.php';?>
<SCRIPT language=JavaScript>
    function reload(form)
    {
        var val=form.bid.options[form.bid.options.selectedIndex].value;
        self.location='register.php?bid=' + val  ;
    }
</script>
</head>





<?php

if (isset($_GET['bid']) && $_GET['bid'] == 1){echo "error"; die();}
if (isset($_GET['confirmemail'])){echo "error"; die();}
if (isset($_GET['captchatime'])){echo "error"; die();}
if (isset($_GET['contribute_donor'])){echo "error"; die();}
if (isset($_GET['contribute_member'])){echo "error"; die();}
if (isset($_GET['contribute_volunteering'])){echo "error"; die();}
if (isset($_GET['disciplin'])){echo "error"; die();}
if (isset($_GET['divisionid'])){echo "error"; die();}
if (isset($_GET['educationlevelid'])){echo "error"; die();}
if (isset($_GET['email'])){echo "error"; die();}
if (isset($_GET['firstname'])){echo "error"; die();}
if (isset($_GET['gender'])){echo "error"; die();}
if (isset($_GET['Lastname'])){echo "error"; die();}
if (isset($_GET['maritalstatus'])){echo "error"; die();}
if (isset($_GET['occupation'])){echo "error"; die();}
if (isset($_GET['organisation'])){echo "error"; die();}
if (isset($_GET['other_names'])){echo "error"; die();}
if (isset($_GET['password'])){echo "error"; die();}
if (isset($_GET['password_again'])){echo "error"; die();}
if (isset($_GET['personal_info'])){echo "error"; die();}
if (isset($_GET['student'])){echo "error"; die();}
if (isset($_GET['contribute_volunteering'])){echo "error"; die();}
if (isset($_GET['submit'])){echo "error"; die();}
if (isset($_GET['telephone1'])){echo "error"; die();}
if (isset($_GET['telephone2'])){echo "error"; die();}
if (isset($_GET['titleid'])){echo "error"; die();}
if (isset($_GET['workplace_address'])){echo "error"; die();}
if (isset($_GET['year_of_birth'])){echo "error"; die();}



// VALIDATIONS
if ($_SERVER["REQUEST_METHOD"] == "POST"){    // If user clicked Sumbit

    $errorFlag = false;

    //First Name
    if (empty($_POST["firstname"])) {
        $firstnameErr = "Name is required";
        $errorFlag = true;
    } else {
        $firstname = test_input($_POST["firstname"]);
        if (strlen($firstname) == 0){   // If entered whitespace only
            $firstnameErr = "Name is required";
            $errorFlag = true;
        }
        if (strlen($firstname) == 1){   // If entered only one letter
            $firstnameErr = "Please write the whole name";
            $errorFlag = true;
        }
    }

    //Othe Names
    $other_names = test_input($_POST["other_names"]);

    // Last Name
    if (empty($_POST["lastname"])) {
        $lastnameErr = "Name is required";
        $errorFlag = true;
    } else {
        $lastname = test_input($_POST["lastname"]);
        if (strlen($lastname) == 0){   // If entered whitespace only
            $lastnameErr = "Name is required";
            $errorFlag = true;
        }
        if (strlen($lastname) == 1){   // If entered only one letter
            $lastnameErr = "Please write the whole name";
            $errorFlag = true;
        }
    }

    // Gender
    if (empty($_POST["gender"])) {
        $genderErr = "Gender is required";
        $errorFlag = true;
    } else {
        $gender = test_input(ucfirst($_POST["gender"]));
    }

    // Year of birth
    if (empty($_POST["year_of_birth"])) {
        $year_of_birthErr = "Select year of birth";
        $errorFlag = true;
    } else {
        $year_of_birth = test_input($_POST["year_of_birth"]);
    }

    // Title
    if (empty($_POST["titleid"])) {
        $titleidErr = "Select title";
        $errorFlag = true;
    } else {
        $titleid = test_input($_POST["titleid"]);
    }

    // Marital status
    if (empty($_POST["maritalstatus"])) {
        $maritalstatusErr = "Select marital status";
        $errorFlag = true;
    } else {
        $maritalstatus = test_input(ucfirst($_POST["maritalstatus"]));
    }

    $occupation = test_input($_POST["occupation"]);
    $student = $_POST["student"];

    // Education level
    if (empty($_POST["educationlevelid"])) {
        $educationlevelidErr = "Select education level";
        $errorFlag = true;
    } else {
        $educationlevelid = $_POST["educationlevelid"];
    }

    $disciplin = test_input($_POST["disciplin"]);
    $organisation = test_input($_POST["organisation"]);
    $residential_address = test_input($_POST["residential_address"]);
    $workplace_address = test_input($_POST["workplace_address"]);

    // Branch
    if (empty($_POST["bid"])) {
        $branchidErr = "Select State (Branch)";
        $errorFlag = true;
    } else {
        $bid = $_POST["bid"];
    }

    // Division
    if (empty($_POST["divisionid"])) {
        $divisionidErr = "Select LGA (Division)";
        $errorFlag = true;
    } else {
        $divisionid = $_POST["divisionid"];
    }

    //Email
    if (empty($_POST["email"])) {
        $emailErr = "Email is required";
        $errorFlag = true;
    } else {
        $email = test_input($_POST["email"]);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {   // check if e-mail address is well-formed
            $emailErr = "Invalid email format";
            $errorFlag = true;
        }
        if (email_exists($link, $_POST['email'] ) === true) {
            $emailErr = 'Sorry, the email \'' . $_POST['email'] . '\' is already in use';
            $errorFlag = true;
        }
    }

    //confirm email
    if (empty($_POST["confirmemail"])) {
        $confirmemailErr = "Please confirm email";
        $errorFlag = true;
    } else {
        $confirmemail = test_input($_POST["confirmemail"]);
        if ($confirmemail != $email) {
            $confirmemailErr = "Emails don't match";
            $errorFlag = true;
        }
    }

    //Telephone 1
    if (empty($_POST["telephone1"]) == false){
        $telephone1 = test_input($_POST["telephone1"]);
        if (!preg_match("/^[0-9]*$/",$telephone1)) {
            $telephone1Err = "Use only numbers, for example 08012345678";
        }
        if (strlen($telephone1)<7){
            $telephone1Err = "Phone number too short";
        }
        if (strlen($telephone1)>13){
            $telephone1Err = "Phone number too long";
        }
    }

    //Telephone 2
    if (empty($_POST["telephone2"]) == false){
        $telephone2 = test_input($_POST["telephone2"]);
        if (!preg_match("/^[0-9]*$/",$telephone2)) {
            $telephone2Err = "Use only numbers, for example 08012345678";
            $errorFlag = true;
        }
        if (strlen($telephone2)<7){
            $telephone2Err = "Phone number too short";
            $errorFlag = true;
        }
        if (strlen($telephone2)>13){
            $telephone2Err = "Phone number too long";
            $errorFlag = true;
        }
    }

    $contribute_donor = $_POST["contribute_donor"];
    $contribute_member = $_POST["contribute_member"];
    $contribute_volunteering = $_POST["contribute_volunteering"];
    $personal_info = test_input($_POST["personal_info"]);

    //Password and repeat
    if (empty($_POST["password"])) {
        $passwordErr = "Select a password";
        $errorFlag = true;
    } else {
        $password = $_POST["password"];
        if (strlen($password)<6){
            $passwordErr = 'Your password must be at least 6 characters';
            $errorFlag = true;
        }
    }

    if (empty($_POST["password_again"])) {
        $password_againErr = "Repeat the password";
        $errorFlag = true;
    } else {
        $password_again = $_POST["password_again"];
    }

    if ($password !== $password_again && empty($passwordErr) ){
        $passwordErr = 'Your password do not match';
        $errorFlag = true;
    }

    if (time() - $_POST["captchatime"] < 5){
        $captchaErr ="Please wait a few seconds and submit again";
        $errorFlag = true;
    }

}

?>
<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- ======= titles ======= -->
    <title>The Nigeria Redcross</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="themewarehouse">
    <!-- ======= favicon ======= -->
    <link rel="icon" type="image/png" href="http://localhost:8080/favicon.png">
    <!-- ======= Google Fonts ======= -->
    <!-- Lato+Raleway Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Raleway:300,400,500,600,700,800,900" rel="stylesheet">
    <!-- ======= all css ======= -->
    <!-- bootstrap css -->
    <link rel="stylesheet" href="http://localhost:8080/css/bootstrap.min.css">
    <!-- font-awesome css -->
    <link rel="stylesheet" href="http://localhost:8080/css/font-awesome.min.css">
    <!-- animate css -->
    <link rel="stylesheet" href="http://localhost:8080/css/animate.css">
    <!-- owl carousel css -->
    <link rel="stylesheet" href="http://localhost:8080/css/owl.carousel.min.css">
    <!-- hover css -->
    <link rel="stylesheet" href="http://localhost:8080/css/hover.css">
    <!-- hover css -->
    <link rel="stylesheet" href="http://localhost:8080/css/lightbox.min.css">
    <!-- normalize css -->
    <link rel="stylesheet" href="http://localhost:8080/css/normalize.css">
    <!-- slicknav css -->
    <link rel="stylesheet" href="http://localhost:8080/css/slicknav.min.css">
    <!-- main css -->
    <link rel="stylesheet" href="http://localhost:8080/css/main.css">


    <!-- responsive css -->
    <link rel="stylesheet" href="http://localhost:8080/css/responsive.css">
</head>
<body>
<!-- ======= header part start ======= -->
<header class="header-area">
    <!-- top header area -->
    <style>
        .social-link {
            list-style: none;
            padding: 0;
            line-height: 1;
            box-sizing: border-box;
            float: left;
        }

        .social-link li {
            display: inline-block;
            margin: 0;
            /*padding: 0 10px;*/
        }

        .social-link li a{
            border-radius: 30px;
            margin: 0px 5px;
            padding: 10px;
            width: 40px;
            height: 40px;
            /*border: solid 1px #fff;*/
            background: #D22034;
            transition: .5s;
            color: #fff;
        }

        .social-link li fa{
            font-family: "Font Awesome 5 Brands";
            font-weight: 400;
            color: #FFF;
            position: relative;
            transition: .3s ease;
            font-family: ET-Extra!important;
            speak: none;
            font-style: normal;
            font-weight: 400;
            font-variant: normal;
            text-transform: none;
            line-height: inherit!important;

        }
        .social-link li a:hover{
            background: #FFF !important;
            color: #D22034;
        }
    </style>
    <div class="top-header-area">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <!-- header contact -->
                    <div class="top-header-info">
                        <a href="#"><i class="fa fa-phone"></i>08135928706</a><span class="seprator">|</span>
                        <a href="mailto:admin@redcrossnigeria.org"><i class="fa fa-envelope"></i>admin@redcrossnigeria.org</a>
                    </div>
                </div>
                <div class="col-md-2">
                    <ul class="social-link">
                        <li><a href="https://www.facebook.com/NigerianRedCrossSocietyNhqts"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/nrcs_ng"><i class="fa fa-twitter"></i></a></li>

                        <li><a href="https://www.instagram.com/redcrossabujabranch/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        <!-- <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li> -->
                    </ul>
                </div>
                <div class="col-md-3 text-right">
                    <!-- header support -->
                    <div class="top-header-info header-right">
                        <a href="http://localhost:8080/frequent-asked">faq</a><span class="seprator">|</span>
                        <a href="http://localhost:8080/contact-us">contact us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- menu area -->
    <div class="menu-area">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <!-- logo part -->
                    <div class="believed-logo">
                        <a href="http://localhost:8080"><img src="http://localhost:8080/img/logo.png" alt=""></a>
                    </div>
                    <!-- mobile menu wraper -->
                    <div class="responsive-menu-wrap"></div>
                </div>
                <div class="col-md-10">
                    <!-- menu part -->
                    <nav class="main-menu">
                        <ul class="navigation">
                            <li>
                                <a href="http://localhost:8080">home</a>
                            </li>
                            <li ><a href="http://localhost:8080/about-us">about us</a><i class="fa fa-caret-down" aria-hidden="true"></i>
                                <ul class="drop-menu">
                                    <li><a href="http://localhost:8080/history">Our history</a></li>

                                    <li>
                                        <a href="http://localhost:8080/gallery">Our Gallery</a>
                                    </li>
                                    <li>
                                        <a href="http://localhost:8080/gallery">Click to download Branch manual</a>
                                    </li>
                                    <li>
                                        <a href="http://localhost:8080/gallery">Click to download NRCS Annual Report 2018</a>
                                    </li>
                                    <li>
                                        <a href="http://localhost:8080/fundamentalprinciples">7 fundamental principles</a>
                                    </li>
                                </ul>
                            </li>
                            <li ><a href="http://localhost:8080/about-us">News</a><i class="fa fa-caret-down" aria-hidden="true"></i>
                                <ul class="drop-menu">
                                    <li><a href="http://localhost:8080/news">Latest News</a></li>

                                    <li>
                                        <a href="http://localhost:8080/archive">Archives</a>
                                    </li>
                                    <li>
                                        <a href="http://localhost:8080/newsletter">Newsletter</a>
                                    </li>
                                    <li>
                                        <a href="http://localhost:8080/press_release">Press release</a>
                                    </li>
                                </ul>
                            </li>


                            <li><a href="http://localhost:8080/regions">Branches</a>
                            </li>
                            <li><a href="http://localhost:8080/board-members">Board Members</a>
                            </li>
                            <li>

                                <a href="https://redcrossnigeria.org/web/database/index.php">Join NRC</a><i class="fa fa-caret-down" aria-hidden="true"></i>
                                <ul class="drop-menu">
                                    <li><a href="http://localhost:8080/get-trained-now">Training And Certification</a></li>
                                    <li><a href="https://redcrossnigeria.org/web/database/index.php">Join us</a></li>
                                </ul>
                            </li>


                            <li><a href="http://localhost:8080/contact-us">contact us</a></li>
                        </ul>
                    </nav>
                    <!-- donate box -->
                    <div class="donate-box">
                            <span style="display: block; text-align: right;">
                                <a style="" href="http://localhost:8080/donations" class="donate-btn hvr-shutter-out-horizontal">donate</a><br><br>
                            </span>
                        <span id="some-id" style="text-align: right">Donate for the  Needy</span>
                    </div>

                </div>
            </div>
        </div>
    </div>
</header>
<!-- ======= header part end ======= -->
<script src="http://code.jquery.com/jquery-1.7.min.js"></script>
<script>
    $(function () {
        var messages = [],
            index = 0;

        messages.push('Just for the needy');
        messages.push('We need your support');
        messages.push('Donate Now');

        function cycle() {
            $('#some-id').html(messages[index]);
            index++;

            if (index === messages.length) {
                index = 0;
            }

            setTimeout(cycle, 3000);
        }

        cycle();
    });
</script>
<section class="page-title-area parallax" xmlns="http://www.w3.org/1999/html">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- page title -->
                <div class="page-title">
                    <div class="title">
                        <h2>Join NRC</h2>
                    </div>
                    <ul class="breadcrumb">
                        <li><a href="http://localhost:8080">Home</a></li>
                        <li class="active">Join NRC</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ======= page title part end ======= -->





<section class="contact-area section-padding parallax">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <!-- section title -->
                <div class="title">
                    <h2>Join The Nigerian Red Cross</h2>
                    <?php
                    if (isset($_GET['fail'])){
                        echo "Sorry, there we are experiencing problems with data connection. ";
                        echo "<a href='register_org.php'>Try again</a>";
                        exit();
                    }

                    if (isset($_GET['success']) == false && empty($errorFlag) == true){?>
                        <h1>Online Registration Form</h1>
                        <div style="max-width: 400px"><strong>Important note:</strong> The purpose of this online registration is to create your personal Nigerian Red Cross Database account.
                            In order to become a member, you also need to pay an <b>annual membership fee.</b>
                            <br><br><b>THIS IS NOT A JOB APPLICATION!</b>

                            <br><br><br>
                            <p><span class="error">Fields with a star * must be filled in.</span></p>
                        </div>
                    <?php } ?>

                    <?php
                    if (empty($_POST) === false && empty($errorFlag) === true){
                        $register_data= array(
                            'firstname' 	    => ucwords(strtolower($_POST['firstname'])),
                            'lastname' 	    => ucwords(strtolower($_POST['lastname'])),
                            'other_names'	    => ucwords(strtolower($_POST['other_names'])),
                            'gender' 	        => $_POST['gender'],
                            'year_of_birth' 	=> $_POST['year_of_birth'],
                            'titleid' 	    => $_POST['titleid'],
                            'maritalstatus' 	=> $_POST['maritalstatus'],
                            'occupation'	    => $_POST['occupation'],
                            'student' 	    => bool_to_tinyint($_POST['student']),
                            'educationlevelid'=> $_POST['educationlevelid'],
                            'disciplin' 	    => $_POST['disciplin'],
                            'organisation'    => $_POST['organisation'],
                            'isorganisation'    =>0,
                            'residential_address'	    => $_POST['residential_address'],
                            'workplace_address' 	    => $_POST['workplace_address'],
                            'branchid' 	    => $_POST['bid'],
                            'divisionid' 	    => $_POST['divisionid'],
                            'email'	        => $_POST['email'],
                            'email_code'  => md5($_POST['personid'] + microtime()),
                            'telephone1' 	    => $_POST['telephone1'],
                            'telephone2' 	    => $_POST['telephone2'],
                            'contribute_donor' 	    => bool_to_tinyint($_POST['contribute_donor']),
                            'contribute_member'	    => bool_to_tinyint($_POST['contribute_member']),
                            'contribute_volunteering' => bool_to_tinyint($_POST['contribute_volunteering']),
                            'personal_info' => $_POST['personal_info'],
                            'password'    	=> $_POST['password'],
                            'inactive'    	=> 0,



                        );
                        //print_r ($register_data);
                        // die();
                        register_user($link, $register_data);
                        echo mysqli_error($link);

                        if (empty(mysqli_error($link)) == true){
                            header("Location: register_result.php?success&email=$email");
                            exit();
                        } else {

                            header('Location: register.php?fail');
                            exit();
                        }

                    } else if(empty($errorFlag) === false) {

                        ?> <p><span class="error"><big>Registration not submitted. </big><br> Please make corrections and submit again.</span></p><?php
                    }

                    ?>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                <div class="contact-form bg-trans bootstrap-iso" id="signup">
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                        <?php branch_division_cascading_dynamic($link, $bid, $divisionid, $branchidErr, $divisionidErr, 'State of current residence', 'LGA of current residence');  ?>

                        <input type="hidden" name="_token" value="zvIHFxMXGX7KdPrpXUy2fU2lgqZm2hTZ4ta2fpSo">
                        <div class="col-sm-6">

                            <div class="form-group">

                                <select class="form-control"  name="bid" id="popa" onchange="soso()" placeholder="Choose your State of Residence" required>
                                    <option value="">Choose your State of Residence</option>

                                    <option value=1>Abia</option>
                                    <option value=2>Adamawa</option>
                                    <option value=3>Anambra</option>
                                    <option value=4>Akwa Ibom</option>
                                    <option value=5>Bauchi</option>
                                    <option value=6>Bayelsa</option>
                                    <option value=7>Benue</option>
                                    <option value=8>Borno</option>
                                    <option value=9>Cross River</option>
                                    <option value=10>Delta</option>
                                    <option value=11>Ebonyi</option>
                                    <option value=12>Edo</option>
                                    <option value=13>Ekiti</option>
                                    <option value=14>Enugu</option>
                                    <option value=15>Federal Capital Territory</option>
                                    <option value=16>Gombe</option>
                                    <option value=17>Imo</option>
                                    <option value=18>Jigawa</option>
                                    <option value=19>Kaduna</option>
                                    <option value=20>Kano</option>
                                    <option value=21>Katsina</option>
                                    <option value=22>Kebbi </option>
                                    <option value=23>Kogi</option>
                                    <option value=24>Kwara</option>
                                    <option value=25>Lagos</option>
                                    <option value=26>Nasarawa</option>
                                    <option value=27>Niger</option>
                                    <option value=28>Ogun</option>
                                    <option value=29>Ondo</option>
                                    <option value=30>Osun</option>
                                    <option value=31>Oyo</option>
                                    <option value=32>Plateau </option>
                                    <option value=33>Rivers</option>
                                    <option value=34>Sokoto</option>
                                    <option value=35>Taraba</option>
                                    <option value=36>Yobe</option>
                                    <option value=37>Zamfara</option>

                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">

                                <select class="form-control"  name="divisionid" id="pops"  placeholder="Choose your Local Government Area" required>
                                    <option value="">Choose your Local Government Area</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control" type="text" name="firstname" placeholder="First Name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control" type="text" name="other_names" placeholder="Middlename" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control" type="text" name="lastname" placeholder="Last name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control" type="text" name="telephone1" placeholder="Use this format: 08012345678 " required>
                            </div>
                        </div>


                        <div class="col-sm-6">
                            <div class="form-group">

                                <select class="form-control"  name="year_of_birth" placeholder="Choose your State of Residence" required>
                                    <option value="">Choose your Year of Birth</option>

                                    <option value="">--Select--</option><option value="2016">2016</option><option value="2015">2015</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option><option value="1974">1974</option><option value="1973">1973</option><option value="1972">1972</option><option value="1971">1971</option><option value="1970">1970</option><option value="1969">1969</option><option value="1968">1968</option><option value="1967">1967</option><option value="1966">1966</option><option value="1965">1965</option><option value="1964">1964</option><option value="1963">1963</option><option value="1962">1962</option><option value="1961">1961</option><option value="1960">1960</option><option value="1959">1959</option><option value="1958">1958</option><option value="1957">1957</option><option value="1956">1956</option><option value="1955">1955</option><option value="1954">1954</option><option value="1953">1953</option><option value="1952">1952</option><option value="1951">1951</option><option value="1950">1950</option><option value="1949">1949</option><option value="1948">1948</option><option value="1947">1947</option><option value="1946">1946</option><option value="1945">1945</option><option value="1944">1944</option><option value="1943">1943</option><option value="1942">1942</option><option value="1941">1941</option><option value="1940">1940</option><option value="1939">1939</option><option value="1938">1938</option><option value="1937">1937</option><option value="1936">1936</option><option value="1935">1935</option><option value="1934">1934</option><option value="1933">1933</option><option value="1932">1932</option><option value="1931">1931</option><option value="1930">1930</option><option value="1929">1929</option><option value="1928">1928</option><option value="1927">1927</option><option value="1926">1926</option><option value="1925">1925</option><option value="1924">1924</option><option value="1923">1923</option><option value="1922">1922</option><option value="1921">1921</option><option value="1920">1920</option><option value="1919">1919</option><option value="1918">1918</option><option value="1917">1917</option><option value="1916">1916</option>

                                </select>


                            </div>
                        </div>












                        <script type="text/javascript">
                            $(function () {
                                //  $('#datetimepicker6').datetimepicker();
                                $('#datetimepicker6').datetimepicker({
                                    //useCurrent: false //Important! See issue #1075
                                    format :"YYYY-MM-DD HH:mm:ss"
                                });

                            });
                        </script>


                        <div class="col-sm-6">
                            <div class="form-group">

                                <select class="form-control"  name="titleid" placeholder="Title" required>
                                    <option value="">Choose your Title</option>
                                    <option value="1">Mr</option>
                                    <option value="2">Mrs</option>
                                    <option value="3">Ms</option>
                                    <option value="4">Miss</option>
                                    <option value="5">Prof</option>
                                    <option value="7">Chief</option>
                                    <option value="8">Dr.</option>
                                    <option value="9">Hon.</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">

                                <select class="form-control"  name="maritalstatus" placeholder="Marital Status" required>
                                    <option value="">Choose your Marital status</option>
                                    <option value="Single">Single</option>
                                    <option value="Married">Married</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-12" style="padding-left:40px">
                            <div class="radio">
                                <label> Choose your gender<br><input type="radio" name="gender" value="Male"/>Male</label><br>
                                <label><input type="radio" name="gender" value="Female"/>Female</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">

                                <select class="form-control"  name="educationlevelid" placeholder="Education Level" required>
                                    <option value="">Choose your Education Level</option>
                                    <option value="1">No Education</option>
                                    <option value="2">Primary</option>
                                    <option value="3">Secondary</option>
                                    <option value="4">Tertiary</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control" type="text" name="disciplin" placeholder="Discipline" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <textarea class="form-control" name="residential_address" data-gramm="true" spellcheck="false" data-gramm_editor="true" placeholder="Residential address (include P.O. Box address if you have one)" required="required"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control" type="text" name="organisation" placeholder="Workplace (organisation, company etc.)" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <textarea class="form-control" name="work_address" data-gramm="true" spellcheck="false" data-gramm_editor="true" placeholder="Workplace Address" required="required"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input class="form-control" type="text" name="occupation" placeholder="Occupation" required>
                            </div>
                        </div>
                        <script>
                            $(document).ready(function() {
                                $('.vehicleChkBox').change(function () {
                                    if ($(this).attr('checked')) {
                                        $(this).val(1);
                                    } else {
                                        $(this).val(0);
                                    }
                                })
                            });
                        </script>

                        <div class="col-sm-12">
                            <div class="radio">
                                <label> How would you like to contribute to the Nigerian Red Cross?
                                    <br><input type="checkbox" class="vehicleChkBox" value="True" name="contribute_donor"/>By making donations</label><br>
                                <label><input type="checkbox" class="vehicleChkBox" value="True" name="contribute_member" />By being a member</label><br>
                                <label><input type="checkbox" class="vehicleChkBox" value="True" name="contribute_volunteering"/>By volunteering</label><br>
                            </div>
                        </div>

                        <script type="text/javascript">
                            // // when page is ready
                            // $(document).ready(function() {
                            //     // on form submit
                            //     $("#form").on('submit', function() {
                            //         // to each unchecked checkbox
                            //         $(this + 'input[type=checkbox]:not(:checked)').each(function () {
                            //             // set value 0 and check it
                            //             $(this).attr('checked', true).val(0);
                            //         });
                            //     })
                            // })
                        </script>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control" type="email" name="email" placeholder="Email" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control" type="password" name="password" placeholder="Password" required>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <textarea class="form-control" name="about_you" data-gramm="true" spellcheck="false" data-gramm_editor="true" placeholder="Tell us About yourself" required="required"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">

                            </div>
                        </div>



                        <div class="col-sm-12">
                            <div class="form-group">

                            </div>
                            <button class="sub-btn donate-btn more-btn hvr-shutter-out-horizontal">Signup Now</button>
                            <p>Please check your email inbox once you have submitted!
                                Make sure you click on the link to activate your database account.</p>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</section>
<!-- ======= Contact Area End ======= -->


<!-- ======= call to action part start ======= -->
<script>
    $(function () {
        var messages = [],
            index = 0;

        messages.push('Just for the needy');
        messages.push('We care for children, protect their welfare, and prepare them for the future.');
        messages.push('Please Donate Now');

        function cycle() {
            $('#changing_footer').html(messages[index]);
            index++;

            if (index === messages.length) {
                index = 0;
            }

            setTimeout(cycle, 3000);
        }

        cycle();
    });
</script>


<section class="call-to-action">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="call-to-main" >
                    <h2 id="changing_footer">We care for children, protect their welfare, and prepare them for the future.</h2>
                </div>
            </div>
            <div class="col-md-3 text-right">
                <div class="donate-call">
                    <a href="{{ route('donation-index-page') }}" class="donate-btn more-btn hvr-shutter-out-horizontal"><i class="fa fa-send"></i>donate now</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ======= call to action part end ======= -->
<!-- ======= footer area start ======= -->
<footer class="footer-area parallax">
    <div class="top-footer-area section-padding">
        <div class="container">
            <div class="row">
                <!-- footer widgets -->
                <div class="col-md-4 col-sm-6">
                    <div class="footer-widgets">
                        <h2>Head Office</h2>
                        <p>
                            Nigerian Red Cross, National Headquarters, Plot 589, T.O.S. Benson Crescent Off Ngozi Okonjo Iweala
                            Street, Utako District Abuja, Federal Capital Territory, Nigeria
                        </p>
                        <ul>
                            <li><i class="fa fa-phone" aria-hidden="true"></i> <a href="#">08135928706</a></li>
                            <li><i class="fa fa-envelope" aria-hidden="true"></i> <a href="#">admin@redcrossnigeria.org</a></li>
                        </ul>
                        <!-- footer social link -->
                        <div class="footer-social-link">
                            <a href="https://www.facebook.com/NigerianRedCrossSocietyNhqts" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="https://twitter.com/nrcs_ng" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="https://www.instagram.com/redcrossabujabranch/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
                <!-- footer widgets -->
                <div class="col-md-4 col-sm-6">
                    <div class="footer-widgets">
                        <h2>quick links</h2>
                        <ul>
                            <li><a href="http://localhost:8080/about-us"><i class="fa fa-angle-right"></i>About Us</a></li>
                            <li><a href="http://localhost:8080/donations"><i class="fa fa-angle-right"></i>Donate</a></li>
                            <li><a href="http://localhost:8080/contact-us"><i class="fa fa-angle-right"></i>Contact Us</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="footer-widgets m-unset">
                        <h2>Photo Gallery</h2>
                        <ul class="list">
                            <li><a href="http://localhost:8080/img/gallery/gallery1.jpg" data-lightbox="example-set" data-title="believed"><img src="http://localhost:8080/img/gallery/gallery1.jpg" alt="#"></a></li>
                            <li><a href="http://localhost:8080/img/gallery/gallery2.jpg" data-lightbox="example-set" data-title="believed"><img src="http://localhost:8080/img/gallery/gallery2.jpg" alt="#"></a></li>
                            <li><a href="http://localhost:8080/img/gallery/gallery3.jpg" data-lightbox="example-set" data-title="believed"><img src="http://localhost:8080/img/gallery/gallery3.jpg" alt="#"></a></li>
                            <li><a href="http://localhost:8080/img/gallery/gallery4.jpg" data-lightbox="example-set" data-title="believed"><img src="http://localhost:8080/img/gallery/gallery4.jpg" alt="#"></a></li>
                            <li><a href="http://localhost:8080/img/gallery/gallery5.jpg" data-lightbox="example-set" data-title="believed"><img src="http://localhost:8080/img/gallery/gallery5.jpg" alt="#"></a></li>
                            <li><a href="http://localhost:8080/img/gallery/gallery6.jpg" data-lightbox="example-set" data-title="believed"><img src="http://localhost:8080/img/gallery/gallery2.jpg" alt="#"></a></li>
                            <li><a href="http://localhost:8080/img/gallery/gallery7.jpg" data-lightbox="example-set" data-title="believed"><img src="http://localhost:8080/img/gallery/gallery3.jpg" alt="#"></a></li>
                            <li><a href="http://localhost:8080/img/gallery/gallery8.jpg" data-lightbox="example-set" data-title="believed"><img src="http://localhost:8080/img/gallery/gallery1.jpg" alt="#"></a></li>
                        </ul>
                        <div class="subscribe-box">
                            <form action="" method="post">
                                <input type="hidden" name="_token" value="zvIHFxMXGX7KdPrpXUy2fU2lgqZm2hTZ4ta2fpSo">
                                <input type="text" placeholder="Subscribe for our newsletter">
                                <button type="submit" class="donate-btn more-btn hvr-shutter-out-horizontal"><i class="fa fa-send"></i>Subscribe</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-footer-area">
        <div class="container">
            <!-- copy right -->
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div class="copy-right">
                        <p>Copyright &copy; 2018 all rights reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>


<!--Start of Tawk.to Script-->











<!--End of Tawk.to Script-->
<!-- ======= footer area end ======= --><!-- ======= All js ======= -->
<!-- modernizr js -->
<script src="http://localhost:8080/js/vendor/modernizr-3.5.0.min.js"></script>

<!-- jQuery min js -->
<script src="http://localhost:8080/js/vendor/jquery-3.2.1.min.js"></script>


<!-- bootstrap js -->
<script src="http://localhost:8080/js/bootstrap.min.js"></script>
<!-- sticky js -->
<script src="http://localhost:8080/js/jquery.sticky.js"></script>
<!-- owl carousel js -->
<script src="http://localhost:8080/js/owl.carousel.min.js"></script>
<!-- jarallax js -->
<script src="http://localhost:8080/js/jarallax.min.js"></script>
<!-- waypoints js -->
<script src="http://localhost:8080/js/jquery.waypoints.min.js"></script>
<!-- barfiller js -->
<script src="http://localhost:8080/js/jquery.barfiller.js"></script>
<!-- countdown js -->
<script src="http://localhost:8080/js/jquery.countdown.min.js"></script>
<!-- countdown js -->
<script src="http://localhost:8080/js/isotope.min.js"></script>
<!-- lightbox js -->
<script src="http://localhost:8080/js/lightbox.min.js"></script>
<!-- scrollUp js -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCq4jmXOSB3mlhBKAmVhWq5BUmFzvwWIsk"></script>
<script src="js/gmap3.min.js"></script>

<!-- slick js -->
<script src="http://localhost:8080/js/slicknav.min.js"></script>
<!-- plugins js -->
<script src="http://localhost:8080/js/plugins.js"></script>
<!-- main js -->
<script src="http://localhost:8080/js/main.js"></script>
<script src="http://localhost:8080/js/lga.js"></script>

<script src='https://www.google.com/recaptcha/api.js'></script>



</body>

</html>
