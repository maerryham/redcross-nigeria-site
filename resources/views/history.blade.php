@extends('layouts.base')
@section('main-section')
 <!-- ======= page title part srat ======= -->
    <section class="page-title-area parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- page title -->
                    <div class="page-title">
                        <div class="title">
                            <h2>Our history</h2>
                        </div>
                        <ul class="breadcrumb">
                            <li><a href="{{ route('index')}}">Home</a></li>
                            <li class="active">Our history </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ======= page title part end ======= -->

    <!-- ======= about part start ======= -->
    <div class="about-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <!-- section title -->
                    <div class="title">
                        <h2>The History of Redcross</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1 ">
                    <!-- about details -->
                    <div class="about-details">
                        {!! $history->history_of_redcross !!}
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <!-- section title -->
                    <div class="title">
                        <h2>The history of Nigerian Redcross society</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1 ">
                    <!-- about details -->
                    <div class="about-details">
                        <p>
                            {!! $history->history_of_nigerian !!}


                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ======= about part end ======= -->

@endsection()