@extends('layouts.base')
@section('main-section')
<section class="page-title-area parallax section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- page title -->
                   <div class="page-title">
                        <div class="title">
                            <h2>training and certification</h2>
                        </div>
                        <ul class="breadcrumb">
                            <li><a href="{{ route('index')}}">Home</a></li>
                            <li class="active">TRAINING AND CERTIFICATION</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ======= page title part end ======= -->




    <!-- ======= Contact Area Start ======= -->
    <section class="contact-area section-padding parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <!-- section title -->
                    <div class="title">
                        <h2>First Aid Training</h2>
                        <p>We offer training and certifications for people, for work, parents and anyone who
                            wants to learn the first aid skills needed to help in a first aid emergency.</p>
                        <p>Get First Aid Certified. At the Nigerian Red Cross, our mission is to help people prepare for and respond
                            to emergencies properly. In order to accomplish this goal, we have developed a wide range of training
                            opportunities and certification programs that can empower you to help during times of emergency.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row" id="training">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <!-- section title -->
                    <div class="title">
                        <h2>Contact us today</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                    @if(Session::has('message'))
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    <div class="contact-form bg-trans">
                        <form action="get-trained-now/train" method="post">
                            {{ csrf_field() }}
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="name" placeholder="Full Name" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" type="email" name="email" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="phone" placeholder="Phone Number" required>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                   <select class="form-control"  name="training" placeholder="Choose your First Aid Training" required>
                                        <option value="">Choose your First Aid Training</option>
                                        <option value="Competence Based Test">Competence Based Test</option>
                                        <option value="Standard First Aid">Standard First Aid</option>
                                        <option value="First Aid at Work">First Aid at Work</option>
                                        <option value="Cardiopulmonary Resuscitation (CPR)">Cardiopulmonary Resuscitation (CPR)</option>
                                        <option value="Automated External Defibrillator (AED)">Automated External Defibrillator (AED)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea class="form-control" name="message" data-gramm="true" spellcheck="false" data-gramm_editor="true" placeholder="Message" required="required"></textarea>
                                </div>
                                <button class="sub-btn donate-btn more-btn hvr-shutter-out-horizontal">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection()
@section('google-map-script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCq4jmXOSB3mlhBKAmVhWq5BUmFzvwWIsk"></script>
    <script src="js/gmap3.min.js"></script>
@endsection()