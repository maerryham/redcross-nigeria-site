<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- ======= titles ======= -->
    <title>The Nigeria Redcross</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="themewarehouse">
    <!-- ======= favicon ======= -->
    <link rel="icon" type="image/png" href="{{ asset('favicon.png')}}">
    <!-- ======= Google Fonts ======= -->
    <!-- Lato+Raleway Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Raleway:300,400,500,600,700,800,900" rel="stylesheet">
    <!-- ======= all css ======= -->
    <!-- bootstrap css -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
    <!-- font-awesome css -->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css')}}">
    <!-- animate css -->
    <link rel="stylesheet" href="{{ asset('css/animate.css')}}">
    <!-- owl carousel css -->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css')}}">
    <!-- hover css -->
    <link rel="stylesheet" href="{{ asset('css/hover.css')}}">
    <!-- hover css -->
    <link rel="stylesheet" href="{{ asset('css/lightbox.min.css')}}">
    <!-- normalize css -->
    <link rel="stylesheet" href="{{ asset('css/normalize.css')}}">
    <!-- slicknav css -->
    <link rel="stylesheet" href="{{ asset('css/slicknav.min.css')}}">
    <!-- main css -->
    <link rel="stylesheet" href="{{ asset('css/main.css')}}">

{{--    <link rel="stylesheet" href="{{ asset('css/form.css')}}">--}}
    <!-- responsive css -->
    <link rel="stylesheet" href="{{ asset('css/responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/trumbowyg.min.css')}}">
    @yield('style-section')
</head>
<body>
@include('components.headers')
@yield('main-section')
@include('components.footer')
<!-- ======= All js ======= -->
    <!-- modernizr js -->
    <script src="{{ asset('js/vendor/modernizr-3.5.0.min.js')}}"></script>

    <!-- jQuery min js -->
    <script src="{{ asset('js/vendor/jquery-3.2.1.min.js')}}"></script>

    {{--<script src="{{ asset('js/form.js')}}"></script>--}}
    <!-- bootstrap js -->
    <script src="{{ asset('js/bootstrap.min.js')}}"></script>
    <!-- sticky js -->
    <script src="{{ asset('js/jquery.sticky.js')}}"></script>
    <!-- owl carousel js -->
    <script src="{{ asset('js/owl.carousel.min.js')}}"></script>
    <!-- jarallax js -->
    <script src="{{ asset('js/jarallax.min.js')}}"></script>
    <!-- waypoints js -->
    <script src="{{ asset('js/jquery.waypoints.min.js')}}"></script>
    <!-- barfiller js -->
    <script src="{{ asset('js/jquery.barfiller.js')}}"></script>
    <!-- countdown js -->
    <script src="{{ asset('js/jquery.countdown.min.js')}}"></script>
    <!-- countdown js -->
    <script src="{{ asset('js/isotope.min.js')}}"></script>
    <!-- lightbox js -->
    <script src="{{ asset('js/lightbox.min.js')}}"></script>
    <!-- scrollUp js -->
    @yield('google-map-script')
    {{--<script src="{{ asset('js/jquery.scrollUp.min.js')}}"></script>--}}
    <!-- slick js -->
    <script src="{{ asset('js/slicknav.min.js')}}"></script>
    <!-- plugins js -->
    <script src="{{ asset('js/plugins.js')}}"></script>
    <!-- main js -->
    <script src="{{ asset('js/main.js')}}"></script>
    <script src="{{ asset('js/lga.js')}}"></script>

    <script src='https://www.google.com/recaptcha/api.js'></script>


    @yield('script-section')

</body>

</html>
