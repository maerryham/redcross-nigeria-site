@extends('layouts.base')
@section('main-section')
      <!-- ======= page title part srat ======= -->
    <section class="page-title-area parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- page title -->
                    <div class="page-title">
                        <div class="title">
                            <h2>Archive</h2>
                        </div>
                        <ul class="breadcrumb">
                            <li><a href="">Home</a></li>
                            <li class="active">Archive</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ======= page title part end ======= -->
     <!-- ======= blog part start ======= -->
    <section class="blog-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <!-- section title -->
                    <div class="title">
                        <h2>{{$news->title}}</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8 col-md-offset-2">
               <?php   // $date_det = strftime("%b %d, %Y", strtotime($report->date));
                     $date = strftime("%d", strtotime($news->date));
                     $month = strftime("%b", strtotime($news->date));
                    $day = strftime("%A", strtotime($news->date));
                    $year = strftime("%Y", strtotime($news->date));
                    // $time =  date("g:i A", strtotime($report->date));

                 $date_det = strftime("%b %d, %Y", strtotime($news->date)); ?>

                    <article class="single-blog-content" style="margin-bottom: 100px">
                        <img src="{{asset('img/news/'.$news->image)}}" alt="">
                        <h2>{{$news->title}}</h2>
                        <div class="blog-get-info">
                            <i class="fa fa-calendar" aria-hidden="true"></i> <span>{{$date}} {{$month}}, {{$year}}</span>
                            {{--<i class="fa fa-clock-o" aria-hidden="true"></i><span> 6.30pm</span>--}}
                        </div>
                        <p>{!!$news->description!!}</p>

                    </article>



                </div>
            </div>


        </div>
    </section>

@endsection()