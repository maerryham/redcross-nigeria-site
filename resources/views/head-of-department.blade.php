@extends('layouts.base')
@section('main-section')
      <!-- ======= page title part srat ======= -->
    <section class="page-title-area parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- page title -->
                    <div class="page-title">
                        <div class="title">
                            <h2>Head of Departments</h2>
                        </div>
                        <ul class="breadcrumb">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">Head of Departments</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
      <section id="team" class="team-area section-padding">
          <div class="container">
              <div class="row">
                  <div class="col-md-8 col-md-offset-2 text-center">
                      <!-- section title -->
                      <div class="title">
                          <h2>Our Head of Departments</h2>
                          {{--<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi tempora veritatis nemo aut ea iusto eos est expedita, quas ab adipisci consectetur tempora jet.</p>--}}
                      </div>
                  </div>
              </div>
              <div class="row">
                  @foreach($boardmembers as $boardmember)
                  <div class="col-sm-6 col-md-3">
                      <!-- Single Member -->
                      <div class="team-member">
                          <div class="member-top">
                              <a href="{{route('head-of-departments')}}/{{$boardmember->id}}"> <img src="{{asset('img/head_of_departments/'.$boardmember->image)}}" alt=""></a>
                              {{--<div class="team-social">--}}
                                  {{--<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>--}}
                                  {{--<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>--}}
                                  {{--<a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>--}}
                                  {{--<a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>--}}
                              {{--</div>--}}
                          </div>
                          <div class="member-dec">
                              <a href="{{route('head-of-departments')}}/{{$boardmember->id}}"><h4>{{$boardmember->full_name}}</h4></a>
                              <span style="text-transform: uppercase">{{$boardmember->post}}</span>
                          </div>
                      </div>
                  </div>
                  @endforeach

              </div>
          </div>
      </section>


@endsection()