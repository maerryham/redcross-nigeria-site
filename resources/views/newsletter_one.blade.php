@extends('layouts.base')
@section('main-section')
      <!-- ======= page title part srat ======= -->
    <section class="page-title-area parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- page title -->
                    <div class="page-title">
                        <div class="title">
                            <h2>News</h2>
                        </div>
                        <ul class="breadcrumb">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">Newsletter</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ======= page title part end ======= -->
     <!-- ======= blog part start ======= -->
    <section class="blog-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <!-- section title -->
                    <div class="title">
                        <h2>{{$news->title}}</h2>
                    </div>
                </div>
            </div>
            <div class="row">


                <div class="col-sm-8 col-md-offset-2">

                        <?php   // $date_det = strftime("%b %d, %Y", strtotime($report->date));
                        $date = strftime("%d", strtotime($news->date));

                        $day = strftime("%A", strtotime($news->date));
                        $year = strftime("%Y", strtotime($news->date));
                        $month = strftime("%b", strtotime($news->date));
                        // $time =  date("g:i A", strtotime($report->date));

                        $date_det = strftime("%b %d, %Y", strtotime($news->date)); ?>
                    <article class="single-blog-content">
                        <img src="{{asset('img/newsletter/'.$news->image)}}" alt="">

                        <h4 style="margin-top: 20px"><a style="color: #D22034; " href="newsletter/{{$news->page_link}}">{{$news->title}}</a></h4>

                       <p> {!! $news->description !!}</p>

                    </article>


                </div>


            </div>
                
        </div>
    </section>
    
@endsection()