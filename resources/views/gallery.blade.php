@extends('layouts.base')
@section('main-section')
    <!-- ======= page title part srat ======= -->
    <section class="page-title-area parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- page title -->
                    <div class="page-title">
                        <div class="title">
                            <h2>Photo Gallery</h2>
                        </div>
                        <ul class="breadcrumb">
                            <li><a href="{{ route('index')}}">Home</a></li>
                            <li class="active">Gallery</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ======= page title part end ======= -->

    <!-- ======= gallery part start ======= -->
    <div class="gallery-area section-padding ex-mg" id="gallery">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- gallery Menu -->
                    <div class="iso-content">
                        @foreach($gallerys as $gallery)
                        <div class="single-gallery full-wid iso-item children volunteer" style="height: 190px; overflow: hidden;">
                            <div class="gallery-img" style="max-height: 190px; overflow: hidden; ">
                                <img src="{{ asset('img/gallery/'.$gallery->image)}}" alt="">
                                <div class="gallery-lightbox">
                                    <a href="{{ asset('img/gallery/'.$gallery->image)}}" data-lightbox="example-set" data-title="believed">
                                    <i class="fa fa-search"></i>
                                    </a>
                                    <a href="#">
                                    <i class="fa fa-expand" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>

                </div>
                <center>{{$gallerys->links() }}</center>
            </div>
        </div>
    </div>
    <!-- ======= gallery part end ======= -->

    <!-- ======= call to action part start ======= -->

@endsection()