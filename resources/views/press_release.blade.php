@extends('layouts.base')
@section('main-section')
    <!-- ======= page title part srat ======= -->
    <section class="page-title-area parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- page title -->
                    <div class="page-title">
                        <div class="title">
                            <h2>Press Release</h2>
                        </div>
                        <ul class="breadcrumb">
                            <li><a href="{{route('index')}}">Home</a></li>
                            <li><a href="{{route('news')}}">News</a></li>
                            <li class="active">Press Release</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="blog-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <!-- section title -->
                    <div class="title">
                        <h2>Press Release</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="related-causes col-md-offset-2 col-md-8">

                    @foreach($press_releases as $press_release)
                        <?php   // $date_det = strftime("%b %d, %Y", strtotime($report->date));
                        $date = strftime("%d", strtotime($press_release->date));
                        $month = strftime("%b", strtotime($press_release->date));
                        $day = strftime("%A", strtotime($press_release->date));
                        $year = strftime("%Y", strtotime($press_release->date));
                        // $time =  date("g:i A", strtotime($report->date));

                        $date_det = strftime("%b %d, %Y", strtotime($press_release->date)); ?>

                    <div class="related-causes-info">

                        <h3><a style="color: #D22034" href="press_release/{{$press_release->page_link}}">{{$press_release->title}}</a></h3>
                        <p>{!!  $press_release->description !!}</p>
                        <span style="float: right">{{$date}} {{$month}}, {{$year}}</span>
                        {{--<a href="#" class="donate-btn hvr-shutter-out-horizontal">read more</a>--}}

                    </div>
                        @endforeach

                    {{--<!-- single related causes -->--}}
                    {{--<div class="related-causes-info">--}}

                            {{--<h3>Gear up for giving</h3>--}}
                            {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam quas, quidem totam, fuga iste et voluptatem tempora molestiae ipsa. Similique voluptates mollitia.</p>--}}
                            {{--<a href="#" class="donate-btn hvr-shutter-out-horizontal">read more</a>--}}

                    {{--</div>--}}
                </div>

            </div>
        </div>
    </section>

@endsection()