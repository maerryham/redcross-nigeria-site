@extends('layouts.base')
@section('main-section')
<section class="page-title-area parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- page title -->
                    <div class="page-title">
                        <div class="title">
                            <h2>contact us</h2>
                        </div>
                        <ul class="breadcrumb">
                            <li><a href="{{ route('index')}}">Home</a></li>
                            <li class="active">contact us</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ======= page title part end ======= -->

    <!-- ======= Contact Area Start ======= -->
    <section class="contact-area section-padding parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <!-- section title -->
                    <div class="title">
                        <h2>Get In Touch</h2>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                    <div class="contact-form bg-trans">
                        @if(Session::has('message'))
                        <div class="alert alert-success" >
                            {{ Session::get('message') }}
                        </div>
                        @endif
                        <form action="{{route('contact-us/send')}}" method="post">
                            {{ csrf_field() }}
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="name" placeholder="Name" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" type="email" name="email" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="phone" placeholder="Phone" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="subject" placeholder="subject" required>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea class="form-control" name="message" data-gramm="true" spellcheck="false" data-gramm_editor="true" placeholder="Message" required="required"></textarea>
                                </div>
                                <button class="sub-btn donate-btn more-btn hvr-shutter-out-horizontal">send now</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ======= Contact Area End ======= -->

@endsection()
@section('google-map-script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCq4jmXOSB3mlhBKAmVhWq5BUmFzvwWIsk"></script>
    <script src="js/gmap3.min.js"></script>
@endsection()