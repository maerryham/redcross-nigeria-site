@extends('layouts.base')
@section('main-section')
<section class="page-title-area parallax" xmlns="http://www.w3.org/1999/html">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- page title -->
                    <div class="page-title">
                        <div class="title">
                            <h2>Join NRC</h2>
                        </div>
                        <ul class="breadcrumb">
                            <li><a href="{{ route('index')}}">Home</a></li>
                            <li class="active">Join NRC</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ======= page title part end ======= -->





    <section class="contact-area section-padding parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <!-- section title -->
                    <div class="title">
                        <h2>Join The Nigerian Red Cross</h2>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                    @if(Session::has('message'))
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    <div class="contact-form bg-trans bootstrap-iso" id="signup">
                        <form action="signup/store" method="post">
                            {{ csrf_field() }}
                            <div class="col-sm-6">

                                <div class="form-group">

                                    <select class="form-control"  name="bid" id="popa" onchange="soso()" placeholder="Choose your State of Residence" required>
                                        <option value="">Choose your State of Residence</option>

                                            @foreach($states as $state)
                                                <option value={{$state->state_id}}>{{ $state->state_name }}</option>
                                            @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <select class="form-control"  name="divisionid" id="pops"  placeholder="Choose your Local Government Area" required>
                                        <option value="">Choose your Local Government Area</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="firstname" placeholder="First Name" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="other_names" placeholder="Middlename" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="lastname" placeholder="Last name" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="telephone1" placeholder="Use this format: 08012345678 " required>
                                </div>
                            </div>


                            <div class="col-sm-6">
                                <div class="form-group">

                                    <select class="form-control"  name="year_of_birth" placeholder="Choose your State of Residence" required>
                                        <option value="">Choose your Year of Birth</option>

                                        <option value="">--Select--</option><option value="2016">2016</option><option value="2015">2015</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option><option value="1974">1974</option><option value="1973">1973</option><option value="1972">1972</option><option value="1971">1971</option><option value="1970">1970</option><option value="1969">1969</option><option value="1968">1968</option><option value="1967">1967</option><option value="1966">1966</option><option value="1965">1965</option><option value="1964">1964</option><option value="1963">1963</option><option value="1962">1962</option><option value="1961">1961</option><option value="1960">1960</option><option value="1959">1959</option><option value="1958">1958</option><option value="1957">1957</option><option value="1956">1956</option><option value="1955">1955</option><option value="1954">1954</option><option value="1953">1953</option><option value="1952">1952</option><option value="1951">1951</option><option value="1950">1950</option><option value="1949">1949</option><option value="1948">1948</option><option value="1947">1947</option><option value="1946">1946</option><option value="1945">1945</option><option value="1944">1944</option><option value="1943">1943</option><option value="1942">1942</option><option value="1941">1941</option><option value="1940">1940</option><option value="1939">1939</option><option value="1938">1938</option><option value="1937">1937</option><option value="1936">1936</option><option value="1935">1935</option><option value="1934">1934</option><option value="1933">1933</option><option value="1932">1932</option><option value="1931">1931</option><option value="1930">1930</option><option value="1929">1929</option><option value="1928">1928</option><option value="1927">1927</option><option value="1926">1926</option><option value="1925">1925</option><option value="1924">1924</option><option value="1923">1923</option><option value="1922">1922</option><option value="1921">1921</option><option value="1920">1920</option><option value="1919">1919</option><option value="1918">1918</option><option value="1917">1917</option><option value="1916">1916</option>

                                    </select>


                                </div>
                            </div>

                            {{--<div class="form-group">--}}
                                {{--<div class='input-group date' id='datetimepicker'>--}}
                                    {{--<input type='text' class="form-control" />--}}
                                    {{--<span class="input-group-addon">--}}
										{{--<span class="glyphicon glyphicon-calendar"></span>--}}
									{{--</span>--}}
                                {{--</div>--}}
                            {{--</div>--}}



                            <script type="text/javascript">
                                $(function () {
                                  //  $('#datetimepicker6').datetimepicker();
                                    $('#datetimepicker6').datetimepicker({
                                        //useCurrent: false //Important! See issue #1075
                                        format :"YYYY-MM-DD HH:mm:ss"
                                    });

                                });
                            </script>


                            <div class="col-sm-6">
                                <div class="form-group">

                                    <select class="form-control"  name="titleid" placeholder="Title" required>
                                        <option value="">Choose your Title</option>
                                        <option value="1">Mr</option>
                                        <option value="2">Mrs</option>
                                        <option value="3">Ms</option>
                                        <option value="4">Miss</option>
                                        <option value="5">Prof</option>
                                        <option value="7">Chief</option>
                                        <option value="8">Dr.</option>
                                        <option value="9">Hon.</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {{--<input class="form-control" type="text" name="marital_status" placeholder="Marital Status" required>--}}
                                    <select class="form-control"  name="maritalstatus" placeholder="Marital Status" required>
                                        <option value="">Choose your Marital status</option>
                                        <option value="Single">Single</option>
                                        <option value="Married">Married</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-12" style="padding-left:40px">
                                <div class="radio">
                                    <label> Choose your gender<br><input type="radio" name="gender" value="Male"/>Male</label><br>
                                    <label><input type="radio" name="gender" value="Female"/>Female</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <select class="form-control"  name="educationlevelid" placeholder="Education Level" required>
                                        <option value="">Choose your Education Level</option>
                                        <option value="1">No Education</option>
                                        <option value="2">Primary</option>
                                        <option value="3">Secondary</option>
                                        <option value="4">Tertiary</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="disciplin" placeholder="Discipline" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <textarea class="form-control" name="residential_address" data-gramm="true" spellcheck="false" data-gramm_editor="true" placeholder="Residential address (include P.O. Box address if you have one)" required="required"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="organisation" placeholder="Workplace (organisation, company etc.)" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <textarea class="form-control" name="work_address" data-gramm="true" spellcheck="false" data-gramm_editor="true" placeholder="Workplace Address" required="required"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="occupation" placeholder="Occupation" required>
                                </div>
                            </div>
                            <script>
                                $(document).ready(function() {
                                    $('.vehicleChkBox').change(function () {
                                        if ($(this).attr('checked')) {
                                            $(this).val(1);
                                        } else {
                                            $(this).val(0);
                                        }
                                    })
                                });
                            </script>

                            <div class="col-sm-12">
                                <div class="radio">
                                    <label> How would you like to contribute to the Nigerian Red Cross?
                                        <br><input type="checkbox" class="vehicleChkBox" value="True" name="contribute_donor"/>By making donations</label><br>
                                    <label><input type="checkbox" class="vehicleChkBox" value="True" name="contribute_member" />By being a member</label><br>
                                    <label><input type="checkbox" class="vehicleChkBox" value="True" name="contribute_volunteering"/>By volunteering</label><br>
                                </div>
                           </div>

                            <script type="text/javascript">
                                // // when page is ready
                                // $(document).ready(function() {
                                //     // on form submit
                                //     $("#form").on('submit', function() {
                                //         // to each unchecked checkbox
                                //         $(this + 'input[type=checkbox]:not(:checked)').each(function () {
                                //             // set value 0 and check it
                                //             $(this).attr('checked', true).val(0);
                                //         });
                                //     })
                                // })
                            </script>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" type="email" name="email" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" type="password" name="password" placeholder="Password" required>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea class="form-control" name="about_you" data-gramm="true" spellcheck="false" data-gramm_editor="true" placeholder="Tell us About yourself" required="required"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {{--<div class="g-recaptcha" data-sitekey="6LfXfnwUAAAAAIJW_OYS6viKZ-ydqSn5_-obFdys"></div>--}}
                                </div>
                            </div>



                            <div class="col-sm-12">
                                <div class="form-group">

                                </div>
                                <button class="sub-btn donate-btn more-btn hvr-shutter-out-horizontal">Signup Now</button>
                                <p>Please check your email inbox once you have submitted!
                                    Make sure you click on the link to activate your database account.</p>
                            </div>
                        </form>
                    </div>
                </div>



            </div>
            </div>
    </section>
    <!-- ======= Contact Area End ======= -->


@endsection()
@section('google-map-script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCq4jmXOSB3mlhBKAmVhWq5BUmFzvwWIsk"></script>
    <script src="js/gmap3.min.js"></script>
@endsection()