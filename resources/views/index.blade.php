@extends('layouts.base')
@section('main-section')
<script>
        timer = window.setInterval("autoSlide()", 10000);
        function autoSlide(){ jQuery(".owl-next").click(); }

</script>
<style>
    .background{
        background-color: #363636;
        padding: 50px;
    }

    .crop-corner {
        border-top: solid 15px #f5333f;
        border-left: solid 15px #f5333f;
        border-bottom: none;
        border-right: none;
        height: 45px;
        width: 45px;
    }

    .slider-info{
        margin-left: 30px;
        margin-right: 30px;
    }
    .slider-area .owl-dots {
        bottom: 20px;
    }
    .slider-table {
        display: table;
        height: 40vh;
    }
</style>
    <!-- ======= slider part start ======= -->
    <section class="slider-area">
        <div class="slider-wraper owl-carousel">
            <!-- single slider -->
            @foreach($slider as $slide)
                <?php   // $date_det = strftime("%b %d, %Y", strtotime($report->date));
                $date = strftime("%d", strtotime($slide->date));
                $month = strftime("%b", strtotime($slide->date));
                $day = strftime("%A", strtotime($slide->date));
                $year = strftime("%Y", strtotime($slide->date));
                // $time =  date("g:i A", strtotime($report->date));

                $date_det = strftime("%b %d, %Y", strtotime($slide->date)); ?>
            <div class="single-slider slider-one" style="background-color: #363636;">
                <div class="slider-table">

                    <div class="slider-table-cell">
                        <div class="container-fluid">
                            <div class="row height">
                                <div class="col-sm-6 col-sm-offset-0 col-lg-6 col-md-12" style="padding: 0px; height:100%; overflow: hidden ">
                                    <a href="news/{{$slide->page_link}}"> <img src="{{asset('img/news/'.$slide->image)}}" style="min-width: 100%; width: auto; height: auto; min-height: 100%; margin: 0px; padding: 0px;"></a>
                                </div>
                                <div class="col-sm-6 col-sm-offset-0 col-lg-6 col-md-12 background" style="height:100%">
                                    <div class="crop-corner"></div>
                                    <!-- single slider content -->
                                    <div class="slider-info">

                                        <h3><a style="color: #D22034" href="news/{{$slide->page_link}}">{{$slide->title}}</a></h3>
                                        <p>
                                            {!! substr($slide->description,0,150) !!}...

                                        </p>
                                        <p><i class="fa fa-calendar" aria-hidden="true"></i> <span>{{$date}} {{$month}}, {{$year}}</span></p>
                                        <a href="news/{{$slide->page_link}}" style="margin:0px" class="donate-btn hvr-shutter-out-horizontal">Read More</a>
                                        {{--<a href="https://redcrossnigeria.org/database/index.php" class="donate-btn hvr-shutter-in-horizontal">join volunteer</a>--}}
                                    </div>
                                    <!-- /.single slider content -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           @endforeach

        </div>
    </section>
    <!-- ======= slider part end ======= -->

    <!-- ======= about part start ======= -->
    <section class="about-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <!-- section title -->
                    <div class="title">
                        <h2>who we are</h2>
                        <p>A sure sign of hope.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <!-- about img -->
                    <div class="about-img">
                        <img src="{{asset('img/about/'.$about->image)}}" alt="" >
                    </div>
                </div>
                <div class="col-sm-7">
                    <!-- about details -->
                    <div class="about-details">
                        <p>
                          {{$about->who_we_are}}
                        </p>
                        <a href="{{ route('about-us')}}" class="donate-btn more-btn hvr-shutter-out-horizontal">read more</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <!-- service content -->
                    <div class="service-content">
                        <div class="service-title">
                            <h3>Training and certification</h3>
                        </div>
                        <i class="fa fa-book" aria-hidden="true"></i>
                        <p>
                            First Aid and tips on emergency response
                            are important. First aid is a humanitarian act that should be accessible to all.
                            With first aid skill volunteers and communities are empowered to save life
                            without discrimination.
                        </p>
                        <a href="{{ route('get-trained-now')}}" class="donate-btn more-btn hvr-shutter-out-horizontal">Apply for a training</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <!-- service content -->
                    <div class="service-content">
                        <div class="service-title">
                            <h3>get involved</h3>
                        </div>
                        <i class="fa fa-globe" aria-hidden="true"></i>
                        <p>We appreciate your support, kindly donate to our cause</p>
                        <a href="{{ route('donation-index-page') }}" class="donate-btn more-btn hvr-shutter-out-horizontal">donate now</a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <!-- service content -->
                    <div class="service-content mobile-auto">
                        <div class="service-title">
                            <h3>be a volunteer</h3>
                        </div>
                        <i class="fa fa-users" aria-hidden="true"></i>
                        <p>
                            Our volunteers are at the heart of everything
                            we do, from first aid and disaster response to health and hygiene promotion. Join
                            our growing list of volunteers today.
                        </p>
                        <a href="http://redcrossnigeria.org/database/index_new.php" class="donate-btn more-btn hvr-shutter-out-horizontal">join us</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ======= about part end ======= -->
    <!-- ======= gallery part start ======= -->
    <div class="gallery-area section-padding" id="gallery">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <!-- section title -->
                    <div class="title">
                        <h2>our gallery</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="iso-content">
            @foreach($gallerys as $gallery)
                <div class="single-gallery full-wid iso-item children volunteer" style="height: 190px; overflow: hidden;">
                    <div class="gallery-img" style="max-height: 190px; overflow: hidden; ">
                        <img src="{{ asset('img/gallery/'.$gallery->image)}}" alt="">
                        <div class="gallery-lightbox">
                            <a href="{{ asset('img/gallery/'.$gallery->image)}}" data-lightbox="example-set" data-title="believed">
                                <i class="fa fa-search"></i>
                            </a>
                            <a href="#">
                                <i class="fa fa-expand" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach

        </div><div class="center">
            <a href="{{ route('gallery') }}" class="donate-btn more-btn hvr-shutter-out-horizontal">View more</a>
        </div>
    </div>
    <!-- ======= gallery part end ======= -->

    <!-- ======= events part start ======= -->
{{--  <section class="events-area section-padding" id='upcoming-event'>
     <div class="container">
         <div class="row">
             <div class="col-md-8 col-md-offset-2 text-center">
                 <!-- section title -->
                 <div class="title">
                     <h2>Upcoming Events</h2>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis dictum tempus velit et tincidunt. In suscipit tempor interdum. Vestibulum porta mollis eros.</p>
                 </div>
             </div>
         </div>
         <div class="row">
             <!-- single events -->
             <div class="col-md-6 col-sm-6">
                 <div class="events-info">
                     <div class="events-date">
                         <span>08</span>
                         <span>May</span>
                         <span>2018</span>
                     </div>
                     <div class="events-details">
                         <a href="#">
                             <h3>World Red Cross And Red Crescent Day</h3>
                         </a>
                         <p>
                             Abia, Bauchi, Enugu and Kaduna state branches of the Nigerian Red Cross
                             conduct First Aid training for National Youth Corpers
                         </p>
                     </div>
                 </div>
             </div>
             <!-- single events -->
             <div class="col-md-6 col-sm-6">
                 <div class="events-info">
                     <div class="events-date">
                         <span>2018</span>
                         <span>May</span>
                     </div>
                     <div class="events-details">
                         <a href="#">
                             <h3>Health &amp; Hygiene Promotion Outreach</h3>
                         </a>
                         <p>Enugu state branch of the Nigerian Red Cross with the National Youth Corpers members of the branch community
                         development group promote Health and Hygiene at the Market Road primary school in Enugu</p>
                     </div>
                 </div>
             </div>
             <!-- single events -->
             <div class="col-md-6 col-sm-6">
                 <div class="events-info">
                     <div class="events-date">
                         <span>2018</span>
                         <span>May</span>
                     </div>
                     <div class="events-details">
                         <a href="#">
                             <h3>Data Harmonization Meeting in Nassarawa</h3>
                         </a>
                         <p>
                             The PMER and Volunteer Database Management Team hold a data harmonization meeting in Akwanga, Nassarawa State.
                         </p>
                     </div>
                 </div>
             </div>
         </div>
         <div class="row">
             <div class="col-md-6 col-sm-6">
                 <div class="events-info">
                     <div class="events-date">
                         <span>24</span>
                         <span>April</span>
                         <span>2018</span>
                     </div>
                     <div class="events-details">
                         <a href="#">
                             <h3>Communication Workshop</h3>
                         </a>
                         <p>
                         The Nigerian Red Cross Society holds a four-day workshop for newly appointed branch communication coordinators</p>
                     </div>
                 </div>
             </div>
             <!-- single events -->
             <div class="col-md-6 col-sm-6">
                 <div class="events-info">
                     <div class="events-date">
                         <span>30</span>
                         <span>April</span>
                         <span>2018</span>
                     </div>
                     <div class="events-details">
                         <a href="#">
                             <h3>Volunteer Training In Edo</h3>
                         </a>
                         <p>
                             The Nigerian Red Cross Society trains 50 volunteers in Edo branch on Lassa fever sensitization
                         </p>
                     </div>
                 </div>
             </div>
             <!-- single events -->
         </div>
     </div>
 </section>
 --}}<!-- ======= events part end ======= -->

    <!-- ======= donation part start ======= -->
    {{--  <section class="donation-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-0">
                    <form class="donate-form">
                        <div class="title">
                            <h2 class="text-white">make a donation</h2>
                        </div>
                        <div class="col-sm-6 mb">
                            <!-- form group -->
                            <div class="form-group">
                                <label><strong class="title-don">Recurring</strong></label> <br>
                            </div>
                            <!-- form group -->
                            <div class="form-group">
                                <label><strong class="don-type">Donation Amount</strong></label>
                                <div class="radio mt-5">
                                    <label class="radio-inline">
                                       <input name="t3" checked="" type="text"> <span>Naira</span>
                                    </label>
                                </div>
                                <label><strong class="don-type">How Often:</strong></label>
                                <div class="radio mt-5">
                                    <label class="radio-inline">
                                        <select name="t3" class="select">
                                          <option value="W">Weekly </option>
                                          <option value="M">Monthly </option>
                                          <option value="Y">Yearly </option>
                                        </select>
                                    </label>
                                    <button type="submit" class="donate-btn more-btn hvr-shutter-out-horizontal">donate now</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <!-- form group -->
                            <div class="form-group">
                                <label><strong class="title-don">One-Time Donation</strong></label> <br>
                            </div>
                            <!-- form group -->
                            <div class="form-group">
                                <label><strong class="don-type">Donation Amount</strong></label>
                                <div class="radio mt-5">
                                    <label class="radio-inline">
                                    <input name="t3" checked="" type="text"> <span>Naira</span>
                                    </label>
                                </div>
                                <div class="radio mt-5">
                                    <button type="submit" class="donate-btn more-btn hvr-shutter-out-horizontal">donate now</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>  ---}}
    <!-- ======= donation part end ======= -->
    <!-- ======= blog part start ======= -->
    {{--<section class="blog-area section-padding">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-8 col-md-offset-2 text-center">--}}
                    {{--<!-- section title -->--}}
                    {{--<div class="title">--}}
                        {{--<h2>latest News</h2>--}}
                        {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis dictum tempus velit et tincidunt. In suscipit tempor interdum. Vestibulum porta mollis eros.</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="row">--}}
                {{--<div class="col-sm-4">--}}
                    {{--<!-- Single news -->--}}
                    {{--<article class="news-info">--}}
                        {{--<div class="news-img">--}}
                            {{--<img src="img/news/01.jpg" alt="">--}}
                            {{--<div class="post-date-info">--}}
                                {{--<span>26</span> feb/18--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="news-content">--}}
                            {{--<div class="post-meta">--}}
                                {{--<ul>--}}
                                    {{--<li><a href="#"><i class="fa fa-user"></i> By Smith</a></li>--}}
                                    {{--<li><a href="#"><i class="fa fa-thumbs-up" aria-hidden="true"></i>253 likes</a></li>--}}
                                    {{--<li><a href="#"><i class="fa fa-commenting-o"></i> 23 Comments</a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                            {{--<a href="#">--}}
                                {{--<h2>post title here</h2>--}}
                            {{--</a>--}}
                            {{--<p>We care for children, protect their welfare, and prepare them for the future.</p>--}}
                            {{--<a href="#" class="donate-btn more-btn hvr-shutter-out-horizontal">read more</a>--}}
                        {{--</div>--}}
                    {{--</article>--}}
                {{--</div>--}}
                 {{--<div class="col-sm-4">--}}
                    {{--<!-- Single news -->--}}
                    {{--<article class="news-info">--}}
                        {{--<div class="news-img">--}}
                            {{--<img src="img/news/01.jpg" alt="">--}}
                            {{--<div class="post-date-info">--}}
                                {{--<span>26</span> feb/18--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="news-content">--}}
                            {{--<div class="post-meta">--}}
                                {{--<ul>--}}
                                    {{--<li><a href="#"><i class="fa fa-user"></i> By Smith</a></li>--}}
                                    {{--<li><a href="#"><i class="fa fa-thumbs-up" aria-hidden="true"></i>253 likes</a></li>--}}
                                    {{--<li><a href="#"><i class="fa fa-commenting-o"></i> 23 Comments</a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                            {{--<a href="#">--}}
                                {{--<h2>post title here</h2>--}}
                            {{--</a>--}}
                            {{--<p>We care for children, protect their welfare, and prepare them for the future.</p>--}}
                            {{--<a href="#" class="donate-btn more-btn hvr-shutter-out-horizontal">read more</a>--}}
                        {{--</div>--}}
                    {{--</article>--}}
                {{--</div>--}}
                 {{--<div class="col-sm-4">--}}
                    {{--<!-- Single news -->--}}
                    {{--<article class="news-info">--}}
                        {{--<div class="news-img">--}}
                            {{--<img src="img/news/01.jpg" alt="">--}}
                            {{--<div class="post-date-info">--}}
                                {{--<span>26</span> feb/18--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="news-content">--}}
                            {{--<div class="post-meta">--}}
                                {{--<ul>--}}
                                    {{--<li><a href="#"><i class="fa fa-user"></i> By Smith</a></li>--}}
                                    {{--<li><a href="#"><i class="fa fa-thumbs-up" aria-hidden="true"></i>253 likes</a></li>--}}
                                    {{--<li><a href="#"><i class="fa fa-commenting-o"></i> 23 Comments</a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                            {{--<a href="#">--}}
                                {{--<h2>post title here</h2>--}}
                            {{--</a>--}}
                            {{--<p>We care for children, protect their welfare, and prepare them for the future.</p>--}}
                            {{--<a href="#" class="donate-btn more-btn hvr-shutter-out-horizontal">read more</a>--}}
                        {{--</div>--}}
                    {{--</article>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="center">--}}
                {{--<a href="#" class="donate-btn more-btn hvr-shutter-out-horizontal">See more news</a>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}
    {{--<!-- ======= blog part end ======= -->--}}

      <!-- ======= patner area start ======= -->
    <div class="patner-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="partners-brand owl-carousel">
                        @foreach($movement_partners as $movement_partner)
                        <div class="single-partners-brand">
                            <a href="{{$movement_partner->link}}"><img src="{{asset('img/movement_partners/'.$movement_partner->image)}}" alt="ICRC Logo"></a>
                        </div>
                        <div class="single-partners-brand">

                        </div>
                        @endforeach


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ======= patner area end ======= -->


@endsection()

