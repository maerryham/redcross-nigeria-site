<?php

use App\State;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses' => 'PageController@indexPage',
    'as' => 'index'
]);

Route::get('/about-us', [
    'uses' => 'PageController@aboutUsPage',
    'as' => 'about-us'
]);

Route::get('/history', [
    'uses' => 'PageController@historyPage',
    'as' => 'history-page'
]);
//
//Route::get('/history',  function (){
//    return view('history');
//})->name('history-page');


//Contact page route
Route::get('/contact-us', [
    'uses' => 'PageController@contactUsPage',
    'as' => 'contact-us'
]);


Route::post('/contact-us', 'ContactController@postContact');
Route::get('/contact-us/send', 'ContactController@postContact')->name('contact-us/send');
Route::post('/contact-us/send', 'ContactController@send');

Route::get('download_manual', 'PageController@getDownload')->name('download_manual');
Route::get('download_report', 'PageController@getDownloadReport')->name('download_report');
Route::get('download_page', 'PageController@getDownloadPage')->name('download_page');


//signup route
Route::get('/signup', [
    'uses' => 'PageController@signUpPage',
    'as' => 'signup'
]);

Route::post('/signup', 'UsersSignedupController@postUserSignupDetails');


Route::post('/signup/store', 'UsersSignedupController@store');
Route::get('/signup/store', 'UsersSignedupController@store');

Route::get('/signup/success', function (){
    return view('signupsuccess');
})->name('success');

Route::get('/frequent-asked', [
    'uses' => 'PageController@frequentAsked',
    'as' => 'frequent-asked'
]);

//Route::post('/newsletter', 'NewsletterController@postUserSignupDetails');
//Route::get('/newsletter', 'NewsletterController@postUserSignupDetails');


Route::get('newsletter', [
    'uses' => 'PageController@newsletterPage',
    'as' => 'newsletter'
]);


Route::post('newsletter/subscribe', 'PageController@newsletterPost');


Route::get('/newsletter/subscribe', [
    'uses' => 'PageController@newsletter',
    'as' => 'newsletter/subscribe'
]);


//
//Route::post('/newsletter', 'NewsletterController@postEmail');
//Route::post('/newsletter/send', 'NewsletterController@send');
//


//For training


Route::post('/get-trained-now/train', 'TrainingController@train');


Route::get('/get-trained-now', [
    'uses' => 'TrainingController@trainingPage',
    'as' => 'get-trained-now'
]);


Route::get('/gallery', [
    'uses' => 'PageController@galleryPage',
    'as' => 'gallery'
]);

Route::get('news', [
    'uses' => 'PageController@newsPage',
    'as' => 'news'
]);

Route::get('archive', [
    'uses' => 'PageController@archivePage',
    'as' => 'archive'
]);

Route::get('archive/{page_link}', 'PageController@archiveFullView');
Route::get('newsletter/{page_link}', 'PageController@newsletterFullView');
Route::get('news/{page_link}', 'PageController@newsFullView');
Route::get('press_release/{page_link}', 'PageController@pressFullView');

Route::get('board-members', [
    'uses' => 'PageController@boardMembersPage',
    'as' => 'board-members'
]);


Route::get('board-members/{id}', 'PageController@singleBoardMember');

Route::get('head-of-departments', [
    'uses' => 'PageController@hoDPage',
    'as' => 'head-of-departments'
]);

Route::get('head-of-departments/{id}', 'PageController@singleHOD');



Route::get('branch-profile', [
    'uses' => 'PageController@branchProfilePage',
    'as' => 'branch-profile'
]);

Route::get('branch-profile/{state_id}', 'PageController@branchProfilePage');

Route::get('branch-profile/{state_id}/{id}', 'PageController@branchProfilePageothers');



Route::get('press_release', [
    'uses' => 'PageController@pressPage',
    'as' => 'press_release'
]);


Route::get('fundamentalprinciples', [
    'uses' => 'PageController@fundamentalprinciplesPage',
    'as' => 'fundamentalprinciples'
]);


Route::get('regions', [
    'uses' => 'PageController@regionsPage',
    'as' => 'regions'
]);




Route::group(['prefix' => 'donations'], function (){

    Route::get('/', [
        'uses' => 'DonationsController@donationPendingPage',
        'as' => 'donation-index-page'
    ]);

    Route::get('/confirmation/{ref}', [
        'uses' => 'DonationsController@confirmationPage',
        'as' => 'donation-confirm-page'
    ]);

    Route::post('/make-donation', [
        'uses' => 'DonationsController@donate',
        'as' => 'make-donation'
    ]);

    // Route::post('/verify-payment', [
    //     'uses' => 'DonationsController@verifyDonationPayment',
    //     'as' => 'verify-donation-payment'
    // ]);

});

// Route::post('/verify-payment', [
//     'uses' => 'DonationsController@verifyDonationPayment',
//     'as' => 'verify-donation-payment'
// ]);
Route::get('coming-soon', function (){
    return view('coming-soon');
})->name('cmming-soon');

Route::get('contact/success', function (){
    return view('success');
})->name('success');

Auth::routes();

Route::get('logout', array('uses' => 'HomeController@doLogout'));

Route::group(['middleware' => 'Headquaters'], function () {

Route::get('/home', 'HomeController@index')->name('home');

//Contact Details

Route::get('/admin/contact_details', 'HomeController@contactEdit')->name('admin/contact_details');
Route::get('/admin/contact_details_edit', 'HomeController@contactEdit')->name('admin/contact_details_edit');
Route::post('/admin/contact_details_edit', 'HomeController@contactEditPost')->name('admin/contact_details_edit');


//About Page
Route::get('/admin/about', 'HomeController@about')->name('admin/about');
Route::get('/admin/about_edit', 'HomeController@aboutEdit')->name('about_edit');
Route::post('/admin/about_edit', 'HomeController@aboutEditPost')->name('about_edit');

//Document for download Page
Route::get('/admin/download_page', 'HomeController@documentManual')->name('admin/download_page');
Route::get('/admin/download_page_edit', 'HomeController@documentManualEdit')->name('admin/download_page_edit');
Route::post('/admin/download_page_edit', 'HomeController@documentManualEditPost')->name('admin/download_page_edit');
    Route::get('/admin/download_page_edit/{id}', 'HomeController@documentManualEdit');

    Route::get('/admin/download_page_upload', 'HomeController@documentManualUpload')->name('admin/download_page_upload');
    Route::post('/admin/download_page_upload', 'HomeController@documentManualUploadPost')->name('admin/download_page_upload');
    Route::get('/admin/delete_document/{id}', 'HomeController@deleteDocument');

Route::get('/admin/document_report', 'HomeController@documentReport')->name('admin/document_report');
Route::get('/admin/document_report_edit', 'HomeController@documentReportEdit')->name('admin/document_report_edit');
Route::post('/admin/document_report_edit', 'HomeController@documentReportEditPost')->name('admin/document_report_edit');


//History Page

Route::get('/admin/history', 'HomeController@history')->name('admin/history');

Route::get('/admin/history_edit', 'HomeController@historyEdit')->name('history_edit');
Route::post('/admin/history_edit', 'HomeController@historyEditPost')->name('history_edit');

//Gallery Page

Route::get('/admin/gallery', 'HomeController@gallery')->name('admin/gallery');

Route::get('/admin/gallery_edit/{id}', 'HomeController@galleryEdit');
Route::post('/admin/gallery_edit/', 'HomeController@galleryEditPost');
Route::get('/admin/gallery_upload', 'HomeController@galleryUpload')->name('admin/gallery_upload');
Route::post('/admin/gallery_upload', 'HomeController@galleryUploadPost');
Route::get('/admin/delete_gallery/{id}', 'HomeController@deleteGallery');

//News Page

Route::get('/admin/news', 'HomeController@news')->name('admin/news');

Route::get('/admin/news_edit/{id}', 'HomeController@newsEdit');
Route::post('/admin/news_edit/', 'HomeController@newsEditPost');

Route::get('/admin/news_upload', 'HomeController@newsUpload')->name('admin/news_upload');

Route::post('/admin/news_upload', 'HomeController@newsUploadPost');

Route::get('/admin/delete_news/{id}', 'HomeController@deleteNews');

//Board Members
Route::get('/admin/board_members', 'HomeController@boardMembers')->name('admin/board_members');
Route::get('/admin/board_members_edit/{id}', 'HomeController@boardMembersEdit');
Route::post('/admin/board_members_edit/', 'HomeController@boardMembersEditPost');

Route::get('/admin/board_members_upload', 'HomeController@boardMembersUpload')->name('admin/board_members_upload');
Route::post('/admin/board_members_upload', 'HomeController@boardMembersUploadPost');

Route::get('/admin/delete_board_member/{id}', 'HomeController@deleteBoardMember');

//Head of Department
Route::get('/admin/head_of_departments', 'HomeController@hOD')->name('admin/head_of_departments');
Route::get('/admin/head_of_departments_edit/{id}', 'HomeController@hODEdit');
Route::post('/admin/head_of_departments_edit/', 'HomeController@hODEditPost');

Route::get('/admin/head_of_departments_upload', 'HomeController@hODUpload')->name('admin/head_of_departments_upload');
Route::post('/admin/head_of_departments_upload', 'HomeController@hODUploadPost');

Route::get('/admin/delete_board_member/{id}', 'HomeController@deletehOD');

//Movement Partners

Route::get('/admin/movement_partners', 'HomeController@movementPartners')->name('admin/movement_partners');

Route::get('/admin/movement_partners_edit/{id}', 'HomeController@movementPartnersEdit');
Route::post('/admin/movement_partners_edit/', 'HomeController@movementPartnersEditPost');

Route::get('/admin/movement_partners_upload', 'HomeController@movementPartnersUpload')->name('admin/movement_partners_upload');
Route::post('/admin/movement_partners_upload', 'HomeController@movementPartnersUploadPost');

Route::get('/admin/delete_movement_partners/{id}', 'HomeController@deleteMovementPartners');


//Press Release Page

Route::get('/admin/press_release', 'HomeController@pressRelease')->name('admin/press_release');

Route::get('/admin/press_release_edit/{id}', 'HomeController@pressReleaseEdit');
Route::post('/admin/press_release_edit/', 'HomeController@pressReleaseEditPost');

Route::get('/admin/press_release_upload', 'HomeController@pressReleaseUpload')->name('admin/press_release_upload');
Route::post('/admin/press_release_upload', 'HomeController@pressReleaseUploadPost');

Route::get('/admin/delete_press/{id}', 'HomeController@deletePress');


//FAQs Page

    Route::get('/admin/faqs', 'HomeController@faqs')->name('admin/faqs');
    Route::get('/admin/faqs_edit/{id}', 'HomeController@faqsEdit');
    Route::post('/admin/faqs_edit/', 'HomeController@faqsEditPost');

    Route::get('/admin/faqs_upload', 'HomeController@faqsUpload')->name('admin/faqs_upload');
    Route::post('/admin/faqs_upload', 'HomeController@faqsUploadPost');

    Route::get('/admin/delete_faqs/{id}', 'HomeController@deleteFaqs');

//Newsletter Page

Route::get('/admin/newsletter', 'HomeController@newsletter')->name('admin/newsletter');

Route::get('/admin/newsletter_edit/{id}', 'HomeController@newsletterEdit');
Route::post('/admin/newsletter_edit/', 'HomeController@newsletterEditPost');

Route::get('/admin/newsletter_upload', 'HomeController@newsletterUpload')->name('admin/newsletter_upload');
Route::post('/admin/newsletter_upload', 'HomeController@newsletterUploadPost');

Route::get('/admin/delete_newsletter/{id}', 'HomeController@deleteNewsletter');




//End of Middleware for headquaters only
});

Route::get('verify', function(){

    $user = Auth::user();
    if ($user->isAdmin()) {
        return redirect('home');
    }
    return redirect('admin/branch_profile');
});





//Branch Profile

Route::group(['middleware' => 'State'], function () {


Route::get('/admin/branch_profile', 'HomeController@branchProfile')->name('admin/branch_profile');

Route::get('/admin/branch_profile/{id}', 'HomeController@branchProfileFullView');


Route::get('/admin/branch_profile_upload', 'HomeController@branchProfileUpload')->name('admin/branch_profile_upload');

Route::post('/admin/branch_profile_upload', 'HomeController@branchProfileUploadPost');


Route::get('/admin/branch_profile_edit/{id}', 'HomeController@branchProfileEdit');
Route::post('/admin/branch_profile_edit/', 'HomeController@branchProfileEditPost');

Route::get('/admin/delete_branch_profile/{id}', 'HomeController@deleteBranchProfile');

});


//Others
//Route::get('/admin/get_all_state', function(){
//
//
//    User::create([
//            'name' => "Mariam",
//            'email' => "lawalmariamyetunde@gmail.com",
//            'role_id' => 1,
//            'password' => Hash::make('awesome'),
//        ]);
//
//});
