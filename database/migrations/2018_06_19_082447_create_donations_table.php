<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('reference');
            $table->string('phone_number')->nullable();
            $table->decimal('amount')->default(0);
            $table->string('cause')->default(0);
            $table->string('dcause')->default(0);
            $table->integer('region')->default(0);
            $table->text('message')->nullable();
            $table->boolean('anonymous')->default(false);
            $table->enum('status', ['pending','success', 'failed'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donations');
    }
}
