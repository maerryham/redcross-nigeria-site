<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('state_id');
            $table->date('date');
            $table->string('subject');
            $table->string('content');
            $table->string('branch_services');
            $table->integer('volunteer_strength');
            $table->integer('active_volunteer');
            $table->string('video_link');
            $table->string('images');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_profiles');
    }
}
