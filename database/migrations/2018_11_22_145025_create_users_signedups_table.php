<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersSignedupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_signedups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('state_id');
            $table->string('local_govt_id');
            $table->string('first_name');
            $table->string('surname');
            $table->string('phone_number');
            $table->date('date_of_birth');
            $table->string('title');
            $table->integer('education_level_id');
            $table->string('discipline');
            $table->longText('res_address');
            $table->longText('work_address');
            $table->string('occupation');
            $table->string('gender');
            $table->integer('marital_status');
            $table->boolean('make_donations')->nullable();
            $table->boolean('be_a_member')->nullable();
            $table->boolean('be_a_volunteer')->nullable();
            $table->longText('about_you');
            $table->string('email');
            $table->char('password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_signedups');
    }
}
