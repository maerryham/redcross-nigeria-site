<?php

use Illuminate\Database\Seeder;
use App\Region;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = ['ABIA', 'ABUJA', 'ADAMAWA', 'AKWA IBOM', 'ANAMBRA ', 'ENUGU', 'BAUCHI','BAYELSA' , 'BENUE', 'BORNO', 'CROSS RIVER',  'DELTA',  'EBONYI',  'EDO',  'EKITI',  'ENUGU', 
        'GOMBE', 'IMO', 'JIGAWA', 'KADUNA', 'KANO', 'KATSINA', 'KEBBI', 'KOGI', 'KWARA', 'LAGOS', 'NASARAWA', 'OGUN',  
        'ONDO', 'OSUN', 'OYO',  'PLATEAU',  'RIVERS',  'SOKOTO',  'TARABA',  'YOBE',  'ZAMFARA'];
        
        collect($regions)->each(function ($region){
            $regionModel = new Region();
            $regionModel->name = $region;
            $regionModel->save();
        });
    }
}



  



