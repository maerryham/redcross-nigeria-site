<?php

use Illuminate\Database\Seeder;

class LocalGovtSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $local_govt = ['ABIA', 'ABUJA', 'ADAMAWA', 'AKWA IBOM', 'ANAMBRA ', 'ENUGU', 'BAUCHI','BAYELSA' , 'BENUE', 'BORNO', 'CROSS RIVER',  'DELTA',  'EBONYI',  'EDO',  'EKITI',  'ENUGU',
            'GOMBE', 'IMO', 'JIGAWA', 'KADUNA', 'KANO', 'KATSINA', 'KEBBI', 'KOGI', 'KWARA', 'LAGOS', 'NASARAWA', 'OGUN',
            'ONDO', 'OSUN', 'OYO',  'PLATEAU',  'RIVERS',  'SOKOTO',  'TARABA',  'YOBE',  'ZAMFARA'];

        collect($local_govt)->each(function ($local_govt){
            $regionModel = new Region();
            $regionModel->name = $local_govt;
            $regionModel->save();
        });
    }
}
