<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\PasswordReset;
use App\Role;

class User extends Authenticatable
{
    use Notifiable;

    protected $token;
//    public function __construct($token) {
//        $this->token = $token;
//    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



//    public function sendPasswordResetNotification($token)
//    {
//        $this->notify(new ResetPasswordNotification($token));
//    }

    public function role(){
        return $this->belongsTo('App\Role');
    }

    public function isAdmin(){

        if($this->role->role_name == 'Headquaters'){
            return true;
        }
        return false;
    }

}
