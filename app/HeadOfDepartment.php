<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeadOfDepartment extends Model
{
    public function state(){
        return $this->belongsTo('App\State');
    }

    public $table = 'head_of_departments';
}
