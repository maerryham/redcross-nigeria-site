<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class State
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if (!$user) {
//            echo "You are not the Admin";
            return redirect('login');
        }
        if ($user->isAdmin()) {
//            if you are headquaters, go back to home
            return redirect('home');
        }
        return $next($request);
    }
}
