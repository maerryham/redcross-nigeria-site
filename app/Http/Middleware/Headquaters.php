<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Headquaters
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = Auth::user();
        if (!$user) {
//            echo "You are not the Admin";
            return redirect('login');
        }
        if (!$user->isAdmin()) {
//            if you are not headquaters, go back to branch profile
            return redirect('admin/branch_profile');
        }
        return $next($request);
    }
}
