<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DonationRequest;
use App\Donation;
use Yabacon\Paystack as Paystack;
use App\Cause;
use App\Region;
use App\ContactDetail;

class DonationsController extends Controller
{

    public function donationPendingPage(){
        $causes = Cause::all();
        $regions = Region::all();
        $contact_detail = ContactDetail::find(1);
        return view('donations.index', [
            'causes' => $causes,
            'regions' => $regions,
            'contact_detail' => $contact_detail
        ]);
    }

    public function donate(DonationRequest $request){
        $data = $request->all();
        if($request->cause == "other"){
            $data['cause'] = $request->dcause;
        }

        $donation = Donation::create($data);
        if(!$donation){
            return response()->json([
                'status' => false,
                'message' => 'unable to process request'
            ], 200);
        }
        return response()->json([
            'status' => true,
            'data' => $donation
        ], 201);
    }

    public function confirmationPage(Request $request, $ref){

        $contact_detail = ContactDetail::find(1);
        $data= $this->verifyDonationPayment($ref);
        $this->updateDonationStatus(explode('-', $ref)[1], $data->status);
        return view('donations.donation-confirm')->with(['contact_detail' => $contact_detail]);
    }

    protected function verifyDonationPayment($ref){
        $paystack = new Paystack(env('PAYSTACK_SECRET_KEY'));
        try{
            $response = $paystack->transaction->verify([
                'reference' => $ref
            ]);

        }catch(\Yabacon\Paystack\Exception\ApiException $e){
            return response(['status' => false, 'message' => 'Internal server error'], 500);
        }
        return  $response->data;


    }

    protected function updateDonationStatus($ref, $status){
        return Donation::where('reference', $ref)->update([
            'status' => $status
        ]);
    }

    protected function sendEmail(){
        // send thank you email...
    }

}
