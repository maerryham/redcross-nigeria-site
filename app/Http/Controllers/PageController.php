<?php

namespace App\Http\Controllers;

use App\Document;
use App\Faq;
use App\HeadOfDepartment;
use App\NewsletterPost;
use App\Region;
use App\State;
use App\Aboutpage;
use App\Gallery;
use App\News;
use App\BoardMember;
use Carbon\Carbon;
use App\MovementPartners;
use App\PressRelease;
use App\HistoryPage;
use App\ContactDetail;
use App\BranchProfile;
use App\Newsletter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


class PageController extends Controller
{
    public function indexPage(){
        $slider = News::orderBy('id', 'desc')->paginate(4);
        $about_page = Aboutpage::find(1);

        $gallery = Gallery::orderBy('id', 'desc')->paginate(12);
        $movement_partners = MovementPartners::orderBy('id', 'desc')->get();

        $contact_detail = ContactDetail::find(1);

        return view('index')->with(['slider' => $slider, 'about' => $about_page,
            'gallerys' => $gallery, 'movement_partners' => $movement_partners, 'contact_detail' => $contact_detail]);
    }

    public function aboutUsPage(){
        $about_page = Aboutpage::find(1);
        $contact_detail = ContactDetail::find(1);
        return view('about-us')->with(['page_data' => $about_page, 'contact_detail' => $contact_detail]);
    }

    public function historyPage(){
        $history_page = HistoryPage::find(1);
        $contact_detail = ContactDetail::find(1);
        return view('history')->with(['history' => $history_page, 'contact_detail' => $contact_detail]);
    }

    

    public function contactUsPage(){
        $contact_detail = ContactDetail::find(1);
        return view('contact-us')->with(['contact_detail' => $contact_detail]);
    }

    public function signUpPage(){
        $states = State::all();
        $contact_detail = ContactDetail::find(1);
        return view('signup')->with(['contact_detail' => $contact_detail, 'states' => $states]);
    }

    public function frequentAsked(){
        $contact_detail = ContactDetail::find(1);
        $faqs = Faq::all();
        return view('frequent-asked')->with(['contact_detail' => $contact_detail,'faqs' => $faqs]);
    }



    public function galleryPage(){
        $gallery = Gallery::orderBy('id', 'desc')->paginate(12);
        $contact_detail = ContactDetail::find(1);
        return view('gallery')->with(['gallerys' => $gallery, 'page' => 'gallery', 'contact_detail' => $contact_detail]);
    }

    public function newsPage(){
        $contact_detail = ContactDetail::find(1);
        $news = News::orderBy('id', 'desc')->where('date', '>=', Carbon::now()->startOfMonth())->paginate(12);
        if(count($news ) == 0){
//            $news = News::orderBy('date', 'desc')->paginate(10);
            $news = News::orderBy('id', 'desc')->where('date', '>=', Carbon::now()->subMonth())->paginate(10);
        }


        return view('news')->with(['new' => $news, 'page' => 'news', 'contact_detail' => $contact_detail]);
    }

    public function newsletterPage(){
        $contact_detail = ContactDetail::find(1);
        $newsletter = NewsletterPost::orderBy('date', 'desc')->get();

        return view('newsletter')->with(['contact_detail' => $contact_detail, 'newsletter' => $newsletter]);
    }

    public function boardMembersPage(){
        $contact_detail = ContactDetail::find(1);
        $board_members = BoardMember::orderBy('id', 'desc')->paginate(10);

        return view('board-members')->with(['boardmembers' => $board_members, 'page' => 'news', 'contact_detail' => $contact_detail]);
    }

    public function singleBoardMember($id){
        $contact_detail = ContactDetail::find(1);
        $board_members = BoardMember::find($id);

        return view('board-member-single')->with(['boardmember' => $board_members, 'page' => 'news', 'contact_detail' => $contact_detail]);
    }

    public function hoDPage(){
        $contact_detail = ContactDetail::find(1);
        $board_members = HeadOfDepartment::orderBy('id', 'desc')->paginate(10);

        return view('head-of-department')->with(['boardmembers' => $board_members, 'page' => 'news', 'contact_detail' => $contact_detail]);
    }

    public function singleHOD($id){
        $contact_detail = ContactDetail::find(1);
        $board_members = HeadOfDepartment::find($id);

        return view('head-of-department-single')->with(['boardmember' => $board_members, 'page' => 'news', 'contact_detail' => $contact_detail]);
    }
    public function branchProfilePage($state_id){
        $contact_detail = ContactDetail::find(1);
        $state = State::find($state_id);
        $branch_profile = BranchProfile::orderBy('date', 'desc')->where('state_id',$state_id)->first();

        $previous = BranchProfile::orderBy('date', 'desc')->where('state_id',$state_id)->where('date','<',$branch_profile->date)->first();
        $next = BranchProfile::orderBy('date', 'asc')->where('state_id',$state_id)->where('date','>',$branch_profile->date)->first();

        if(is_null($branch_profile ) || empty($branch_profile)){
            return "$state->state_name State has not uploaded any Profile";
        }
        return view('branch-profile')->with(['contact_detail' => $contact_detail,
            'state' => $state,
            'branch_profile' => $branch_profile,
            'previous' => $previous,
            'next' => $next,
            ]);
    }
    public function branchProfilePageothers($state_id, $id){
        $contact_detail = ContactDetail::find(1);

        // id is the id of the post
        $state = State::find($state_id);
        $branch_profile = BranchProfile::orderBy('date', 'desc')->where('state_id',$state_id)->where('id',$id)->first();

        $previous = BranchProfile::orderBy('date', 'desc')->where('state_id',$state_id)->where('date','<',$branch_profile->date)->first();
        $next = BranchProfile::orderBy('date', 'asc')->where('state_id',$state_id)->where('date','>',$branch_profile->date)->first();

        if(is_null($branch_profile ) || empty($branch_profile)){
            return "Invalid Branch Profile";
        }
        return view('branch-profile')->with(['contact_detail' => $contact_detail,
            'state' => $state,
            'previous' => $previous,
            'next' => $next,
            'branch_profile' => $branch_profile]);
    }

    public function archivePage(){
        $contact_detail = ContactDetail::find(1);
        //if latest is last month post
        //Make archive last two month post
        $latest = News::orderBy('id', 'desc')->where('date', '>=', Carbon::now()->startOfMonth())->paginate(10);
        if(count($latest ) == 0){
//            $news = News::orderBy('date', 'desc')->paginate(10);
            $archive = News::orderBy('id', 'desc')->where('date', '<', Carbon::now()->subMonth())->paginate(10);
        }else{
            $archive = News::orderBy('id', 'desc')->where('date', '<=', Carbon::now()->subMonth())->paginate(10);
        }
        return view('archive')->with(['archives' => $archive, 'page' => 'archive', 'contact_detail' => $contact_detail]);
    }


    public function archiveFullView($page_link){
        $contact_detail = ContactDetail::find(1);

        $one_archive = News::where('page_link', $page_link)->first();
        return view('archive_one')->with(['news' => $one_archive, 'page' => 'archive', 'contact_detail' => $contact_detail]);

    }

    public function newsFullView($page_link){
        $contact_detail = ContactDetail::find(1);

        $one_archive = News::where('page_link', $page_link)->first();
        return view('news_one')->with(['news' => $one_archive, 'page' => 'news', 'contact_detail' => $contact_detail]);

    }

    public function pressFullView($page_link){
        $contact_detail = ContactDetail::find(1);

        $one_archive = PressRelease::where('page_link', $page_link)->first();
        return view('press_release_one')->with(['news' => $one_archive, 'page' => 'press_release', 'contact_detail' => $contact_detail]);

    }

    public function newsletterFullView($page_link){
        $contact_detail = ContactDetail::find(1);

        $one_newsletter = NewsletterPost::where('page_link', $page_link)->first();
        return view('newsletter_one')->with(['news' => $one_newsletter, 'page' => 'newsletter', 'contact_detail' => $contact_detail]);

    }


    public function pressPage(){

        $contact_detail = ContactDetail::find(1);

        $press_releases = PressRelease::all();
        return view('press_release')->with(['press_releases' => $press_releases, 'page' => 'press_releases', 'contact_detail' => $contact_detail]);

    }

    public function fundamentalprinciplesPage(){
        $contact_detail = ContactDetail::find(1);
//        return redirect()->route('coming-soon');
        return view('fundamentalprinciples')->with(['contact_detail' => $contact_detail]);
    }


    public function regionsPage(){
        $contact_detail = ContactDetail::find(1);
        return view('regions')->with(['contact_detail' => $contact_detail]);
    }

    public function getDownload()
    {
        //PDF file is stored under project/public/download/info.pdf
        $document = Document::find(1);

        $file= public_path(). "/download/".$document->branch_manual;

        $headers = [
            'Content-Type' => 'application/pdf',
        ];

        return response()->download($file, $document->branch_manual, $headers);

    }

    public function getDownloadReport()
    {
        $document = Document::find(1);
        //PDF file is stored under project/public/download/info.pdf
        $file= public_path(). "/download/".$document->annual_report;

        $headers = [
            'Content-Type' => 'application/pdf',
        ];

        return response()->download($file, $document->annual_report, $headers);

    }

    public function getDownloadPage()
    {
        $document = Document::where('id','!=','0')->get();
//        //PDF file is stored under project/public/download/info.pdf
//        $file= public_path(). "/download/".$document->annual_report;
//
//        $headers = [
//            'Content-Type' => 'application/pdf',
//        ];
//
//        return response()->download($file, $document->annual_report, $headers);
        $contact_detail = ContactDetail::find(1);
        return view('download-page')->with(['contact_detail' => $contact_detail,'documents' => $document, 'page' => 'download_page' ]);

    }

    public function newsletter(){
        $contact_detail = ContactDetail::find(1);
        return view('newsletter_subscribe')->with(['contact_detail' => $contact_detail, 'page' => 'newsletter' ]);
    }

    public function newsletterPost(Request $request)
    {
        $contact_detail = ContactDetail::find(1);
       $exist_email = Newsletter::where('email', $request->email)->get();
        if ( count($exist_email) == 0) {
            // Send Mail



            //Make the mail send without connection issues
            try{

                Mail::send('emails.mailClientNewsletterSubscription', [ 'request' => $request->all() ], function($message) use($request) {
                    $message->sender('newsletter@redcrossnigeria.org', $name = 'Nigerian RedCross');
                    $message->from('newsletter@redcrossnigeria.org', $name = 'Nigerian RedCross');
                    $message->to("$request->email");
                    $message->subject('Thanks for subscribing to Our Newsletter');

                });

            }catch(Exception $e){

            }



            //Save to Db

            $newsletterModel = new Newsletter();

            $newsletterModel->email = $request->email;

            $newsletterModel->save();



            return redirect()->to(route('newsletter/subscribe').'#message')->with("message", "Thanks for subscribing!!");

        }
        return redirect()->to(route('newsletter/subscribe').'#message')->with("message", "You are already subscribed to our newsletter");


    }




}
