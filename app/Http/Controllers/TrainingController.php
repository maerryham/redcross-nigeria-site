<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;
use App\Training;
use App\ContactDetail;
use Illuminate\Support\Facades\Mail;

class TrainingController extends Controller
{
    //
    public function training(Request $request)
    {

//        $regions = Region::all();

//        $regions = DB::table('regions')->select('name')->get();
        $contact_detail = ContactDetail::find(1);

        return view('contact-us')->with(['contact_detail' => $contact_detail]);

    }

    public function trainingPage(){
        $contact_detail = ContactDetail::find(1);

        return view('get-trained-now')->with(['contact_detail' => $contact_detail]);
    }


    public function postTraining(Request $request)
    {

        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'subject' => 'required',
            'training' => 'required',
            'message' => 'required',


        ]);


    }

    public function train(Request $request)
    {
        $userModel = new Training;
        $userModel->name = $request->name;
        $userModel->email = $request->email;
        $userModel->phone_number = $request->phone;
        $userModel->training = $request->training;
        $userModel->message = $request->message;

        $userModel->save();

        Mail::send('emails.mailAdminTrainingNotification', [ 'request' => $request->all()], function($message) use($request) {
            $message->sender('traincert@redcrossnigeria.org', $name = 'Nigerian RedCross');
            $message->from('traincert@redcrossnigeria.org', $name = 'NRCS Training Page');
            $message->to('traincert@redcrossnigeria.org');
            $message->subject('Message from Training Page');

        });

        return redirect()->to(route('get-trained-now').'#training')->with("message", "Submitted Successfully!");


    }

}
