<?php

namespace App\Http\Controllers;
use App\Contact;

use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function contact(Request $request)
    {

//        $regions = Region::all();

//        $regions = DB::table('regions')->select('name')->get();

        return view('contact-us');

    }


    public function postContact(Request $request)
    {

        $validatedData = $request->validate([
            'name' => 'required',
            'emails' => 'required',
            'phone' => 'required',
            'subject' => 'required',
            'message' => 'required',


        ]);


    }

    public function send(Request $request)
    {
        $userModel = new Contact;
        $userModel->name = $request->name;
        $userModel->email = $request->email;
        $userModel->phone_number = $request->phone;
        $userModel->subject = $request->subject;
        $userModel->message = $request->message;

        $userModel->save();


        Mail::send('emails.mailAdminContactNotification', [ 'request' => $request->all()], function($message) use($request) {
            $message->sender('admin@redcrossnigeria.org', $name = 'Nigerian RedCross');
            $message->from('admin@redcrossnigeria.org', $name = 'NRCS Contact Page');
            $message->to('admin@redcrossnigeria.org');
            $message->subject('Message from Contact Page');

        });

        return redirect()->to(route('contact-us').'#message')->with("message", "Thanks for contacting us, We would get back to you soon!");

    }

}
