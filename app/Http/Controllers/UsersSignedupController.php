<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\users_signedup;
use App\Region;



class UsersSignedupController extends Controller
{


    public function signup(Request $request)
    {

//        $regions = Region::all();

//        $regions = DB::table('regions')->select('name')->get();

        return view('signup', compact('regions'));

    }


    public function postUserSignupDetails(Request $request)
    {

        $validatedData = $request->validate([
            'state_id' => 'required',
            'local_govt_id' => 'required',
            'first_name' => 'required',
            'surname' => 'required',
            'phone_number' => 'required',
            'date_of_birth' => 'required',
            'title' => 'required',
            'education_level_id' => 'required|numeric',
            'discipline' => 'required',
            'res_address' => 'required',
            'work_address' => 'required',
            'occupation' => 'required',
            'gender' => 'required|numeric',
            'marital_status' => 'required|numeric',
            'make_donations' => 'numeric',
            'be_a_member' => 'numeric',
            'be_a_volunteer' => 'numeric',
            'about_you' => 'required',
            'email' => 'required|unique:users_signedups',
            'password' => 'required'

        ]);




    }

    public function store(Request $request)
    {

        $make_donations = $request->has('make_donations') ? 1 : 0;
        $be_a_member = $request->has('be_a_member') ? 1 : 0;
        $be_a_volunteer = $request->has('be_a_volunteer') ? 1 : 0;
      // $gender = $request->gender == 'checked' ? 1 : 0;

        $userModel = new users_signedup;
        $userModel->state_id = $request->state_id;
        $userModel->local_govt_id = $request->local_govt_id;
        $userModel->first_name = $request->first_name;
        $userModel->surname = $request->surname;
        $userModel->phone_number = $request->phone_number;
        $userModel->date_of_birth = $request->date_of_birth;
        $userModel->title = $request->title;
        $userModel->education_level_id = $request->education_level_id;
        $userModel->discipline = $request->discipline;
        $userModel->work_address = $request->work_address;
        $userModel->res_address = $request->res_address;
        $userModel->occupation = $request->occupation;
        $userModel->gender = $request->gender;
        $userModel->marital_status = $request->marital_status;
        $userModel->make_donations = $make_donations;
        $userModel->be_a_member = $be_a_member;
        $userModel->be_a_volunteer = $be_a_volunteer;
        $userModel->about_you = $request->about_you;
        $userModel->email = $request->email;
        $userModel->password = $request->password;
        $userModel->save();

//        $users_signedup->save();
//        return redirect('/signup/success');
        return redirect()->to(route('signup').'#signup')->with("message", "Your Signup is successfull!");
    }


//$userModel->state_id = $request->session()->get('users_signedup.state_id');
//        $userModel->local_govt_id = $request->session()->get('users_signedup.local_govt_id');
//        $userModel->first_name = $request->session()->get('users_signedup.first_name');
//        $saveUser = $userModel->save();

}
