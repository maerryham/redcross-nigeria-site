<?php

namespace App\Http\Controllers;

use App\BranchProfile;
use App\Document;
use App\MovementPartners;
use App\Newsletter;
use App\NewsletterPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Aboutpage;
use App\HistoryPage;
use App\Gallery;
use App\News;
use App\PressRelease;
use App\Faq;
use App\BoardMember;
use App\HeadOfDepartment;
use App\ContactDetail;
use Auth;
use Redirect;
use App\State;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contact_details = ContactDetail::find(1);
        return view('admin.contact_details')->with(['page' => 'home', 'page_data' => $contact_details]);
    }

    public function doLogout()
    {
        Auth::logout(); // log the user out of our application
        return Redirect::to('login'); // redirect the user to the login screen
    }

    public function contactEdit()
    {
        $contact_page = ContactDetail::find(1);
        return view('admin.contact_details_edit')->with(['page_data' => $contact_page, 'page' => 'home']);
    }

    public function contactEditPost(Request $request)
    {

        ContactDetail::where('id', 1)->update([

            'address' => $request->address,
            'email' => $request->email,
            'phone' => $request->phone,
            'facebook' => $request->facebook,
            'twitter' => $request->twitter,
            'instagram' => $request->instagram,
            'first_message_h' => $request->first_message_h,
            'second_message_h' => $request->second_message_h,
            'third_message_h' => $request->third_message_h,
            'first_message_f' => $request->first_message_f,
            'second_message_f' => $request->second_message_f,
            'third_message_f' => $request->third_message_f
        ]);

        return redirect()->to(route('admin/contact_details').'#message')->with("message", "Contact Details edited Successfully!");
    }
//About
    public function about()
    {
        $about_page = Aboutpage::find(1);
        return view('admin.about_page')->with(['page_data' => $about_page, 'page' => 'about']);
    }
    public function aboutEdit()
    {
        $about_page = Aboutpage::find(1);
        return view('admin.about_edit')->with(['page_data' => $about_page, 'page' => 'about']);
    }

    public function aboutEditPost(Request $request)
    {



        if($request->hasFile('image')) {



            $file = $request->file('image');

            $name = time() . '-' . $file->getClientOriginalName();


            $file->move('img/about',$name);

            Aboutpage::where('id', 1)->update([

                'who_we_are' => $request->who_we_are,
                'our_mission' => $request->our_mission,
                'our_vision' => $request->our_vision,
                'image' => $name

            ]);


        }else{
            Aboutpage::where('id', 1)->update([

                'who_we_are' => $request->who_we_are,
                'our_mission' => $request->our_mission,
                'our_vision' => $request->our_vision,

            ]);
        }

        return redirect()->to(route('admin/about').'#message')->with("message", "About page edited Successfully!");
    }


    //Document Manual Download

    public function documentManual()
    {
        $document_manual = Document::all();
        return view('admin.document_manual')->with(['page_data' => $document_manual, 'page' => 'download_page']);
    }

    public function documentManualUpload()
    {
        $document_manual = Document::all();
        return view('admin.document_manual_upload')->with(['page_data' => $document_manual, 'page' => 'download_page']);
    }

    public function documentManualUploadPost(Request $request)
    {

        $file = $request->file('image');

        $name = time() . '-' . $file->getClientOriginalName();


        $file->move('img/download',$name);


        $userModel = new Document();
        $userModel->file_name = $name;
        $userModel->title = $request->title;
        $userModel->save();

        return redirect()->to(route('admin/download_page').'#message')->with("message", "Document Uploaded Successfully!");
    }


    public function documentManualEdit($id)
    {
        $document_manual = Document::find($id);
        return view('admin.document_manual_edit')->with(['board_members' => $document_manual, 'page' => 'document_manual']);
    }


    public function documentManualEditPost(Request $request)
    {

        if($request->hasFile('image')) {

            $file = $request->file('branch_manual');

            $name = time() . '-' . $file->getClientOriginalName();

            $file->move('download',$name);

            Document::where('id',  $request->id)->update([

                'title' =>  $request->title,
                'file_name' => $name

            ]);


        }else{
            Document::where('id', $request->id)->update([

                'title' => $request->title,

            ]);
        }

        return redirect()->to(route('admin/download_page').'#message')->with("message", "Download Page Uploaded Successfully!");
    }

    public function deleteDocument($id){
        Document::where('id', $id)->delete();

        return redirect()->to(route('admin/download_page').'#message')->with("message", "Document Deleted Successfully!");
    }

    //Document Report Download

    public function documentReport()
    {
        $document_manual = Document::find(1);
        return view('admin.document_report')->with(['page_data' => $document_manual, 'page' => 'document_report']);
    }

    public function documentReportEdit()
    {
        $document_report = Document::find(1);
        return view('admin.document_report_edit')->with(['page_data' => $document_report, 'page' => 'document_report']);
    }


    public function documentReportEditPost(Request $request)
    {



        if($request->hasFile('annual_report')) {



            $file = $request->file('annual_report');

            $name = time() . '-' . $file->getClientOriginalName();


            $file->move('download',$name);

            Document::where('id', 1)->update([

               'annual_report' => $name

            ]);


        }else{
           echo "Nothing to Upload";
        }

        return redirect()->to(route('admin/document_report').'#message')->with("message", "Branch report Uploaded Successfully!");
    }

    //History
    public function history()
    {
        $history_page = HistoryPage::find(1);
        return view('admin.history_page')->with(['page_data' => $history_page, 'page' => 'history']);
    }
    public function historyEdit()
    {
        $history_page = HistoryPage::find(1);
        return view('admin.history_edit')->with(['page_data' => $history_page, 'page' => 'history']);
    }

    public function historyEditPost(Request $request)
    {

        HistoryPage::where('id', 1)->update([

            'history_of_redcross' => $request->history_of_redcross,
            'history_of_nigerian' => $request->history_of_nigerian
        ]);

        return redirect()->to(route('admin/history').'#message')->with("message", "History page edited Successfully!");
    }

    //Gallery

    public function gallery()
    {
        $gallery = Gallery::orderBy('id', 'desc')->paginate(10);
        return view('admin.gallery_page')->with(['gallerys' => $gallery, 'page' => 'gallery']);
    }
    public function galleryEdit($id)
    {
        $gallery = Gallery::find($id);




        return view('admin.gallery_edit')->with(['gallery' => $gallery, 'page' => 'gallery']);
    }


    public function galleryEditPost(Request $request)
    {
        $file = $request->file('image');

        $name = time() . '-' . $file->getClientOriginalName();


        $file->move('img/gallery',$name);
// dd($request->id);

        Gallery::where('id', $request->id)->update([

            'image' => $name
        ]);

        return redirect()->to(route('admin/gallery').'#message')->with("message", "Gallery edited Successfully!");
    }

    public function galleryUpload()
    {
        return view('admin.gallery_upload')->with(['page' => 'gallery_upload']);
    }


    public function galleryUploadPost(Request $request)
    {

        $file = $request->file('image');

        $name = time() . '-' . $file->getClientOriginalName();


        $file->move('img/gallery',$name);


        $userModel = new Gallery();
        $userModel->image = $name;
        $userModel->save();

        return redirect()->to(route('admin/gallery').'#message')->with("message", "Gallery Uploaded Successfully!");
    }


    public function deleteGallery($id){
        Gallery::where('id', $id)->delete();

        return redirect()->to(route('admin/gallery').'#message')->with("message", "Gallery Deleted Successfully!");
    }


    //News

    public function news()
    {
        $news = News::orderBy('id', 'desc')->paginate(10);
        return view('admin.news_page')->with(['news' => $news, 'page' => 'news']);
    }
    public function newsEdit($id)
    {
        $news = News::find($id);

       return view('admin.news_edit')->with(['news' => $news, 'page' => 'news']);
    }


    public function newsEditPost(Request $request)
    {

        $date = date('Y-m-d');
        if($request->hasFile('image')) {



            $file = $request->file('image');

            $name = time() . '-' . $file->getClientOriginalName();


            $file->move('img/news',$name);


            News::where('id', $request->id)->update([

                'title' => $request->title,
                'description' => $request->description,
                'date' => $date,
                'image' => $name
            ]);
        }else{
            News::where('id', $request->id)->update([

                'title' => $request->title,
                'description' => $request->description,
                'date' => $date
            ]);
        }



        return redirect()->to(route('admin/news').'#message')->with("message", "News edited Successfully!");
    }

    public function newsUpload()
    {
        return view('admin.news_upload')->with(['page' => 'news_upload']);
    }

    public function newsUploadPost(Request $request)
    {

        $date = date('Y-m-d');

            $file = $request->file('image');

            $name = time() . '-' . $file->getClientOriginalName();


            $file->move('img/news',$name);

            $code = rand(100000000,999999999);
            $title = strtolower($request->title);
            $title = substr($title, 0, 150);
            $title = str_replace(' ', '_', $title);
            $code = $title.'_'.$code;


        $userModel = new News();
            $userModel->title = $request->title;
            $userModel->page_link =  $code;
            $userModel->description = $request->description;
            $userModel->date = $date;//get from the booking hall_id;
            $userModel->image = $name;
            $userModel->save();

        return redirect()->to(route('admin/news').'#message')->with("message", "News Uploaded Successfully!");
    }

    public function deleteNews($id){
        News::where('id', $id)->delete();

        return redirect()->to(route('admin/news').'#message')->with("message", "News Deleted Successfully!");
    }
    //Board Members


    public function boardMembers()
    {
        $board_members = BoardMember::orderBy('id', 'desc')->paginate(10);
        return view('admin.board_members')->with(['board_members' => $board_members, 'page' => 'board_members']);
    }


    public function boardMembersUpload()
    {
        $states = State::all();
        return view('admin.board_members_upload')->with(['page' => 'board_members_upload', 'states' => $states]);
    }


    public function boardMembersUploadPost(Request $request)
    {

        $file = $request->file('image');

        $name = time() . '-' . $file->getClientOriginalName();


        $file->move('img/board_members',$name);


        $userModel = new BoardMember();
        $userModel->full_name = $request->full_name;
        $userModel->post = $request->post;
        $userModel->sex = $request->sex;
        $userModel->state_id = $request->state_id;
        $userModel->marital_status = $request->marital_status;
        $userModel->profession = $request->profession;
        $userModel->qualification = $request->qualification;
        $userModel->reason_for_joining = $request->reason_for_joining;
        $userModel->image = $name;
        $userModel->save();

        return redirect()->to(route('admin/board_members').'#message')->with("message", "New Board Member Added Successfully!");
    }


    public function boardMembersEdit($id)
    {
        $board_members = BoardMember::find($id);
        $states = State::all();

        return view('admin.board_members_edit')->with(['board_members' => $board_members, 'page' => 'board_members', 'states' => $states]);
    }

    public function boardMembersEditPost(Request $request)
    {

        if($request->hasFile('image')) {

            $file = $request->file('image');

            $name = time() . '-' . $file->getClientOriginalName();


            $file->move('img/board_members',$name);

            BoardMember::where('id', $request->id)->update([

                'full_name' => $request->full_name,
                'post' => $request->post,
                'sex' => $request->sex,
                'state_id' => $request->state_id,
                'marital_status' => $request->marital_status,
                'profession' => $request->profession,
                'qualification' => $request->qualification,
                'reason_for_joining' => $request->reason_for_joining,
                'image' => $name
            ]);
        }else{
            BoardMember::where('id', $request->id)->update([
                'full_name' => $request->full_name,
                'post' => $request->post,
                'sex' => $request->sex,
                'state_id' => $request->state_id,
                'marital_status' => $request->marital_status,
                'profession' => $request->profession,
                'qualification' => $request->qualification,
                'reason_for_joining' => $request->reason_for_joining
            ]);
        }



        return redirect()->to(route('admin/board_members').'#message')->with("message", "Board Member edited Successfully!");
    }


    public function deleteBoardMember($id){
        BoardMember::where('id', $id)->delete();

        return redirect()->to(route('admin/board_members').'#message')->with("message", "Board Member Deleted Successfully!");
    }

    //Head of Departments


    public function hOD()
    {
        $board_members = HeadOfDepartment::orderBy('id', 'desc')->paginate(10);
        return view('admin.head_of_departments')->with(['board_members' => $board_members, 'page' => 'head_of_departments']);
    }


    public function hODUpload()
    {
        $states = State::all();
        return view('admin.head_of_departments_upload')->with(['page' => 'head_of_departments_upload', 'states' => $states]);
    }


    public function hODUploadPost(Request $request)
    {

        $file = $request->file('image');

        $name = time() . '-' . $file->getClientOriginalName();


        $file->move('img/head_of_departments',$name);


        $userModel = new HeadOfDepartment();
        $userModel->full_name = $request->full_name;
        $userModel->post = $request->post;
        $userModel->sex = $request->sex;
        $userModel->state_id = $request->state_id;
        $userModel->marital_status = $request->marital_status;
        $userModel->profession = $request->profession;
        $userModel->qualification = $request->qualification;
        $userModel->email = $request->email;
        $userModel->image = $name;
        $userModel->save();

        return redirect()->to(route('admin/head_of_departments').'#message')->with("message", "New Head of Department Added Successfully!");
    }


    public function hODEdit($id)
    {
        $board_members = HeadOfDepartment::find($id);
        $states = State::all();

        return view('admin.head_of_departments_edit')->with(['board_members' => $board_members, 'page' => 'head_of_departments', 'states' => $states]);
    }

    public function hODEditPost(Request $request)
    {

        if($request->hasFile('image')) {

            $file = $request->file('image');

            $name = time() . '-' . $file->getClientOriginalName();


            $file->move('img/head_of_departments',$name);

            HeadOfDepartment::where('id', $request->id)->update([

                'full_name' => $request->full_name,
                'post' => $request->post,
                'sex' => $request->sex,
                'state_id' => $request->state_id,
                'marital_status' => $request->marital_status,
                'profession' => $request->profession,
                'qualification' => $request->qualification,
                'email' => $request->email,
                'image' => $name
            ]);
        }else{
            HeadOfDepartment::where('id', $request->id)->update([
                'full_name' => $request->full_name,
                'post' => $request->post,
                'sex' => $request->sex,
                'state_id' => $request->state_id,
                'marital_status' => $request->marital_status,
                'profession' => $request->profession,
                'qualification' => $request->qualification,
                'email' => $request->email
            ]);
        }



        return redirect()->to(route('admin/head_of_departments').'#message')->with("message", "Head of Department edited Successfully!");
    }


    public function deletehOD($id){
        HeadOfDepartment::where('id', $id)->delete();

        return redirect()->to(route('admin/head_of_departments').'#message')->with("message", "Head of Department Deleted Successfully!");
    }

//Movement Partners


    public function movementPartners()
    {
        $movement_partners = MovementPartners::orderBy('id', 'desc')->paginate(10);
        return view('admin.movement_partners')->with(['movement_partners' => $movement_partners, 'page' => 'movement_partners']);
    }
    public function movementPartnersEdit($id)
    {
        $movement_partners = MovementPartners::find($id);

        return view('admin.movement_partners_edit')->with(['movement_partners' => $movement_partners, 'page' => 'movement_partners']);
    }

    public function movementPartnersEditPost(Request $request)
    {

        $date = date('Y-m-d');
        if($request->hasFile('image')) {



            $file = $request->file('image');

            $name = time() . '-' . $file->getClientOriginalName();


            $file->move('img/movement_partners',$name);


            MovementPartners::where('id', $request->id)->update([

                'link' => $request->link,
                'image' => $name
            ]);
        }else{
            MovementPartners::where('id', $request->id)->update([

                'link' => $request->link
            ]);
        }



        return redirect()->to(route('admin/movement_partners').'#message')->with("message", "Movement Partners edited Successfully!");
    }

    public function movementPartnersUpload()
    {
        return view('admin.movement_partners_upload')->with(['page' => 'movement_partners']);
    }


    public function movementPartnersUploadPost(Request $request)
    {

        $date = date('Y-m-d');

        $file = $request->file('image');

        $name = time() . '-' . $file->getClientOriginalName();


        $file->move('img/movement_partners',$name);

        $userModel = new MovementPartners();
        $userModel->link = $request->link;
        $userModel->image = $name;
        $userModel->save();

        return redirect()->to(route('admin/movement_partners').'#message')->with("message", "Movement Partners Uploaded Successfully!");
    }

    public function deleteMovementPartners($id){
        MovementPartners::where('id', $id)->delete();

        return redirect()->to(route('admin/movement_partners').'#message')->with("message", "Movement Partner Deleted Successfully!");
    }


//Newsletter
    public function newsletter()
    {
        $newsletter = NewsletterPost::orderBy('id', 'desc')->paginate(10);
        return view('admin.newsletter')->with(['press_release' => $newsletter, 'page' => 'newsletter']);
    }
    public function newsletterEdit($id)
    {
        $newsletter = NewsletterPost::find($id);

        return view('admin.newsletter_edit')->with(['newsletter' => $newsletter, 'page' => 'newsletter']);
    }


    public function newsletterEditPost(Request $request)
    {

        $date = date('Y-m-d');
        if($request->hasFile('image')) {



            $file = $request->file('image');

            $name = time() . '-' . $file->getClientOriginalName();


            $file->move('img/newsletter',$name);


            NewsletterPost::where('id', $request->id)->update([

                'title' => $request->title,
                'description' => $request->description,
                'date' => $date,
                'image' => $name
            ]);
        }else{
            NewsletterPost::where('id', $request->id)->update([

                'title' => $request->title,
                'description' => $request->description,
                'date' => $date
            ]);
        }



        return redirect()->to(route('admin/newsletter').'#message')->with("message", "Newsletter edited Successfully!");
    }

    public function newsletterUpload()
    {
        return view('admin.newsletter_upload')->with(['page' => 'newsletter_upload']);
    }



    public function newsletterUploadPost(Request $request)
    {

        $date = date('Y-m-d');

        $file = $request->file('image');

        $name = time() . '-' . $file->getClientOriginalName();


        $file->move('img/newsletter',$name);

        $code = rand(100000000,999999999);
        $title = strtolower($request->title);
        $title = substr($title, 0, 150);
        $title = str_replace(' ', '_', $title);
        $code = $title.'_'.$code;


        $userModel = new NewsletterPost();
        $userModel->title = $request->title;
        $userModel->page_link =  $code;
        $userModel->description = $request->description;
        $userModel->date = $date;//get from the booking hall_id;
        $userModel->image = $name;
        $userModel->save();

        //send mails to all subscribers
        $subscribers = Newsletter::all();
        foreach($subscribers as $subscriber){

            Mail::send('emails.mailClientNewsletterMonthlyPost', [ 'request' => $request->all() ], function($message) use($subscriber, $request) {
                $message->sender('newsletter@redcrossnigeria.org', $name = 'Nigerian RedCross');
                $message->from('newsletter@redcrossnigeria.org', $name = 'Nigerian RedCross');
                $message->to("$subscriber->email");
                $message->subject('Our Monthly Newsletter | '.$request->title);
            });

        }


        return redirect()->to(route('admin/newsletter').'#message')->with("message", "Newsletter  Uploaded and sent to Suscribers Successfully!");
    }

    public function deleteNewsletter($id){
        NewsletterPost::where('id', $id)->delete();

        return redirect()->to(route('admin/newsletter').'#message')->with("message", "Newsletter Deleted Successfully!");
    }


//Press Release
    public function pressRelease()
    {
        $press_release = PressRelease::orderBy('id', 'desc')->paginate(10);
        return view('admin.press_release')->with(['press_release' => $press_release, 'page' => 'press_release']);
    }
    public function pressReleaseEdit($id)
    {
        $press_release = PressRelease::find($id);

        return view('admin.press_release_edit')->with(['press_release' => $press_release, 'page' => 'press_release']);
    }


    public function pressReleaseEditPost(Request $request)
    {

        $date = date('Y-m-d');
        if($request->hasFile('image')) {



            $file = $request->file('image');

            $name = time() . '-' . $file->getClientOriginalName();


            $file->move('img/press_release',$name);


            PressRelease::where('id', $request->id)->update([

                'title' => $request->title,
                'description' => $request->description,
                'date' => $date,
                'image' => $name
            ]);
        }else{
            PressRelease::where('id', $request->id)->update([

                'title' => $request->title,
                'description' => $request->description,
                'date' => $date
            ]);
        }



        return redirect()->to(route('admin/press_release').'#message')->with("message", "Press Release edited Successfully!");
    }

    public function pressReleaseUpload()
    {
        return view('admin.press_release_upload')->with(['page' => 'press_release_upload']);
    }



    public function pressReleaseUploadPost(Request $request)
    {

        $date = date('Y-m-d');

//        $file = $request->file('image');

//        $name = time() . '-' . $file->getClientOriginalName();


//        $file->move('img/press_release',$name);

        $code = rand(100000000,999999999);
        $title = strtolower($request->title);
        $title = substr($title, 0, 150);
        $title = str_replace(' ', '_', $title);
        $code = $title.'_'.$code;


        $userModel = new PressRelease();
        $userModel->page_link = $code;
        $userModel->title = $request->title;
        $userModel->description = $request->description;
        $userModel->date = $date;
//        $userModel->image = $name;
        $userModel->save();

        return redirect()->to(route('admin/press_release').'#message')->with("message", "Press Release Uploaded Successfully!");
    }

    public function deletePress($id){
        PressRelease::where('id', $id)->delete();

        return redirect()->to(route('admin/faqs').'#message')->with("message", "Press Release Deleted Successfully!");
    }





//Press Release
    public function faqs()
    {
        $press_release = Faq::orderBy('id', 'desc')->paginate(10);
        return view('admin.faqs')->with(['press_release' => $press_release, 'page' => 'faqs']);
    }
    public function faqsEdit($id)
    {
        $press_release = Faq::find($id);

        return view('admin.faqs_edit')->with(['press_release' => $press_release, 'page' => 'faqs']);
    }


    public function faqsEditPost(Request $request)
    {


          $save =  Faq::where('id', $request->id)->update([

                'question' => $request->question,
                'answer' => $request->answer,
//                'date' => $date,
            ]);




       if($save){
           return redirect()->to(route('admin/faqs').'#message')->with("message", "FAQS edited Successfully!");
       }
        return redirect()->to(route('admin/faqs').'#message')->with("message", "FAQS NOT edited");
    }

    public function faqsUpload()
    {
        return view('admin.faqs_upload')->with(['page' => 'faqs_upload']);
    }



    public function faqsUploadPost(Request $request)
    {

        $date = date('Y-m-d');


        $userModel = new Faq();
//        $userModel->page_link = $code;
        $userModel->question = $request->question;
        $userModel->answer = $request->answer;

//        $userModel->image = $name;
        $userModel->save();

        return redirect()->to(route('admin/faqs').'#message')->with("message", "FAQS Uploaded Successfully!");
    }

    public function deleteFaqs($id){
        Faq::where('id', $id)->delete();

        return redirect()->to(route('admin/faqs').'#message')->with("message", "FAQS Deleted Successfully!");
    }


    //Branch Prrofile

    public function branchProfile()
    {
//        $branch_profile = BranchProfile::all()->where('state_id', 13)->first();
        $branch_profile = BranchProfile::orderBy('date', 'desc')->where('state_id', Auth::user()->id)->paginate(10);
        return view('admin.branch_profile')->with(['page' => 'branch_profile', 'branch_profile' => $branch_profile]);
    }

    public function branchProfileUploadPost(Request $request)
    {
//        $file = $request->file('images');
//        $name = time() . '-' . $file->getClientOriginalName();
//        $file->move('img/branch_profile',$name);
//        dd(json_encode($images));

        $date = date('Y-m-d');
        if($request->any_month != '' && $request->any_year != '') {
            $date = $request->any_year.'-'.$request->any_month.'-02';
        }



        $services = $request->branch_services;

        if($request->has('others_services')){
        $array =  explode(',', $request->others_services);
        $services = $request->branch_services;
        $services = array_merge($services, $array);
        }

        $images = [];
        $files=$request->file('images');
        foreach($files as $file){
            $name= time() . '-' . $file->getClientOriginalName();
            $file->move('img/branch_profile',$name);
            $images[] = $name;
        }


        $userModel = new BranchProfile();
        $userModel->date = $date;
        $userModel->state_id = Auth::user()->id;
        $userModel->subject = $request->subject;
        $userModel->content =  $request->contents;
        $userModel->branch_services = json_encode($services);
        $userModel->volunteer_strength = $request->volunteer_strength;
        $userModel->active_volunteer = $request->active_volunteer;
        $userModel->video_link = $request->video_link;
        $userModel->images = json_encode($images);
        $userModel->save();

        $state_name = Auth::user()->name;
        return redirect()->to(route('admin/branch_profile').'#message')->with("message", "$state_name Branch Profile Uploaded Successfully!");
    }

    public function branchProfileFullView($id)
    {
        $branch_profile = BranchProfile::find($id);

        return view('admin.branch_profile_view')->with(['branch_profile' => $branch_profile, 'page' => 'branch_profile']);
    }

    public function branchProfileEdit($id)
    {
        $branch_profile = BranchProfile::find($id);

        return view('admin.branch_profile_edit')->with(['branch_profile' => $branch_profile, 'page' => 'branch_profile']);
    }




    public function branchProfileEditPost(Request $request)
    {

        $date = date('Y-m-d');
        if($request->hasFile('new_images')) {


            $date = $request->any_year.'-'.$request->any_month.'-02';


            $services = $request->branch_services;

            if($request->has('others_services')){
                $array =  explode(',', $request->others_services);
                $services = $request->branch_services;
                $services = array_merge($services, $array);
            }

            //if we have new images
            $images = [];
            $files=$request->file('new_images');
            foreach($files as $file){
                $name= time() . '-' . $file->getClientOriginalName();
                $file->move('img/branch_profile',$name);
                $images[] = $name;
            }

            //if old images also exist
            if($request->has('images')){
                $array = $request->images;;
                $images = array_merge($array, $images);
            }

            BranchProfile::where('id', $request->id)->update([

                'date' => $date,
                'subject' => $request->subject,
                'content' => $request->contents,
                'branch_services' => json_encode($services),
                'volunteer_strength' => $request->volunteer_strength,
                'active_volunteer' => $request->active_volunteer,
                'video_link' => $request->video_link,
                'images' => json_encode($images)

            ]);
        }else{

            if($request->has('images')){
                $array = $request->images;
                $images = $array;
//                dd('yes');
            }else{
                $images = '';
            }



            $date = $request->any_year.'-'.$request->any_month.'-'.date('d');


            $services = $request->branch_services;

            if($request->has('others_services')){
                $array =  explode(',', $request->others_services);
                $services = $request->branch_services;
                $services = array_merge($services, $array);
            }
//            dd($request->id);

            BranchProfile::where('id', $request->id)->update([

                'date' => $date,
                'subject' => $request->subject,
                'content' => $request->contents,
                'branch_services' => json_encode($services),
                'volunteer_strength' => $request->volunteer_strength,
                'active_volunteer' => $request->active_volunteer,
                'video_link' => $request->video_link,
                'images' => json_encode($images)

            ]);
        }


        $state_name = Auth::user()->name;
        return redirect()->to(route('admin/branch_profile').'/'.$request->id.'#message')->with("message", "$state_name Branch Profile Edited Successfully!");
    }

    public function branchProfileUpload()
    {
        return view('admin.branch_profile_upload')->with(['page' => 'branch_profile_upload']);
    }

    public function deleteBranchProfile($id){
        BranchProfile::where('id', $id)->delete();

        return redirect()->to(route('admin/branch_profile').'#message')->with("message", "Branch Profile Deleted Successfully!");
    }

    public function newsletterMonthlyPost(Request $request)
    {

        try{

            Mail::send('emails.mailClientNewsletter', [ 'request' => $request->all(), 'image' =>'newsletter_aqua.jpg'  ], function($message) use($request) {
                $message->sender('info@aquapoolclub.com', $name = 'AquapoolClub');
                $message->from('info@aquapoolclub.com', $name = 'AquapoolClub Website');
                $message->to("$request->email");
                $message->subject('Thanks for subscribing to Our Newsletter');

            });

        }catch(Exception $e){

        }
    }




}
