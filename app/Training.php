<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
    public $table = 'trainings';
    protected $guarded = [];
}
